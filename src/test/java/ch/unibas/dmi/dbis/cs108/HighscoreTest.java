package ch.unibas.dmi.dbis.cs108;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class HighscoreTest {

    @Test
    public void shouldLoadFile() throws IOException {

        ch.unibas.dmi.dbis.cs108.Highscore.load();
        String s = ch.unibas.dmi.dbis.cs108.Highscore.printHighscore();
        ch.unibas.dmi.dbis.cs108.Highscore.save();

        Assert.assertEquals(s, ch.unibas.dmi.dbis.cs108.Highscore.printHighscore());
    }
    @Test
    public void shouldChangeFile() throws IOException {
        ch.unibas.dmi.dbis.cs108.Highscore.load();
        String s = ch.unibas.dmi.dbis.cs108.Highscore.printHighscore();
        ch.unibas.dmi.dbis.cs108.Highscore.addWin("Daniel");
        Assert.assertNotSame(s, ch.unibas.dmi.dbis.cs108.Highscore.printHighscore());
    }

}
