package ch.unibas.dmi.dbis.cs108.client;

import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.junit.Test;

import java.io.IOException;

public class ClientGameMachineTest {
    @Test(expected = IOException.class)
    public void shouldThrowIOException() {

        ClientGameMachine clientGameMachine = new ClientGameMachine("localhost", 8090, "test");
        clientGameMachine.start();
    }

}