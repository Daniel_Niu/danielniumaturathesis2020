package ch.unibas.dmi.dbis.cs108.gameEngines;

import ch.unibas.dmi.dbis.cs108.entity.Collision;
import ch.unibas.dmi.dbis.cs108.entity.Hitbox;
import org.joml.Vector2f;
import org.junit.Assert;
import org.junit.Test;

public class CollisionTest {
    private final Hitbox a = new Hitbox(new Vector2f(1, 1), 2, 2);
    private final Hitbox b = new Hitbox(new Vector2f(1, 0.5f), 2, 2);

    public CollisionTest() {
    }

    @Test
    public void shouldCollide() {

        Collision collision = a.getCollision(b);
        Assert.assertTrue(collision.overlap);

    }

    @Test
    public void shouldCorrect() {

        float y = a.getCenter().y;
        Collision collision = a.getCollision(b);
        a.correctPosition(b, collision);
        Assert.assertNotEquals(y, a.getCenter().y);
    }



}