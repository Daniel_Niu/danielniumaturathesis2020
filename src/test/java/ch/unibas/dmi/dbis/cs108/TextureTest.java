package ch.unibas.dmi.dbis.cs108;

import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.io.Display;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Model;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.opengl.GL11.*;

class TextureTest {

    public static void main(String[] args) {


        //Window display = new Window();
        //display.setSize(640, 480);
        //display.setFullscreen(false);

        //display.createWindow("Game");

        Display display = new Display(640, 480, "GAME");
        display.setBackgroundColor(0.0f, 1.0f, 0.0f);
        display.create();

        Input input = display.getInput();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            glEnable(GL_TEXTURE_2D);
            //start GameScreen with the amount of players
        Shader shader = new Shader("shaders/worldFragment.glsl", "shaders/worldVertex.glsl");
            shader.create();

        RectangleAssets.initAsset();
            float[] vertices = new float[] {
                    -1f, 1f, 0, //TOP LEFT 0
                    1f, 1f, 0, //TOP RIGHT 1
                    1f, -1f, 0, //BOTTOM RIGHT 2
                    -1f, -1f, 0,//BOTTOM LEFT 3
            };

            float[] texture = new float[] {
                    0,0,
                    1,0,
                    1,1,
                    0,1,
            };

            int[] indices = new int[] {
                    0,1,2,
                    2,3,0
            };
            Model model = new Model(vertices, texture, indices);
            Texture tex = new Texture("player/0.png");
            Matrix4f projection = new Matrix4f().ortho2D(-1280/2f, 1280/2f, -720/2f, 720/2f).scale(100);            //tick-limiter

            long lastTime = System.nanoTime();
            final double amountOfTicks = 60D;
            double ns = 1000000000 / amountOfTicks;
            float delta = 0;

            long startTime = System.currentTimeMillis();

            //GameScreen loop
        while ((!display.shouldClose() && !input.isKeyDown(GLFW.GLFW_KEY_ESCAPE)) /*&& (System.currentTimeMillis() - startTime) < 100000*/) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            if (delta >= 1) {

                //update


                delta--;

                }
                display.update();
                glClear(GL_COLOR_BUFFER_BIT);
                shader.bind();
                shader.setUniform("sampler", 0);
                shader.setUniform("projection", projection);
                model.render();
                tex.bind(0);
                display.swapBuffers();





            }

            //close Display

    }
}
