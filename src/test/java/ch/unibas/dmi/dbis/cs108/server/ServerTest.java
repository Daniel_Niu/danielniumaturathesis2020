package ch.unibas.dmi.dbis.cs108.server;

import org.junit.Assert;
import org.junit.Test;

public class ServerTest {

    @Test
    public void shouldNotStartGame(){
        Server server = new Server(8090);
        server.startLobby(0);
        Assert.assertEquals(server.lobbies.size(), 0);
    }


}