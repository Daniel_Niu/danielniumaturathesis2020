#version 140

attribute vec2 vertices;
attribute vec2 textures;

out vec4 color;
out vec2 uvCoords;

uniform vec4 matColor;
uniform mat4 projection;
uniform vec4 offset;
uniform vec2 pixelScale;
uniform vec2 screenPos;

void main()
{
	color = matColor;

	gl_Position = projection * vec4((vertices * pixelScale) + screenPos, 0, 1);
	uvCoords = (textures * offset.zw) + offset.xy;
}
