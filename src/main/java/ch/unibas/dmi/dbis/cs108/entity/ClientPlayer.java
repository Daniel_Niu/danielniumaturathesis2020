package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;


/**
 * @author daniel
 * The class for the player of the certain client. The update method is compatible with the direct keyboard inputs
 */
public class ClientPlayer extends Player {

    private static final int ANIMATIONS = 3;
    private static final int ANIMATION_IDLE = 0;
    private static final int ANIMATION_WALK = 1;
    private static final int ANIMATION_MARKED = 2;
    private final Logger logger = LogManager.getLogger(Enemy.class);
    private boolean a = true;
    private boolean d = true;


    /**
     * Constructor of Player
     *
     * @param ID       PlayerID
     * @param position Start position
     */
    public ClientPlayer(String name, int ID, Vector2f position, float width, float height) {
        super(ID, name, position, width, height);
        logger.debug("ClientPlayer has the ID: " + ID);
    }


    /**
     * Player update on the Clientside
     *
     * @param delta       value defined by the tick limiter: how fast this tick is
     * @param input       Opengl input from the window/display
     * @param clientWorld The world in which the player is
     */
    @Override
    public void update(float delta, Input input, ClientWorld clientWorld) {
        Vector2f movement;
        movement = processInputs(input);
        movement = movement.mul(delta);

        //ANIMATION
        marking = super.isMarked();
        if (movement.x == 0) {
            useAnimation(ANIMATION_IDLE, flipX);
        } else {
            if (movement.x > 0) {
                useAnimation(ANIMATION_WALK, -1);
            } else {
                useAnimation(ANIMATION_WALK, 1);
            }
        }


        //Vector2f clientpos = new Vector2f(positionSize.pos.x, positionSize.pos.y).add(movement);
        Vector2f clientpos = hitbox.getCenter().add(movement);

        Vector2f distance = new Vector2f(super.serverPosition.x - clientpos.x, super.serverPosition.y - clientpos.y);
        //if position too far away from the server position
        if (Math.abs(distance.x) < 3 && Math.abs(distance.y) < 3) {
            super.serverPosition.sub(distance.mul(0.75f));
            moveTo(super.serverPosition);
        } else {

            moveTo(new Vector2f(super.serverPosition));
        }
    }


    /**
     * method to process the pressed keys and return the Vector2f with the movement
     *
     * @param input input class containing all key informations from opengl keycallbacks
     * @return Vector2f containing x movement and y movement according to the inputs
     */
    private Vector2f processInputs(Input input) {
        Vector2f movement = new Vector2f();

        //JUMP
        if (input.isKeyDown(GLFW.GLFW_KEY_SPACE) && hitbox.canJump) {
            movement.add(0, velocityJump);
            jump_air = JUMP_AIR_COOLDOWN;
            velY = velocityJump;
        } else {
            if (jump_air == 0) {
                movement.add(0, velY);
                if (velY > MAX_FALL_SPEED) {
                    velY -= GRAVITY;
                }
            } else {
                movement.add(0, velY);
                jump_air--;
            }

        }
        if (!input.isKeyDown(GLFW.GLFW_KEY_SPACE)) {
            jump_air = 0;
        }


        //variables used to enable the newest input from A and D
        if (!input.isKeyDown(GLFW_KEY_A)) {
            d = true;
        }
        if (!input.isKeyDown(GLFW_KEY_D)) {
            a = true;
        }

        //MOVE LEFT
        if (input.isKeyDown(GLFW_KEY_A) && a) {
            movement.add(-velocityX, 0);
            d = false;
        }

        //MOVE RIGHT
        if (input.isKeyDown(GLFW_KEY_D) && d) {
            movement.add(velocityX, 0);
            a = false;
        }

        return movement;

    }


    @Override
    public void update(float delta, PlayerStatus playerStatus, ClientWorld clientWorld) {

    }

    @Override
    public void update(float delta) {

    }


}
