package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.LoopAnimation;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import org.joml.Vector2f;

public class Portal extends Entity {

    private final Vector2f teleport;

    public Portal(int itemID, Vector2f pos, Vector2f tel, float width, float height, boolean shouldRender, int flipx) {
        super(itemID, new Vector2f(pos.x, pos.y), width, height);
        super.createAnimationArray(1);
        this.teleport = tel;


    }

    public void initGraphics() {
        super.createAnimationArray(1);
        setAnimation(0, new LoopAnimation(9, 24, "portalAnimations/" + objectID));
        useAnimation(0, flipX);
    }

    public void checkPortal(Entity entity) {
        if (hitbox.intersects(entity.hitbox)) {
            entity.moveTo(teleport);
        }
    }

    @Override
    public void update(float delta, Input input, ClientWorld clientWorld) {

    }

    @Override
    public void update(float delta, PlayerStatus playerStatus, ClientWorld clientWorld) {

    }

    @Override
    public void update(float delta) {

    }
}
