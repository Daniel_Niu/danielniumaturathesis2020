package ch.unibas.dmi.dbis.cs108.game;

import ch.unibas.dmi.dbis.cs108.server.Server;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Game Class. here we can start our server/client
 */
class Game {
    private static final Logger logger = LogManager.getLogger(Game.class);

    /**
     * main-Methode, Starts a Server or a Client depends on arguments
     *
     * @param args for Server: Server [ip], for Client: Client [ip:port] with optional [username]
     */
    public static void main(String[] args) {


        if(args.length == 0){

            ClientGameMachine clientGameMachine = new ClientGameMachine("0", 0, "Guest");
            clientGameMachine.start();

        }

        //Choosing between server and client
        if (args[0].equalsIgnoreCase("server")) {
            Server s = new Server(Integer.parseInt(args[1]));
            s.run();

        }
        if (args[0].equalsIgnoreCase(("client"))) {

            ClientGameMachine clientGameMachine = new ClientGameMachine("0", 0, "Guest");
            clientGameMachine.start();

            /*int semicolon;
            String port;
            String ip;
            semicolon = args[1].indexOf(":");
            port = args[1].substring(semicolon + 1);

            ip = args[1].substring(0, semicolon);
            if (args.length == 2) {
                ClientGameMachine clientGameMachine = new ClientGameMachine(ip, Integer.parseInt(port), "Guest");
                clientGameMachine.start();

            } else {
                ClientGameMachine clientGameMachine = new ClientGameMachine(ip, Integer.parseInt(port), args[2]);
                clientGameMachine.start();

            }

            */

        }else {
            logger.debug("Please type in server or client");
        }
    }

}
