package ch.unibas.dmi.dbis.cs108.entity;
import org.joml.Vector2f;

/**
 * @author Daniel
 * <p>
 * 2D Hitbox of an entity
 */
public class
Hitbox {
    private Vector2f center;
    private float t;
    private float b;
    private float l;
    private float r;

    private float ot;
    private float ob;
    private float ol;
    private float or;


    private final Vector2f half_extent;
    public long previousSpot = 0;
    boolean canJump = true;
    boolean hitHead = false;

    /**
     * Constructor
     *
     * @param center Vector2f containing x and y value of the center
     * @param width  float value for width of the hitbox
     * @param height float value for the height of the hitbox
     */
    public Hitbox(Vector2f center, float width, float height) {
        this.center = new Vector2f(center);
        this.half_extent = new Vector2f(width / 2f, height / 2f);
        setEdges(this.center);
        setOldEdges(center);
    }


    /**
     * Methode used for creating an Collision class between two hitboxes
     * @param hitbox2   The hitbox to be collided with
     * @return Collision containing the distance between the hitboxes and wether they overlap/intersect
     * with each other
     */
    public Collision getCollision(Hitbox hitbox2) {
        Vector2f distance = hitbox2.center.sub(center, new Vector2f());
        distance.x = Math.abs(distance.x);
        distance.y = Math.abs(distance.y);

        distance.sub(half_extent.add(hitbox2.half_extent, new Vector2f()));

        return new Collision(distance, distance.x < 0 && distance.y < 0);
    }


    public boolean intersects(Hitbox hitbox2) {
        return !(this.b > hitbox2.t) && !(this.t < hitbox2.b) && !(this.l > hitbox2.r) && !(this.r < hitbox2.l);
    }

    /**
     * detection with a point, primarily used for mouse position
     *
     * @param point Vector2f containing the x and y of the current position
     * @return returns true if the point is in the hitbox of this
     */
    public boolean intersects(Vector2f point) {

        Vector2f distance = point.sub(center, new Vector2f());

        distance.x = Math.abs(distance.x);
        distance.y = Math.abs(distance.y);

        distance.sub(half_extent);
        return distance.x < 0 && distance.y < 0;
    }

    /**
     * Collision detection with a point, primarily used for mouse position
     *
     * @param point Vector2f containing the x and y of the current position
     * @return returns true if the point is in the hitbox of this
     */
    public Collision getCollision(Vector2f point) {
        Vector2f distance = point.sub(center, new Vector2f());

        distance.x = Math.abs(distance.x);
        distance.y = Math.abs(distance.y);

        distance.sub(half_extent);

        return new Collision(distance, distance.x < 0 && distance.y < 0);

    }

    /**
     * Method for correcting the overlapping/intersecting of two hitboxes
     *
     * @param hitbox2   other Hitbox
     * @param collision Collision data of the collsion
     */
    public void correctPosition(Hitbox hitbox2, Collision collision) {
        Vector2f corrDistance = hitbox2.center.sub(center, new Vector2f());

        hitHead = false;
        if (collision.distance.x > collision.distance.y) {
            if (corrDistance.x > 0) {
                center.add(collision.distance.x, 0);
            } else {
                center.add(-collision.distance.x, 0);
            }
        } else {
            if (corrDistance.y > 0) {
                center.add(0, collision.distance.y);
                hitHead = true;
            } else {

                center.add(0, -collision.distance.y);
                canJump = true;
            }
        }
    }


    /**
     * Method for correcting the overlapping/intersecting of two hitboxes, with consideration of the old position
     *
     * @param hitbox2 other hitbox
     */
    void correctPosition(Hitbox hitbox2) {

        if (this.b > hitbox2.t || this.t < hitbox2.b || this.l > hitbox2.r || this.r < hitbox2.l) {
            return;
        }
        hitHead = false;

        if (this.b <= hitbox2.t && this.ob > hitbox2.ot) {

            this.setBottom(hitbox2.t + 0.001f);
            this.canJump = true;
            return;

        }
        if (this.t >= hitbox2.b && this.ot < hitbox2.ob && !canJump) {

            this.setTop(hitbox2.b - 0.0001f);
            hitHead = true;
            return;
        }


        if (this.l <= hitbox2.r && this.ol > hitbox2.or) {

            this.setLeft(hitbox2.r + 0.002f);
            return;
        }
        if (this.r >= hitbox2.l && this.or < hitbox2.ol) {

            this.setRight(hitbox2.l - 0.002f);

        }


    }


    /**
     * sets the y-position of the top of the hitbox
     * moves the rest of the hitbox accordingly
     *
     * @param t the y-position of the hitboxes top
     */
    private void setTop(float t) {
        this.t = t;
        this.b = t - 2 * half_extent.y;
        this.center.y = t - half_extent.y;
    }

    /**
     * sets the y-position of the bottom of the hitbox
     * moves the rest of the hitbox accordingly
     *
     * @param b the y-position of the hitboxes bottom
     */
    private void setBottom(float b) {
        this.b = b;
        this.t = b + 2 * half_extent.y;
        this.center.y = b + half_extent.y;
    }

    /**
     * sets the x-position of the left of the hitbox
     * moves the rest of the hitbox accordingly
     *
     * @param l the x-position of the hitboxes left side
     */
    private void setLeft(float l) {
        this.l = l;
        this.r = l + (2 * half_extent.x);
        this.center.x = (l + half_extent.x);
    }

    /**
     * sets the x-position of the right of the hitbox
     * moves the rest of the hitbox accordingly
     *
     * @param r the x-position of the hitboxes right side
     */
    private void setRight(float r) {
        this.r = r;
        this.l = r - 2 * half_extent.x;
        this.center.x = r - half_extent.x;
    }


    /**
     * refresh sides with the current center
     *
     * @param c new center of the hitbox
     */
    private void setEdges(Vector2f c) {
        this.t = c.y + half_extent.y;
        this.b = c.y - half_extent.y;
        this.l = c.x - half_extent.x;
        this.r = c.x + half_extent.x;


    }

    /**
     * refresh sides with the old center
     *
     * @param c the old center of the hitbox before it updates
     */
    private void setOldEdges(Vector2f c) {
        this.ot = c.y + half_extent.y;
        this.ob = c.y - half_extent.y;
        this.ol = c.x - half_extent.x;
        this.or = c.x + half_extent.x;
    }

    /**
     * gets the center of the hitbox
     *
     * @return the position of the center of the hitbox
     */
    public Vector2f getCenter() {
        return new Vector2f(center);
    }

    void setCenter(Vector2f vector2f) {
        setOldEdges(this.center);
        this.center = new Vector2f(vector2f);
        setEdges(this.center);
    }

    public Vector2f getSize() {
        return new Vector2f(this.half_extent.x * 2, this.half_extent.y * 2);
    }

    public float getX() {
        return center.x;
    }

    public float getY() {
        return center.y;
    }

    public float getWidth() {
        return half_extent.x * 2;
    }

    /**
     * moves the hitbox to a new position
     *
     * @param x the new x-position of the center
     * @param y the new y-position of the center
     */
    public void setCenter(float x, float y) {
        setOldEdges(this.center);
        this.center = new Vector2f(x, y);
        setEdges(this.center);
    }

    public Vector2f getHalf_extent() {
        return half_extent;
    }

    public float getHeight() {
        return half_extent.y * 2;
    }

}
