package ch.unibas.dmi.dbis.cs108.entity;
import ch.unibas.dmi.dbis.cs108.server.ServerInThread;
import org.joml.Vector2f;

/**
 * @author Daniel Niu
 * ServerPlayer class
 * Subclass of Player, with ServerInThread as parameter
 */
public class ServerPlayer extends Player {

    private final ServerInThread sIT;

    /**
     * Constructor
     *
     * @param sIT      Thread which we using to run our playerobject
     * @param position Vector2f of the x and y value of the entity's spawn location
     */
    public ServerPlayer(ServerInThread sIT, Vector2f position, float width, float height) {
        super(sIT.playerID, sIT.getName(), position, width, height);
        this.sIT = sIT;
    }

    /**
     * updates our playerobject
     * you cant run out of borders
     */
    @Override
    public void update(float delta) {
        Vector2f movement;
        movement = processInputs();
        movement = movement.mul(delta);
        if (this.getMarkCD() > 0) {
            this.discountMarkCD();
        }
        //JUMP
        //player can only jump if it's standing on something
        super.move(movement);
        //printChanges(printers);
    }

    /**
     * processes the movement inputs for the player
     *
     * @return the movement vector resulting from the current inputs
     */
    private Vector2f processInputs() {
        Vector2f movement = new Vector2f();

        //MOVE LEFT
        if (this.sIT.getA()) {
            movement.add(-velocityX, 0);
        }
        //MOVE RIGHT
        if (this.sIT.getD()) {
            movement.add(velocityX, 0);
        }
        //STAND STILL
        if (!this.sIT.getA() && !this.sIT.getD()) {
            movement.x = 0;
        }

        if (sIT.getSpace() && hitbox.canJump) {
            movement.add(0, velocityJump);
            jump_air = JUMP_AIR_COOLDOWN;
            velY = velocityJump;
            hitbox.canJump = false;
        } else {
            if (jump_air == 0) {
                movement.add(0, velY);
                if (velY > MAX_FALL_SPEED) {
                    velY -= GRAVITY;
                }

            } else {
                movement.add(0, velY);
                jump_air--;
            }

        }
        if (!sIT.getSpace()) {
            jump_air = 0;
        }


        return movement;

    }


    /**
     * creates a String with all argument information for Protocol.VEL
     *
     * @return the playerID and velocities encoded as they are used in Protocol.VEL
     */
    public String printVel() {
        float moveX = 0;

        //MOVE LEFT
        if (this.sIT.getA()) {
            moveX = -velocityX;
        }
        //MOVE RIGHT
        if (this.sIT.getD()) {


            moveX = velocityX;
        }
        return this.getPlayerID() + ";" + moveX + ";" + velY;
    }




}


