package ch.unibas.dmi.dbis.cs108.screens;

import ch.unibas.dmi.dbis.cs108.shaderutils.*;
import ch.unibas.dmi.dbis.cs108.utils.Vector2f;
import ch.unibas.dmi.dbis.cs108.utils.Vector3f;

/**
 * class to set up our StartScreen
 */
class StartScreen {
    private static final Mesh mesh = new Mesh(new Vertex[]{ //creates a rectangle
            new Vertex(new Vector3f(-1f, 1f, 0.0f), new Vector3f(1.0f, 0.0f, 0.0f), new Vector2f(0.0f, 0.0f)),  //top left corner
            new Vertex(new Vector3f(-1f, -1f, 0.0f), new Vector3f(0.0f, 1.0f, 0.0f), new Vector2f(0.0f, 1.0f)),  //top right corner
            new Vertex(new Vector3f(1f, -1f, 0.0f), new Vector3f(0.0f, 0.0f, 1.0f), new Vector2f(1.0f, 1.0f)),  //bottom right corner
            new Vertex(new Vector3f(1f, 1f, 0.0f), new Vector3f(1.0f, 1.0f, 0.0f), new Vector2f(1.0f, 0.0f)) //bottom left corner
    }, new int[]{ //
            0, 1, 2, //draws from vertex[0] to vertex[1] to vertex[2] (just a triangle)
            0, 3, 2, //draws from vertex[0] to vertex[3] to vertex[2] (finish rectangle)
    }, new Material("src/main/resources/textures/START.jpg"));
    private static Renderer renderer;
    private static Shader shader;

    /**
     * render our Startscreen
     */
    public static void render() {
        renderer.renderMesh(mesh);
    }

    /**
     * close our Startscreen
     */
    public static void close() {
        mesh.destroy();
        shader.destroy();
    }
}
