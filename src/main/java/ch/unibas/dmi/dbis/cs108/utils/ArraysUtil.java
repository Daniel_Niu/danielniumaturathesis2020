package ch.unibas.dmi.dbis.cs108.utils;

import java.util.ArrayList;
import java.util.List;

public class ArraysUtil {
    public static <T> int getLength(T[] arr) {
        int count = 0;
        for (T el : arr)
            if (el != null)
                ++count;
        return count;
    }

    public static <T> List<T> getCopy(T[] arr) {
        List<T> list = new ArrayList<>();
        for (T el : arr) {
            if (el != null) {
                list.add(el);
            }
        }
        return list;
    }
}
