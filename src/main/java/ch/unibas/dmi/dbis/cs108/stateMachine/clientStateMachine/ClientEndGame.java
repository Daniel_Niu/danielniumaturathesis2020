package ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine;

import ch.unibas.dmi.dbis.cs108.gui.EndGUI;
import ch.unibas.dmi.dbis.cs108.io.Display;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT;
import static org.lwjgl.opengl.GL11.*;


public class ClientEndGame implements GameState {
    private final ClientGameMachine clientGameMachine;
    private Display display;
    private Input input;

    ClientEndGame(ClientGameMachine clientGameMachine) {
        this.clientGameMachine = clientGameMachine;
    }

    @Override
    public void run() {
        EndGUI endGUI = new EndGUI(clientGameMachine);

        long lastTime = System.nanoTime();
        final double amountOfTicks = 60D;
        double ns = 1000000000 / amountOfTicks;
        float delta = 0;


        //GameScreen loop
        while ((!display.shouldClose() && !input.isKeyDown(GLFW.GLFW_KEY_ESCAPE) && clientGameMachine.endScreen)/*&& (System.currentTimeMillis() - startTime) < 100000*/) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1) {
                //update

                delta--;
                endGUI.update((input.getMouseX() / display.getWidth()) * clientGameMachine.displayWidth,
                        (input.getMouseY() / display.getHeight()) * clientGameMachine.displayHeight,
                        input.isButtonDown(GLFW_MOUSE_BUTTON_LEFT));

            }
            display.update();

            glClearColor(0, 0, 0, 1);

            glClear(GL_COLOR_BUFFER_BIT);
            endGUI.render(clientGameMachine.shader, clientGameMachine.smallFont);

            display.swapBuffers();
        }
        if (display.shouldClose()) {
            clientGameMachine.closeClient();
        }

        clientGameMachine.endScreen = true;
        clientGameMachine.world = null;
        clientGameMachine.setGameState(clientGameMachine.getClientLobby());
        clientGameMachine.chatWindow.setVisible(true);
        clientGameMachine.runState();

    }

    @Override
    public void start() {
        display = clientGameMachine.disp;
        input = clientGameMachine.disp.getInput();
        run();

    }
}
