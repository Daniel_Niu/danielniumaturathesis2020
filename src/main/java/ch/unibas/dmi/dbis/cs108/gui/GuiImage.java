package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.shaderutils.Positioning;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

public class GuiImage implements GuiRenderObject {
    private final Positioning positioning;
    private final Texture texture;

    public GuiImage(Texture texture, Vector2f screenPos, Matrix4f projection, float xScale, float yScale) {
        this.positioning = new Positioning(1, 1, screenPos, projection, xScale, yScale);
        this.texture = texture;
    }

    @Override
    public void render(Shader shader) {
        shader.bind();
        texture.bind(0);
        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("projection", positioning.projection);
        shader.setUniform("sampler", 0);
        shader.setUniform("screenPos", positioning.screenPos);
        shader.setUniform("offset", new Vector4f(0, 0, 1, 1));
        shader.setUniform("pixelScale", new Vector2f(positioning.xScale, positioning.yScale));
        RectangleAssets.initAsset();
        RectangleAssets.getModel().render();
    }
}
