package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.server.Server;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.TextRenderer;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class StartChoiceGUI {

    private final ClientGameMachine clientGameMachine;
    private final List<GuiRenderObject> guiRenderObjectList = new ArrayList<>();
    private final List<GuiUpdatableObject> guiUpdatableObjects = new ArrayList<>();

    public TextRenderer textRenderer = new TextRenderer(new Vector4f(0f, 0f, 0f, 100f));


    public StartChoiceGUI(ClientGameMachine clientGameMachine) {
        this.clientGameMachine = clientGameMachine;
        Matrix4f projection = new Matrix4f().setOrtho2D(0, clientGameMachine.displayWidth, clientGameMachine.displayHeight, 0);

        GuiImage background = new GuiImage(new Texture("bg.png"), new Vector2f(0, -0), new Matrix4f().ortho2D(-1, 1, -1, 1), 2, 2); // TODO add background
        Button serverButton = new Button(new Vector2f(500, clientGameMachine.displayHeight * 0.6f - 50), projection, 500, 400);
        Button clientButton = new Button(new Vector2f(1280 - 100, clientGameMachine.displayHeight * 0.6f - 50), projection, 500, 400);

        Texture[] textures = new Texture[3];
        textures[0] = new Texture("serverButton/0.png");
        textures[1] = new Texture("serverButton/1.png");
        textures[2] = new Texture("serverButton/2.png");

        serverButton.setTexture3t(textures);

        textures[0] = new Texture("clientButton/0.png");
        textures[1] = new Texture("clientButton/1.png");
        textures[2] = new Texture("clientButton/2.png");

        clientButton.setTexture3t(textures);

        serverButton.addActionEvent(() -> {

            try {

                final JDialog dialog = new JDialog();
                dialog.setAlwaysOnTop(true);

                int portNumber;
                String s = JOptionPane.showInputDialog(dialog,"Port number");
                portNumber = Integer.parseInt(s);
                Server server = new Server(portNumber);

                clientGameMachine.server = new Thread(server);
                clientGameMachine.server.start();
                clientGameMachine.ip = "localhost";
                clientGameMachine.port = portNumber;

                clientGameMachine.setGameState(clientGameMachine.getClientLobby());
                clientGameMachine.nextState();


            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Please choose a valid port.");
            }


        });

        clientButton.addActionEvent(() -> {
            try {
                String s = JOptionPane.showInputDialog("IP-address");

                int semicolon;
                String port;
                String ip;
                semicolon = s.indexOf(":");
                port = s.substring(semicolon + 1);

                ip = s.substring(0, semicolon);

                clientGameMachine.ip = ip;
                clientGameMachine.port = Integer.parseInt(port);

                clientGameMachine.setGameState(clientGameMachine.getClientLobby());
                clientGameMachine.nextState();

            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Please choose a valid port.");

            }
        });

        guiRenderObjectList.add(background);
        guiRenderObjectList.add(serverButton);
        guiRenderObjectList.add(clientButton);
        guiUpdatableObjects.add(serverButton);
        guiUpdatableObjects.add(clientButton);
    }

    public void update(double mouseX, double mouseY, boolean mouseLeft) {
        for (GuiUpdatableObject guiUpdatableObject : guiUpdatableObjects) {
            guiUpdatableObject.update(mouseX, mouseY, mouseLeft);
        }
    }

    public void render(Shader shader, Font font) {

        for (GuiRenderObject guiRenderObject : guiRenderObjectList) {
            guiRenderObject.render(shader);
        }


    }
}
