package ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine;

import ch.unibas.dmi.dbis.cs108.entity.Item;
import ch.unibas.dmi.dbis.cs108.entity.Player;
import ch.unibas.dmi.dbis.cs108.entity.Portal;
import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.io.*;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;

import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Niu
 * stateMachine class to manage the client stages
 */
public class ClientGameMachine {
    public String playerName;

    Thread clientIn;
    public Thread server;

    public ClientInThread clientInThread;
    KeyEventSender keyEventSender;
    public Socket sock;
    public String ip;
    public int port;
    public Input input;
    public Font bigFont;
    public Font smallFont;
    public Font verysmallFont;

    public InputStream in;
    public PrintWriter printer;

    Display disp;
    public int displayWidth = 1728;
    public int displayHeight = 972;
    public ChatWindow chatWindow;
    public boolean gameRunning = true;

    public Shader shader;

    public int currentRound = 1;
    public ClientWorld world;

    public boolean gameStart = false;
    public boolean endScreen = true;
    public boolean skinsSelected = false;
    public boolean roundPause = false;
    public Player[] players = new Player[5];
    public Item[] items = new Item[2];
    public List<Portal> portals = new ArrayList<>();


    public int playerID;
    public int lobbyID = -1;
    public int lobbySize;


    private final GameState start;

    private final GameState clientLobby;
    private final GameState inGame;
    private final GameState endGame;

    private GameState gameState;

    /**
     * Constructor
     *
     * @param ip   String ip which can be numbers like 1.1.1.1 or also "localhost"
     * @param port int port number which has 4 digits
     * @param name Name of the player
     */
    public ClientGameMachine(String ip, int port, String name) {
        playerName = name;
        this.ip = ip;
        this.port = port;

        start = new Start(this);
        clientLobby = new ClientLobby(this);
        inGame = new InGame(this);
        endGame = new ClientEndGame(this);

        gameState = start;
    }

    /**
     * removes a player from the list of players in game
     * @param id the id of the player who should be removed
     */
    public void removePlayer(int id) {
        this.players[id] = null;
    }

    /**
     * sets the game state
     *
     * @param newGameState the new game state
     */
    public void setGameState(GameState newGameState) {
        gameState = newGameState;
    }

    public void start() {
        gameState.start();
    }

    public void nextState() {
        gameState.start();
    }

    public void runState() {
        gameState.run();
    }

    public GameState getStart() {
        return start;
    }

    public GameState getClientLobby() {
        return clientLobby;
    }

    public GameState getInGame() {
        return inGame;
    }

    GameState getEndGame() {
        return endGame;
    }

    /**
     * closes the display and exits
     */
    void closeClient() {
        if (disp != null) {
            disp.destroy();
        }
        Texture.destroyAllTextures();
        if (shader != null) {
            shader.destroy();
        }

        RectangleAssets.deleteAsset();
        System.exit(0);
    }
}
