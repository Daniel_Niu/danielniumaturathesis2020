package ch.unibas.dmi.dbis.cs108.shaderutils;

import org.joml.Matrix4f;
import org.joml.Vector2f;

public class Positioning {
    public float x;
    public float y;
    public Vector2f screenPos;
    public Matrix4f projection;
    public float xScale;
    public float yScale;

    public Positioning(float x, float y, Vector2f screenPos, Matrix4f projection, float xScale, float yScale) {
        this.x = x;
        this.y = y;
        this.screenPos = screenPos;
        this.projection = projection;
        this.xScale = xScale;
        this.yScale = yScale;
    }
}
