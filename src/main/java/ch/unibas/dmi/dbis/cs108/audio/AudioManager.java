package ch.unibas.dmi.dbis.cs108.audio;

import ch.unibas.dmi.dbis.cs108.shaderutils.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * class to load a .wav Audio File and play it
 * this class contains methods to play a AudioManager or to play a specific File.
 */
public class AudioManager {
    Logger logger = LogManager.getLogger(AudioManager.class);
    private static long currentFrame;
    private static Clip clip;
    private final AudioInputStream audioInputStream;
    private String status;

    /**
     * Constructor AudioManager, reads a AudioFile as a Stream and loads it into our clip
     *
     * @param filePath String: path where our .wav file is located
     * @throws IOException                   if cant find the file
     * @throws UnsupportedAudioFileException if the file isnt readable
     * @throws LineUnavailableException      if the Stream isnt an AudioInputStream
     */
    private AudioManager(String filePath) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        InputStream in = Model.class.getClassLoader().getResourceAsStream(filePath);
        assert in != null;
        BufferedInputStream bufferedInputStream = new BufferedInputStream(in);
        audioInputStream = AudioSystem.getAudioInputStream(bufferedInputStream);
        clip = AudioSystem.getClip();
        clip.open(audioInputStream);

    }

    /**
     * sets our Volume
     *
     * @param volume between 0f and 1f, 1f is 100%, you can lower the volume by entering smaller float
     */
    private void setVolume(float volume) {
        if ((volume > 0f) || (volume < 1f)) {
            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(20f * (float) Math.log10(volume));
        }
    }

    /**
     * starts our clip which we enabled in our constructor
     */
    private void play() {
        clip.start();

        status = "play";
    }

    /**
     * starts out clip wich we enabled in our constructor
     * before we loop our clip, so it starts again, when he's finished
     */
    private void playLooped() {
        //lower volume from loop-Music (ingame & lobby sound)
        setVolume(0.25f);
        clip.loop(Clip.LOOP_CONTINUOUSLY);
        clip.start();

        status = "play";
    }

    /**
     * plays our background Music.
     * if called, a new AudioManager with our background-Music-file is created and played in loop.
     */
    public static void playBackground() {
        AudioManager am = null;
        try {
            am = new AudioManager("audio/Pim Poy.wav");
        } catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
            e.printStackTrace();
        }

        assert am != null;
        am.playLooped();
    }

    /**
     * plays the sound, when a player gets marked
     * if called, a new AudioManager with our marked-Music-file is created and played in loop.
     */
    public static void playMarked() {
        AudioManager am = null;
        try {
            am = new AudioManager("audio/mark.wav");
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        assert am != null;
        am.play();
    }

    /**
     * plays the sound, when a player gets an item-buff
     * if called, a new AudioManager with our buff-Music-file is created and played in loop.
     */
    public static void playBuff() {
        AudioManager am = null;
        try {
            am = new AudioManager("audio/buff.wav");
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        assert am != null;
        am.play();
    }

    /**
     * plays our sound in the lobby
     * if called, a new AudioManager with our lobby(whistle)-Music-file is created and played in loop.
     */
    public static void playWhistle() {
        AudioManager am = null;
        try {
            am = new AudioManager("audio/whistle.wav");
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        assert am != null;
        am.playLooped();
    }

    /**
     * if called, our current AudioManager stops and clip is closed. to run again, a new AudioManger would be needed
     */
    public static void stop() {
        currentFrame = 0L;
        clip.stop();
        clip.close();
    }
}
