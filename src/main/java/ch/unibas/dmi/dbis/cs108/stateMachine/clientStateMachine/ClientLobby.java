package ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine;

import ch.unibas.dmi.dbis.cs108.audio.AudioManager;
import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.gui.SkinSelection;
import ch.unibas.dmi.dbis.cs108.io.ChatWindow;
import ch.unibas.dmi.dbis.cs108.io.ClientInThread;
import ch.unibas.dmi.dbis.cs108.io.Display;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author Daniel Niu
 * Lobby state of the client
 */
public class ClientLobby implements GameState {
    private final ClientGameMachine clientGameMachine;
    private final Logger logger = LogManager.getLogger(ClientLobby.class);
    private Input input;
    private Display display;

    /**
     * Constructor
     *
     * @param gm the game machine of the client
     */
    ClientLobby(ClientGameMachine gm) {
        clientGameMachine = gm;
    }

    /**
     * waiting for the variable gameMachine.gameStart to turn true, which
     * is changed by the ClientInthread if the server starts the lobby
     */
    @Override
    public void run() {
        //start playing our Lobby Sound
        AudioManager.playWhistle();

        long lastTime = System.nanoTime();
        final double amountOfTicks = 60D;
        double ns = 1000000000 / amountOfTicks;
        float delta = 0;
        Texture welcomeScreen = new Texture("welcomeScreen.png");

        SkinSelection skinSelection = new SkinSelection(clientGameMachine);
        glDisable(GL_DEPTH_TEST);

        //GameScreen loop
        while (!display.shouldClose() && !input.isKeyDown(GLFW.GLFW_KEY_ESCAPE) && !clientGameMachine.skinsSelected) {

            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1) {
                //update
                if (clientGameMachine.gameStart) {
                    //if gamestart, stop our lobbysound
                    AudioManager.stop();

                    skinSelection.update((input.getMouseX() / display.getWidth()) * clientGameMachine.displayWidth,
                            (input.getMouseY() / display.getHeight()) * clientGameMachine.displayHeight,
                            input.isButtonDown(GLFW_MOUSE_BUTTON_LEFT));
                    skinSelection.render(clientGameMachine.shader);
                }
                delta--;


            }
            display.update();

            glClearColor(0, 0, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT);

            if (clientGameMachine.gameStart) {
                clientGameMachine.chatWindow.setVisible(false);
                //if gamestart, stop our lobbysound
                AudioManager.stop();
                skinSelection.render(clientGameMachine.shader);
            } else {

                Matrix4f projection = new Matrix4f().ortho2D(-1, 1, -1, 1);
                clientGameMachine.shader.bind();
                clientGameMachine.shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
                clientGameMachine.shader.setUniform("sampler", 0);
                clientGameMachine.shader.setUniform("screenPos", new Vector2f(0, -0));
                clientGameMachine.shader.setUniform("offset", new Vector4f(0f, 0f, 1f, 1f));
                clientGameMachine.shader.setUniform("projection", projection);
                clientGameMachine.shader.setUniform("pixelScale", new Vector2f(2, 2));
                welcomeScreen.bind(0);
                RectangleAssets.getModel().render();

            }

            display.swapBuffers();
        }
        //if display is closed or skin is selected, stop lobbysound
        AudioManager.stop();

        if (display.shouldClose()) {

            //if display is closed, stop lobbysound
            AudioManager.stop();

            clientGameMachine.closeClient();
        }
        clientGameMachine.skinsSelected = false;

        clientGameMachine.setGameState(clientGameMachine.getInGame());
        clientGameMachine.nextState();

    }


    /**
     * Start of the lobby
     * builds connection to the server and starts the Chatwindow for chat
     * interactions and lobby selection
     */
    @Override
    public void start() {
        //build connection
        try {
            clientGameMachine.sock = new Socket(clientGameMachine.ip, clientGameMachine.port);
            clientGameMachine.in = clientGameMachine.sock.getInputStream();
            clientGameMachine.printer = new PrintWriter(clientGameMachine.sock.getOutputStream());
        } catch (IOException e) {
            logger.error("could not initialize Socket");
            throw new UnsupportedOperationException();
        }

        display = clientGameMachine.disp;
        input = clientGameMachine.disp.getInput();

        clientGameMachine.chatWindow = new ChatWindow(clientGameMachine);
        clientGameMachine.chatWindow.setVisible(true);

        clientGameMachine.clientInThread = new ClientInThread(clientGameMachine);
        clientGameMachine.clientIn = new Thread(clientGameMachine.clientInThread);
        clientGameMachine.clientIn.start();

        run();

    }
}
