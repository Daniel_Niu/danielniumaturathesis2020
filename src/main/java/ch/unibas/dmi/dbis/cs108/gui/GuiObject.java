package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.Collision;
import ch.unibas.dmi.dbis.cs108.entity.Hitbox;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.font.Letter;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.FontAssets;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

/**
 * abstract class for all objects in the GUI
 */
abstract class GuiObject {

    public int id;
    final int SELECTED = 1;
    final int CLICKED = 2;
    private final int IDLE = 0;
    int state;
    Hitbox hitbox;


    /**
     * Constructor
     *
     * @param id       the ID of the object
     * @param position the position on the screen
     * @param width    the width of the hitbox
     * @param height   the height of the hitbox
     */
    GuiObject(int id, Vector2f position, float width, float height) {
        this.id = id;
        this.hitbox = new Hitbox(position, width, height);
        state = IDLE;
    }

    /**
     * determines if the mouse clicked on the object
     * @param input the mouse input
     */
    public void update(Input input) {
        Collision data = hitbox.getCollision(new Vector2f((float) input.getMouseX(), (float) input.getMouseY()));

        if (data.overlap) {
            state = SELECTED;
            if (input.isButtonDown(org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT)) {
                state = CLICKED;
            }
        } else {
            state = IDLE;
        }
    }

    public abstract void render(Shader shader, ClientGameMachine clientGameMachine, Font font);


    void label(String text, Font font, Shader shader, float x, float y, Vector4f textColor, Matrix4f projection, float flipY) {

        float xTemp;
        char[] c;
        glBindTexture(GL_TEXTURE_2D, font.ID());
        Map<Character, Letter> chars = font.getCharacters();
        xTemp = x;
        c = text.toCharArray();
        shader.setUniform("projection", projection);
        shader.setUniform("matColor", textColor);
        shader.setUniform("sampler", 0);

        for (char value : c) {


            Letter r = chars.get(value);
            shader.setUniform("screenPos", new Vector2f(xTemp, y));
            shader.setUniform("offset", new Vector4f(r.x, r.y, r.w, r.h));
            shader.setUniform("pixelScale", new Vector2f(r.scaleX, r.scaleY * flipY));
            FontAssets.initAsset();
            FontAssets.getModel().render();
            xTemp += r.scaleX;
        }
    }

    public void moveTo(float x, float y) {
        hitbox.setCenter(x, y);
    }
}
