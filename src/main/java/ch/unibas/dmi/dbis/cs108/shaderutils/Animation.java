package ch.unibas.dmi.dbis.cs108.shaderutils;

import ch.unibas.dmi.dbis.cs108.io.Timer;
import ch.unibas.dmi.dbis.cs108.utils.Vector2f;
import ch.unibas.dmi.dbis.cs108.utils.Vector3f;
import org.lwjgl.opengl.GL20;

/**
 * Class used for animations
 */
class Animation {

    private final Material[] textures;
    private int pointer;
    private double elapsedTime;
    private double lasTime;
    private double currentTime;
    private final double fps;
    private Mesh mesh;
    private final Shader shader;
    private final Renderer renderer;
    private float x;
    private float y;


    /**
     * Constructor
     *
     * @param s the shader used
     * @param r the renderer used
     */
    public Animation(Shader s, Renderer r) {
        this.shader = s;
        this.renderer = r;
        this.pointer = 0;
        this.elapsedTime = 0;
        this.currentTime = 0;
        this.lasTime = Timer.getTime();
        this.fps = 1.0 / (double) 1;
        this.textures = new Material[10];
        for (int i = 1; i < 10; i++) {
            this.textures[i] = new Material("/src/main/resources/textures/timer" + "timer" + i + ".png");
        }
        this.mesh = new Mesh(new Vertex[]{ //creates a rectangle
                new Vertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 0.0f)),  //top left corner
                new Vertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 1.0f)),  //top right corner
                new Vertex(new Vector3f(1.0f, 0.8f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 1.0f)),  //bottom right corner
                new Vertex(new Vector3f(1.0f, 0.8f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 0.0f)) //bottom left corner
        }, new int[]{ //
                0, 1, 2, //draws from vertex[0] to vertex[1] to vertex[2] (just a triangle)
                0, 3, 2, //draws from vertex[0] to vertex[3] to vertex[2] (finish rectangle)
        }, new Material("src/main/resources/textures/timer/timer1.png"));
        this.mesh.create();
        this.shader.create();

    }



    public Mesh getMesh() {
        this.currentTime = Timer.getTime();
        this.elapsedTime = this.elapsedTime + currentTime - lasTime;

        if (elapsedTime >= fps) {
            elapsedTime = 0;
            pointer++;
        }
        if (pointer >= textures.length) {
            pointer = 0;
        }
        this.lasTime = currentTime;


        return new Mesh(new Vertex[]{ //creates a rectangle
                new Vertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 0.0f)),  //top left corner
                new Vertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 1.0f)),  //top right corner
                new Vertex(new Vector3f(1.0f, 0.8f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 1.0f)),  //bottom right corner
                new Vertex(new Vector3f(1.0f, 0.8f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 0.0f)) //bottom left corner
        }, new int[]{ //
                0, 1, 2, //draws from vertex[0] to vertex[1] to vertex[2] (just a triangle)
                0, 3, 2, //draws from vertex[0] to vertex[3] to vertex[2] (finish rectangle)
        }, textures[pointer]);

    }

    /**
     * renders the animation
     */
    public void render() {
        GL20.glUseProgram(this.shader.getProgID());

        GL20.glLoadIdentity();
        GL20.glColor3f(1.0f, 1.0f, 1.0f);
        GL20.glTranslatef(this.x, this.y, 0f);
        this.mesh = new Mesh(new Vertex[]{ //creates a rectangle
                new Vertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 0.0f)),  //top left corner
                new Vertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 1.0f)),  //top right corner
                new Vertex(new Vector3f(1.0f, 0.8f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 1.0f)),  //bottom right corner
                new Vertex(new Vector3f(1.0f, 0.8f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 0.0f)) //bottom left corner
        }, new int[]{ //
                0, 1, 2, //draws from vertex[0] to vertex[1] to vertex[2] (just a triangle)
                0, 3, 2, //draws from vertex[0] to vertex[3] to vertex[2] (finish rectangle)
        }, new Material("src/main/resources/textures/timer/timer1.png"));
        this.renderer.renderMesh(this.mesh);
        GL20.glUseProgram(0);
    }

}
