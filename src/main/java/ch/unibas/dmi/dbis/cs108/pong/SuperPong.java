package ch.unibas.dmi.dbis.cs108.pong;

import ch.unibas.dmi.dbis.cs108.io.Input;
import org.joml.Vector2f;


public abstract class SuperPong {
    Vector2f position;
    Vector2f velocity;
    public Vector2f size = new Vector2f(50, 100);

    final double xVel = 5;

    public SuperPong(Vector2f position, Vector2f velocity){
        this.position = position;
        this.velocity = velocity;
    }

    public abstract void update(double delta, Input input);



}
