package ch.unibas.dmi.dbis.cs108.pong;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Vector2f;
import org.lwjgl.glfw.GLFW;

public class Pong extends SuperPong{
    Texture tex;
    private boolean up = true;
    private boolean down = true;

    public Pong(){
        super(new Vector2f(1628,-300), new Vector2f(0,0));
    }


    public void initGraphics(){
        this.tex = new Texture("highlight.png");
    }
    @Override
    public void update(double delta, Input input) {
        velocity.y = 0;
        if (!input.isKeyDown(GLFW.GLFW_KEY_UP)) {
            down = true;
        }
        if (!input.isKeyDown(GLFW.GLFW_KEY_DOWN)) {
            up = true;
        }

        //MOVE LEFT
        if (input.isKeyDown(GLFW.GLFW_KEY_UP) && up && position.y < 0) {
            velocity.y = 10;
            down = false;
        }

        //MOVE RIGHT
        if (input.isKeyDown(GLFW.GLFW_KEY_DOWN) && down &&  position.y > -972) {

            velocity.y = -10;
            up = false;
        }

        position.add(velocity);

    }
}
