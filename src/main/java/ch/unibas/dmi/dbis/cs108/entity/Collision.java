package ch.unibas.dmi.dbis.cs108.entity;

import org.joml.Vector2f;


/**
 * @author daniel
 * Class used by the Hitbox class to save an collision event of two enities from the Entity class
 */
public class Collision {


    public Vector2f distance;
    public boolean overlap;


    /**
     * Constructor
     *
     * @param distance distance between two entities
     * @param overlap  boolean wether two game entities are overlapping/intersecting
     */
    public Collision(Vector2f distance, boolean overlap) {
        this.distance = distance;
        this.overlap = overlap;
    }


}
