package ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine;

import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import ch.unibas.dmi.dbis.cs108.world.ServerWorld;

/**
 * GameState used for the start of a lobby
 */
public class ServerGameStart implements GameState {
    private final ServerGame serverGame;

    public ServerGameStart(ServerGame serverGame) {
        this.serverGame = serverGame;
    }


    /**
     * empty method
     */
    @Override
    public void run() {

    }

    /**
     * method for initiating printers
     */
    @Override
    public void start() {
        serverGame.serverWorld = new ServerWorld("test", serverGame);

        int playerID = 0;
        /*
        for (ServerInThread serverInThread : serverGame.users) {
            serverGame.printers.add(serverInThread.getPrinter());
            playerID++;
        }

         */


        serverGame.setGameState(serverGame.getGameLobby());
        serverGame.nextState();

    }
}
