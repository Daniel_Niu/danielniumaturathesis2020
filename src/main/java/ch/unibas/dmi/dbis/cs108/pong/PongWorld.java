package ch.unibas.dmi.dbis.cs108.pong;

import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.io.Display;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.opengl.GL11.*;

public class PongWorld {

    public int width = 1728;
    public int height = 972;
    public Matrix4f projection = new Matrix4f().setOrtho2D(0, width, -height, 0);
    Shader shader;

    Display display;
    Input input;
    boolean pause = false;
    Pong pong;
    Ping ping;
    Ball ball;



    public void start(){
        this.display = new Display(1728, 972 ,"Pong");
        display.create();
        this.input = display.getInput();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        shader = new Shader("shaders/defaultFragment.shader", "shaders/defaultVertex.shader");
        shader.create();

        pong = new Pong();
        ping = new Ping();
        ball = new Ball();
        pong.initGraphics();
        ping.initGraphics();
        ball.initGraphics();
        RectangleAssets.initAsset();

        run();
    }




    public void run(){
        long lastTime = System.nanoTime();
        final double amountOfTicks = 60D;
        double ns = 1000000000 / amountOfTicks;
        float delta = 0;

        //GameScreen loop
        while ((!display.shouldClose() && !input.isKeyDown(GLFW.GLFW_KEY_ESCAPE))) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1) {
                //update
                update( (1 / amountOfTicks), display.getInput());

                delta--;


            }
            display.update();



            glClearColor(0, 0, 0, 1);

            glClear(GL_COLOR_BUFFER_BIT);
            render();
            display.swapBuffers();

        }
    }

    public void update(Double delta, Input input){
        ping.update(delta, input);
        pong.update(delta, input);
        ball.update(delta, input);
        ball.checkCollision(ping);
        ball.checkCollision(pong);
    }

    public void render(){

        shader.bind();
        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", projection);
        shader.setUniform("screenPos", new Vector2f(ping.position));
        shader.setUniform("offset", new Vector4f(0, 0, 1, 1));
        shader.setUniform("pixelScale", new Vector2f(pong.size).mul(2));
        ping.tex.bind(0);
        RectangleAssets.getModel().render();
        shader.setUniform("screenPos", new Vector2f(pong.position));
        pong.tex.bind(0);
        RectangleAssets.getModel().render();
        shader.setUniform("screenPos", new Vector2f(ball.position));
        shader.setUniform("pixelScale", new Vector2f(ball.size).mul(2));

        ball.tex.bind(0);
        RectangleAssets.getModel().render();

    }

}
