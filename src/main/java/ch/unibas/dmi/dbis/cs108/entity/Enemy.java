package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;

/**
 * @author daniel
 * Enemy class, has everything to render a Player of a different Client
 */
public class Enemy extends Player {

    Logger logger = LogManager.getLogger(Enemy.class);


    private static final int ANIMATIONS = 3;
    private static final int ANIMATION_IDLE = 0;
    private static final int ANIMATION_WALK = 1;
    private static final int ANIMATION_MARKED = 2;


    /**
     * Constructor of Player
     *
     * @param ID       PlayerID
     * @param position Vector2d containing x and y value of the spawnlocation of this entity
     * @param width    player width
     * @param height   player height
     */
    public Enemy(String name, int ID, Vector2f position, float width, float height) {
        super(ID, name, position, width, height);
    }

    /**
     * update methode for each game tick
     *
     * @param delta       value defined by the tick limiter: how fast this tick is
     * @param input       Opengl input form the window/display (it is not used in this class, but is necessary for the
     *                    ClientPlayer class
     * @param clientWorld The world in which this entity is
     */
    @Override
    public void update(float delta, Input input, ClientWorld clientWorld) {
        Vector2f movement = new Vector2f(super.serverVelocity);
        movement = movement.mul(delta);

        Vector2f target = new Vector2f(super.serverPosition);

        Vector2f clientpos = hitbox.getCenter().add(movement);
        Vector2f distance = new Vector2f(target.x - clientpos.x, target.y - clientpos.y);
        target.sub(distance.mul(0.5f));
        moveTo(target);

        marking = super.isMarked();
        if (distance.x == 0) {
            useAnimation(ANIMATION_IDLE, 1);
        } else {
            if (distance.x > 0) {
                useAnimation(ANIMATION_WALK, -1);
            } else {
                useAnimation(ANIMATION_WALK, 1);
            }
        }





    }

    @Override
    public void update(float delta, PlayerStatus playerStatus, ClientWorld clientWorld) {

    }

    @Override
    public void update(float delta) {

    }

    public int getPlayerID(){
        return super.getPlayerID();
    }




}
