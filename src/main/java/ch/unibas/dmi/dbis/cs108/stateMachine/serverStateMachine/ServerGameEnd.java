package ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine;

import ch.unibas.dmi.dbis.cs108.Highscore;
import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import ch.unibas.dmi.dbis.cs108.utils.ArraysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Daniel Niu
 * GameState used for ending a lobby
 */
public class ServerGameEnd implements GameState {
    private final Logger logger = LogManager.getLogger(ServerGameEnd.class);

    private final ServerGame serverGame;

    ServerGameEnd(ServerGame serverGame) {
        this.serverGame = serverGame;
    }


    @Override
    public void run() {
    }

    @Override
    public void start() {

        List<ServerInThreadPlayer> copy = ArraysUtil.getCopy(serverGame.serverInThreadPlayers);

        copy.sort(Comparator.comparing(ServerInThreadPlayer::getScore));
        Collections.reverse(copy);

        StringBuilder sb = new StringBuilder();

        for (ServerInThreadPlayer serverInThreadPlayer : copy) {
            sb.append(serverInThreadPlayer.getName()).append(";").append(serverInThreadPlayer.getScore());
        }
        if (serverGame.shouldClose()) {
            return;
        }


        //tell every connected client that  the game has ended
        serverGame.printAll(Protocol.END.name(), sb.toString());
        logger.debug("Game in Lobby " + serverGame.lobbyID + " has ended");
        ServerInThreadPlayer winner = (ServerInThreadPlayer) copy.toArray()[0];
        Highscore.addWin(winner.getName());
        //opens the lobby, so that a new game can start
        serverGame.server.lobbies.get(serverGame.lobbyID).setInLobby();


    }
}
