package ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine;

import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.gui.StartChoiceGUI;
import ch.unibas.dmi.dbis.cs108.io.Display;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;
import org.lwjgl.glfw.GLFW;

import java.awt.*;

import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author Daniel Niu
 * GameState class used to initiate the Client
 */
public class Start implements GameState {
    private final Logger logger = LogManager.getLogger(Start.class);

    private final ClientGameMachine clientGameMachine;
    private Display display;
    private Input input;
    public Color textColor = Color.white;
    public char[] c;
    public float xTemp;
    private Font font;
    private int i;
    public Shader fontShader;

    Start(ClientGameMachine gm) {

        clientGameMachine = gm;

    }

    /**
     * Start screen containing the game logo, waiting to be pressed
     */
    @Override
    public void run() {
        //StartScreen.init();
        Texture start = new Texture("START.png");
        glDisable(GL_DEPTH_TEST);
        Shader shader = clientGameMachine.shader;
        RectangleAssets.initAsset();


        long lastTime = System.nanoTime();
        final double amountOfTicks = 60D;
        double ns = 1000000000 / amountOfTicks;
        float delta = 0;

        glDisable(GL_DEPTH_TEST);
        while (!display.shouldClose() && !input.isKeyDown(GLFW.GLFW_KEY_ESCAPE)) {

            display.update();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glClearColor(0, 0, 0, 1);
            //StartScreen.render();
            Matrix4f projection = new Matrix4f().ortho2D(-1, 1, -1, 1);

            shader.bind();
            shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
            shader.setUniform("sampler", 0);
            shader.setUniform("screenPos", new Vector2f(0, 0));
            shader.setUniform("offset", new Vector4f(0f, 0f, 1f, 1f));
            shader.setUniform("projection", projection);
            shader.setUniform("pixelScale", new Vector2f(2, 2));

            start.bind(0);
            RectangleAssets.getModel().render();


            display.swapBuffers();

            //if Mouse Leftclick on "Start-Picture" end loop
            if (input.isButtonDown(org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT)) {
                if ((input.getMouseX() >= 320 && input.getMouseY() <= 540) && (input.getMouseX() >= 320 && input.getMouseY() >= 180)
                        && (input.getMouseX() <= 1000 && input.getMouseY() <= 540) && (input.getMouseX() <= 1000 && input.getMouseY() >= 180)) {
                    break;
                }
            }


        }

        if (display.shouldClose()) {
            clientGameMachine.closeClient();
        }
        // if shouldClose or Escape is pressed or click on Startpicture, destroy window and terminate
        //StartScreen.close();


        StartChoiceGUI startChoiceGUI = new StartChoiceGUI(clientGameMachine);
        logger.debug("Serve/Client screen initialized.");
        while (!display.shouldClose()) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            if (delta >= 1) {
                //adjusting the values for the Hitboxes
                startChoiceGUI.update((input.getMouseX() / display.getWidth()) * clientGameMachine.displayWidth,
                        (input.getMouseY() / display.getHeight()) * clientGameMachine.displayHeight,
                        input.isButtonDown(GLFW_MOUSE_BUTTON_LEFT));

                startChoiceGUI.render(shader, clientGameMachine.bigFont);

                delta--;
            }

            display.update();


            glClearColor(0, 0, 0, 1);
            glClear(GL_COLOR_BUFFER_BIT);
            startChoiceGUI.render(shader, clientGameMachine.bigFont);

            display.swapBuffers();

        }
        logger.debug("left client server");

        RectangleAssets.initAsset();


    }


    /**
     * initiation of Opengl attributes and creation of the shader
     */
    @Override
    public void start() {
/*
        font = new Font(new java.awt.Font("TimesRoman", java.awt.Font.PLAIN, 64));
        glBindTexture(GL_TEXTURE_2D, font.ID());
        Map<Character, Glyph> chars = font.getCharacters();
        c = text.toC*/


        clientGameMachine.disp = new Display(clientGameMachine.displayWidth, clientGameMachine.displayHeight, "Deadline-Escape");

        display = clientGameMachine.disp;

        clientGameMachine.disp.setBackgroundColor(255.0f, 255.0f, 255.0f);

        clientGameMachine.disp.create();
        this.input = clientGameMachine.disp.getInput();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        clientGameMachine.shader = new Shader("shaders/defaultFragment.shader", "shaders/defaultVertex.shader");
        clientGameMachine.shader.create();

        font = new Font(new java.awt.Font("ARIAL", java.awt.Font.PLAIN, 50));
        clientGameMachine.bigFont = font;
        clientGameMachine.smallFont = new Font(new java.awt.Font("ARIAL", java.awt.Font.PLAIN, 30));
        clientGameMachine.verysmallFont = new Font(new java.awt.Font("ARIAL", java.awt.Font.PLAIN, 15));


        run();


        clientGameMachine.setGameState(clientGameMachine.getClientLobby());
        clientGameMachine.nextState();

    }

}
