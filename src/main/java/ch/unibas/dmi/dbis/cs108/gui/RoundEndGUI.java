package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.Player;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.utils.ArraysUtil;
import org.joml.Vector2f;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to show the result screen between rounds
 */
public class RoundEndGUI {
    private final ClientGameMachine clientGameMachine;
    private final Label roundTitle;
    private final List<GuiObject> guiObjectList = new ArrayList<>();
    private final List<Label> labels = new ArrayList<>();
    private final float width;
    private final float height;

    /**
     * Constructor
     *
     * @param clientGameMachine the clientGameMachine this screen is shown for
     */
    public RoundEndGUI(ClientGameMachine clientGameMachine) {
        this.clientGameMachine = clientGameMachine;
        this.width = clientGameMachine.displayWidth;
        this.height = clientGameMachine.displayHeight;
        for (int i = 0; i < clientGameMachine.players.length; i++) {
            if (clientGameMachine.players[i] != null) {
                Player p = clientGameMachine.players[i];
                float playercount = ArraysUtil.getLength(clientGameMachine.players);

                SkinButton skinButton = new SkinButton(i, p.getSkinID(), new Vector2f(((float) i * width) / playercount + width / (2f * playercount), height / 2f), 240, 300);
                Label label = new Label(i, p.getName() + ": " + p.getScore(), Color.white, new Vector2f(((float) i * width) / playercount + width / (2f * playercount) - 50, height / 2f + 200));
                labels.add(label);
                guiObjectList.add(skinButton);
            }


        }
        roundTitle = new Label(-1, "Result of round " + clientGameMachine.currentRound, Color.white, new Vector2f(500, 200));

    }

    /**
     * updates all objects in the round end screen
     * @param input the mouse input
     */
    public void update(Input input) {
        for (GuiObject guiObject : guiObjectList) {
            guiObject.update(input);
        }

    }

    /**
     * renders all objects in the screen
     * @param shader the shader used
     * @param font the font used for text
     */
    public void render(Shader shader, Font font) {
        for (GuiObject guiObject : guiObjectList) {

            guiObject.render(shader, clientGameMachine, clientGameMachine.smallFont);
        }
        for (Label label : labels) {
            if (clientGameMachine.players[label.id] != null) {
                Player p = clientGameMachine.players[label.id];
                label.changeText(p.getName() + ": " + p.getScore());
                label.render(shader, clientGameMachine, clientGameMachine.smallFont);
            }

        }

        roundTitle.changeText("Result of round " + clientGameMachine.currentRound);
        roundTitle.render(shader, clientGameMachine, clientGameMachine.bigFont);


    }
}
