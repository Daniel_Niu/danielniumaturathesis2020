package ch.unibas.dmi.dbis.cs108.shaderutils;

import ch.unibas.dmi.dbis.cs108.io.Timer;


/**
 * @author Daniel Niu
 * Class for reoccuring Animations
 */
public class LoopAnimation {
    private final Texture[] frames;
    private int textureID;

    private double elapsedTime;
    private double currentTime;
    private double lastTime;
    private final double frameLength;

    /**
     * Constructor
     *
     * @param frameNumber the number of frames in the animation
     * @param frameLength the number of frames shown per second
     * @param filename    the ame of the animation file
     */
    public LoopAnimation(int frameNumber, int frameLength, String filename) {
        this.textureID = 0;
        this.elapsedTime = 0;
        this.currentTime = 0;
        this.lastTime = Timer.getTime();
        this.frameLength = 1.0 / frameLength;

        this.frames = new Texture[frameNumber];
        for (int i = 0; i < frameNumber; i++) {
            this.frames[i] = new Texture(filename + "/" + i + ".png");
        }
    }

    /**
     * bind the right texture to the sampler depending on the elapsed time
     *
     * @param sampler OpenGL sampler id
     */
    public void bind(int sampler) {
        this.currentTime = Timer.getTime();
        this.elapsedTime += currentTime - lastTime;

        //if elapsed time has exceeded frame length, then the texture will be swapped
        if (elapsedTime >= frameLength) {
            elapsedTime = 0;
            textureID++;
        }

        //if the re-occuring animation has reached its last texture
        if (textureID >= frames.length) textureID = 0;

        this.lastTime = currentTime;

        frames[textureID].bind(sampler);
    }
    }
