package ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine;

import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;

/**
 * GameState used for the game lobby
 */
public class ServerGameLobby implements GameState {
    private final ServerGame serverGame;


    ServerGameLobby(ServerGame serverGame) {
        this.serverGame = serverGame;
    }

    @Override
    public void start() {
        run();
    }

    /**
     * //TODO add map, skin selection
     */
    @Override
    public void run() {

        //wait loop for skinselections
        while (serverGame.start) {
            if (serverGame.shouldClose()) {
                return;
            }
            serverGame.start = false;
            //if one player isn't ready the loop starts agains
            for (ServerInThreadPlayer serverInThreadPlayer : serverGame.serverInThreadPlayers) {
                if (serverInThreadPlayer != null) {
                    if (!serverInThreadPlayer.serverInThread.isRdy()) {
                        serverGame.start = true;
                        break;
                    }
                }

            }


        }
        printSkinChoices();
        serverGame.setGameState(serverGame.getGameRun());
        serverGame.nextState();
    }

    /**
     * sends skin information via protocol to the clients
     */
    private void printSkinChoices() {
        StringBuilder sb = new StringBuilder();


        for (ServerInThreadPlayer serverInThreadPlayer : serverGame.serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                int id = serverInThreadPlayer.serverInThread.playerID;
                int skinID = serverInThreadPlayer.serverInThread.skinID;
                sb.append(id).append(";").append(skinID).append(";");
            }
        }

        serverGame.printAll(Protocol.SKIN.name(), sb.toString());
    }


}
