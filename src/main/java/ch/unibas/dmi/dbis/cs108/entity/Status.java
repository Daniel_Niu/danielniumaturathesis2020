package ch.unibas.dmi.dbis.cs108.entity;

import org.joml.Vector2f;

/**
 * @author Daniel Niu
 * abstract class for statuses of different game entities
 */
abstract class Status {
    PositionSize positioning;
    Vector2f movement;

    /**
     * Constructor
     *
     * @param positioning Position of the start position
     * @param movement    Vectur2f current 2D movement
     */
    Status(PositionSize positioning, Vector2f movement) {
        this.positioning = positioning;
        this.movement = movement;
    }


}
