package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.Player;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.utils.ArraysUtil;
import org.joml.Matrix4f;
import org.joml.Vector2f;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class used to show the Endscreen of the game, containing all players and their scores
 */
public class EndGUI {

    private final ClientGameMachine clientGameMachine;
    private final Label roundTitle;
    private final List<GuiObject> guiObjectList = new ArrayList<>();
    List<Label> labels = new ArrayList<>();
    private final List<GuiRenderObject> guiRenderObjects = new ArrayList<>();
    private final List<GuiUpdatableObject> guiUpdatableObjects = new ArrayList<>();
    private final Label winner;
    private final MenuButton menu;
    private final Button button;
    private final float width;
    private final float height;

    /**
     * Constructor
     *
     * @param clientGameMachine the clientGameMachine this EndGUI is connected to
     */
    public EndGUI(ClientGameMachine clientGameMachine) {
        this.clientGameMachine = clientGameMachine;
        this.width = clientGameMachine.displayWidth;
        this.height = clientGameMachine.displayHeight;
        Matrix4f projection = new Matrix4f().setOrtho2D(0, clientGameMachine.displayWidth, clientGameMachine.displayHeight, 0);

        menu = new MenuButton(-1, new Vector2f(1175, 50), 200, 100);
        button = new Button(new Vector2f(1175, 50), projection, 200, 100);
        Texture[] texArray = new Texture[3];
        texArray[0] = new Texture("menubutton/0.png");
        texArray[1] = new Texture("menubutton/1.png");
        texArray[2] = new Texture("menubutton/2.png");
        button.setTexture3t(texArray);
        button.addActionEvent(() -> clientGameMachine.endScreen = false);

        winner = new Label(-1, "Winner", Color.white, new Vector2f(250, 50));


        List<Player> copy = ArraysUtil.getCopy(clientGameMachine.players);
        copy.sort(Comparator.comparing(Player::getScore));
        Collections.reverse(copy);

        Player p = copy.get(0);
        SkinButton skinButton = new SkinButton(0, p.getSkinID(), new Vector2f(300, 300), 400, 400);


        Label label = new Label(0, p.getName() + ": " + p.getScore(), Color.white, new Vector2f(230, 125));

        guiRenderObjects.add(button);
        guiUpdatableObjects.add(button);
        guiObjectList.add(skinButton);
        guiObjectList.add(label);

        for (int i = 1; i < ArraysUtil.getLength(clientGameMachine.players); i++) {
            p = clientGameMachine.players[i];
            skinButton = new SkinButton(i, p.getSkinID(), new Vector2f(((float) i * width) / ArraysUtil.getLength(clientGameMachine.players) + width / (2f * ArraysUtil.getLength(clientGameMachine.players)), (height / 4f) * 3), 100, 100);
            label = new Label(i, p.getName() + ": " + p.getScore(), Color.white, new Vector2f(((float) i * width) / ArraysUtil.getLength(clientGameMachine.players) + width / (2f * ArraysUtil.getLength(clientGameMachine.players)) - 70, (height / 4f) * 3 - 100));
            guiObjectList.add(label);
            guiObjectList.add(skinButton);
        }
        roundTitle = new Label(-1, "Result of round " + clientGameMachine.currentRound, Color.white, new Vector2f(500, 200));

    }


    /**
     * updates components
     *
     * @param mouseX    x coord
     * @param mouseY    y coord
     * @param mouseLeft boolean left mouse button
     */
    public void update(double mouseX, double mouseY, boolean mouseLeft) {
        for (GuiUpdatableObject guiUpdatableObject : guiUpdatableObjects) {
            guiUpdatableObject.update(mouseX, mouseY, mouseLeft);
        }
    }

    /**
     * renders the end screen
     * @param shader the shader used
     * @param font the font used
     */
    public void render(Shader shader, Font font) {
        menu.render(shader, clientGameMachine, font);
        winner.render(shader, clientGameMachine, clientGameMachine.bigFont);
        for (GuiRenderObject guiRenderObject : guiRenderObjects) {
            guiRenderObject.render(shader);
        }

        for (GuiObject guiObject : guiObjectList) {
            guiObject.render(shader, clientGameMachine, clientGameMachine.smallFont);
        }

    }

}
