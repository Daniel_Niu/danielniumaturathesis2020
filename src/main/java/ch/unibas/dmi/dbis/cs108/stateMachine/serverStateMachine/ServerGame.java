package ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine;

import ch.unibas.dmi.dbis.cs108.config.DeadlineEscapeConf;
import ch.unibas.dmi.dbis.cs108.entity.Item;
import ch.unibas.dmi.dbis.cs108.entity.Portal;
import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.server.Server;
import ch.unibas.dmi.dbis.cs108.server.ServerInThread;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import ch.unibas.dmi.dbis.cs108.utils.ArraysUtil;
import ch.unibas.dmi.dbis.cs108.world.ServerWorld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * @author Daniel Niu
 * StateMachine used for a lobby which is in game
 */
public class ServerGame implements Runnable {

    public final int rounds = DeadlineEscapeConf.ROUND_NUMBER;
    final int roundDuration = DeadlineEscapeConf.ROUND_DURATION;
    public Server server;
    public int lobbyID;
    public int markedID;
    boolean running = true;
    //public List<ServerInThread> users;
    //public ServerInThread[] serverInThreads;
    //public List<PrintWriter> printers = new ArrayList<>();
    ServerWorld serverWorld;
    public Item[] items = new Item[2];
    public List<Boolean> itemPlaces = new ArrayList<>();
    public List<Portal> portals = new ArrayList<>();
    //public List<ServerPlayer> players = new ArrayList<>();

    public ServerInThreadPlayer[] serverInThreadPlayers = new ServerInThreadPlayer[5];


    //public ServerPlayer[] serverPlayers = new ServerPlayer[5];
    public int width = 1280, height = 720;
    public int worldWidth, worldHeight;
    public boolean start = true;
    public int time = 0;
    private final GameState serverGameStart;
    public String mapName;
    Logger logger = LogManager.getLogger(ServerGame.class);
    private final GameState serverGameLobby;
    private final GameState serverRun;
    private final GameState serverGameEnd;
    private boolean end = false;
    private GameState gameState;

    /**
     * starts our game thread
     *
     * @param lobbyID ID of the lobby in which the game is
     * @param users   List of all ServerInThreads that are connected to this game
     * @param server  the server
     */
    private ServerGame(int lobbyID, List<ServerInThread> users, Server server) {
        this.lobbyID = lobbyID;
        //this.users = users;
        this.server = server;
        this.serverGameStart = new ServerGameStart(this);
        this.serverGameLobby = new ServerGameLobby(this);
        this.serverRun = new ServerGameRun(this);
        this.serverGameEnd = new ServerGameEnd(this);
        logger.debug("initialized server game");

        this.gameState = serverGameStart;
    }

    /**
     * Constructor
     * starts the game thread
     * @param lobbyID ID of the lobby in which the game is
     * @param users Array of all ServerInThreads that are connected to this game
     * @param server the server
     */
    public ServerGame(int lobbyID, ServerInThread[] users, Server server) {
        this.lobbyID = lobbyID;

        for (int i = 0; i < users.length; i++) {
            if (users[i] != null) {
                serverInThreadPlayers[i] = new ServerInThreadPlayer(users[i]);
            }
        }
        this.server = server;

        this.serverGameStart = new ServerGameStart(this);
        this.serverGameLobby = new ServerGameLobby(this);
        this.serverRun = new ServerGameRun(this);
        this.serverGameEnd = new ServerGameEnd(this);


        this.gameState = serverGameStart;
    }

    void setGameState(GameState newGameState) {
        gameState = newGameState;
    }

    public void start() {
        gameState.start();
    }

    public void nextState() {
        gameState.start();
    }

    public GameState getStart() {
        return serverGameStart;
    }

    public GameState getGameLobby() {
        return serverGameLobby;
    }

    public GameState getGameRun() {
        return serverRun;
    }

    public GameState getGameEnd() {
        return serverGameEnd;
    }

    /**
     * run method for the Runnable interface
     */
    @Override
    public void run() {
        gameState.start();
    }

    public void resetRoundScore() {

        for (ServerInThreadPlayer serverInThreadPlayer : serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                serverInThreadPlayer.serverPlayer.resetRoundScore();
            }

        }
    }


    /**
     * add Score to every player depending on their performance form the last round
     * the lower the round score, the higher the point. However, if you were marked you always receive 0 points.
     * the last place that wasn't marked receives 20 points and the second last place 40 and so on...
     */
    public void setPoints() {
        List<ServerInThreadPlayer> copy = ArraysUtil.getCopy(serverInThreadPlayers);

        copy.remove(serverInThreadPlayers[markedID]);

        copy.sort(Comparator.comparing(ServerInThreadPlayer::getRoundScore));
        Collections.reverse(copy);


        for (int i = 0; i < copy.size(); i++) {
            copy.get(i).addScore((i + 1) * 20);
        }

        StringBuilder sb = new StringBuilder();


        for (ServerInThreadPlayer serverInThreadPlayer : serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                sb.append(serverInThreadPlayer.getPlayerID()).append(";");
                sb.append(serverInThreadPlayer.getScore()).append(";");
            }

        }

        printAll(Protocol.ENDROUND.name(), sb.toString());
    }

    public boolean shouldClose() {
        return end;
    }


    /**
     * starts the next round
     * @param i the round number as a string
     */
    public void nextRound(int i) {
        printAll(Protocol.ROUND.name(), Integer.toString(i));
    }

    /**
     * Print to every Client in this lobby
     *
     * @param prot Protocol
     * @param msg  arguments for Protocol
     */
    public void printAll(String prot, String msg) {
        for (ServerInThreadPlayer serverInThreadPlayer : serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                serverInThreadPlayer.serverInThread.getPrinter().println(prot + " " + msg);
                serverInThreadPlayer.serverInThread.getPrinter().flush();
            }
        }
    }

    /**
     * Removes a player from the current game
     * @param id the id of the player, who gets removed
     */
    public void removePlayer(int id) {
        serverInThreadPlayers[id] = null;
        if (ArraysUtil.getLength(serverInThreadPlayers) == 0) {
            end = true;
            printAll(Protocol.END.name(), "filler");
        }
        printAll(Protocol.REMOVE.name(), Integer.toString(id));
    }

    /**
     * Determines the winner of the game
     * @return the name of the winning player
     */
    public String getWinner() {
        List<ServerInThreadPlayer> copy = ArraysUtil.getCopy(serverInThreadPlayers);

        copy.sort(Comparator.comparing(ServerInThreadPlayer::getRoundScore));
        return copy.get(0).getName();
    }

    /**
     * Gets the LobbyID of the current game
     * @return the ID of the lobby
     */
    public int getLobbyID() {
        return lobbyID;
    }


}
