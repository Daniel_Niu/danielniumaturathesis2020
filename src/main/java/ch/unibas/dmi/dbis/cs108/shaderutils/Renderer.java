package ch.unibas.dmi.dbis.cs108.shaderutils;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

/**
 * our Render-class
 * used to show our Mesh and Texture on the Display
 */
public class Renderer {
    private final Shader shader;

    /**
     * Constructor
     * @param shader from our shader-class
     */
    public Renderer(Shader shader) {
        this.shader = shader;
    }
    /**
     * rendering our Mesh with our texture
     * @param mesh Mesh which we have to initialize before
     */
    public void renderMesh(Mesh mesh) {

        //bind mesh to our VertexArray
        GL30.glBindVertexArray(mesh.getVAO());

        //enable VertexArray[0], VertexArray[1], VertexArray[2]
        GL30.glEnableVertexAttribArray(0);
        GL30.glEnableVertexAttribArray(1);
        GL30.glEnableVertexAttribArray(2);

        //enable our indices
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, mesh.getIBO());

        //activates GL Textures
        GL13.glActiveTexture(GL13.GL_TEXTURE0);

        //binds our texture from mesh as a GL 2D texture
        GL13.glBindTexture(GL_TEXTURE_2D, mesh.getMaterial().getTextureID());
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //gives all needed informations from our texture to LWJGL
        glTexImage2D(GL_TEXTURE_2D, 0 ,GL_RGBA, (int)mesh.getMaterial().getWidth(), (int)mesh.getMaterial().getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, mesh.getMaterial().getBuffer());
        glGenerateMipmap(GL_TEXTURE_2D);

        //binds our shader
        shader.bind();

        //draw our mesh with texture
        GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getIndices().length, GL11.GL_UNSIGNED_INT, 0);

        //unbinds our shader
        shader.unbind();

        //unbind our indices
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

        //disable VertexArray[0], VertexArray[1], VertexArray[2]
        GL30.glDisableVertexAttribArray(0);
        GL30.glDisableVertexAttribArray(1);
        GL30.glDisableVertexAttribArray(2);

        //unbind our VertexArray
        GL30.glBindVertexArray(0);
    }
}
