package ch.unibas.dmi.dbis.cs108.gui;

public interface ClickedFunction {
    void exec();
}
