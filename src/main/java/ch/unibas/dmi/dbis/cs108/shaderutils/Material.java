package ch.unibas.dmi.dbis.cs108.shaderutils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL13;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.glGenTextures;

/**
 * this class creates a Material. loads a PNG texture
 */
public class Material {
    private final Logger logger = LogManager.getLogger(Material.class);
    private final String path;
    private float width, height;
    private int textureID;
    private IntBuffer buffer;

    /**
     * Constructor of Material
     *
     * @param path String path where the texture is located
     */
    public Material(String path) {
        this.path = path;
    }

    /**
     * creates a Material
     */
    public void create() {

        try {
            //try to read path and convert it into a BufferedImage
            BufferedImage image = ImageIO.read(new FileInputStream(path));

            //get width & heigth from our BufferedImage
            width = image.getWidth();
            height = image.getHeight();

            //create a int-Array with length = width*height
            int []pixels = new int[(int) (width*height)];

            //get RGB values from our bufferedImage
            image.getRGB(0, 0, (int)width, (int)height, pixels, 0, (int)width);

            //create a new int-Array with same length as pixels
            int[] data = new int[(int) (width*height)];

            //for-loop store rgb values into data
            for(int i = 0; i<width*height;i++){
                int a = (pixels[i] & 0xff000000)>>24;
                int r = (pixels[i] & 0xff0000)>>16;
                int g = (pixels[i] & 0xff00)>>8;
                int b = (pixels[i] & 0xff);

                data[i] = a << 24 | b <<16 | g << 8 | r;
            }
            //fills rgb values from data into IntBuffer
            buffer = ByteBuffer.allocateDirect(data.length << 2).order(ByteOrder.nativeOrder()).asIntBuffer();

            //flip our buffer for LWJGL
            buffer.put(data).flip();

            //mark textureID to be used by GenTextures
            textureID = glGenTextures();

        } catch (IOException e) {
            //if no png file is found at path, return path and repository where path was searched
            logger.error("no texture at: " + path + " " + (System.getProperty("user.dir")));

        }
    }

    /**
     * destroys our loaded Texture
     */
    public void destroy() {
        GL13.glDeleteTextures(textureID);
    }

    /**
     * height-Getter
     * @return height of texture
     */
    public float getHeight() {
        return height;
    }

    /**
     * width-Getter
     * @return width of texture
     */
    public float getWidth() {
        return width;
    }

    /**
     * textureID-Getter
     * @return textureID of texture.
     */
    public int getTextureID() {
        return textureID;
    }

    /**
     * buffer-Getter
     * @return IntBuffer where our RGB values are stored
     */
    public IntBuffer getBuffer() {
        return buffer;
    }
}
