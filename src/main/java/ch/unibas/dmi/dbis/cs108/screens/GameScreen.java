package ch.unibas.dmi.dbis.cs108.screens;


import ch.unibas.dmi.dbis.cs108.entity.Enemy;
import ch.unibas.dmi.dbis.cs108.io.ClientInThread;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Renderer;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;

import java.util.List;

/**
 * GameScreen class
 * sets up our gamescreen
 */
@SuppressWarnings("SpellCheckingInspection")
class GameScreen {


    public static Renderer bgRenderer;
    private static Shader shader;
    public static List<Enemy> players;
    public static Shader platformShader;

    private ClientInThread cIT;
    private ClientWorld clientWorld;
    private ClientGameMachine clientGameMachine;


    /**
     * Constructor
     *
     * @param cIT the client this screen belongs to
     */
    public GameScreen(ClientInThread cIT) {
        this.cIT = cIT;
    }

    /**
     * Constructor
     * @param gm the client this screen belongs to
     */
    public GameScreen(ClientGameMachine gm) {
        this.clientGameMachine = gm;
    }

    public void update(Float delta, Input input, ClientGameMachine clientGameMachine) {
        clientWorld.update(delta, input, clientGameMachine);
    }

    /**
     * renders the world
     *
     * @param shader            Shader which renders this
     * @param clientGameMachine client to which this is connected to
     */
    public void render(Shader shader, ClientGameMachine clientGameMachine) {

        clientWorld.render(shader, clientGameMachine);
    }


    public void render(Shader shader) {

        clientWorld.render(shader, clientGameMachine);
    }

    /**
     * close our gamescreen and destroy the meshs
     * Sysoutprint Players scores
     */
    public void close() {
        shader.destroy();
        clientGameMachine.clientInThread.requestHS();
    }
}
