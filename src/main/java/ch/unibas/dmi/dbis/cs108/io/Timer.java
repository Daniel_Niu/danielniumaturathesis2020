package ch.unibas.dmi.dbis.cs108.io;

/**
 * a simple timer
 */
public class Timer {

    /**
     * gets the time
     * @return the current time in seconds
     */
    public static double getTime() {
        return (double) System.nanoTime() / (double) 1000000000L;
    }
}
