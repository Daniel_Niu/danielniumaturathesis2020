package ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine;

import ch.unibas.dmi.dbis.cs108.entity.ServerPlayer;
import ch.unibas.dmi.dbis.cs108.server.ServerInThread;

/**
 * the player class connects a player object with a serverthread
 */
public class ServerInThreadPlayer {
    public ServerInThread serverInThread;
    public ServerPlayer serverPlayer;


    /**
     * Constructor
     *
     * @param serverInThread the serverthread
     */
    public ServerInThreadPlayer(ServerInThread serverInThread) {
        this.serverInThread = serverInThread;
    }

    /**
     * assigns a player to this class
     * @param serverPlayer the player object
     */
    public void assignPlayer(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    /**
     * roundScore-Getter
     * @return the score of the player for the current round
     */
    public int getRoundScore() {
        return serverPlayer.getRoundScore();
    }

    /**
     * adds the specified number to the total score
     * @param amount the score increase
     */
    public void addScore(int amount) {
        this.serverPlayer.addScore(amount);
    }

    /**
     * PlayerID-Getter
     * @return the ID of the player
     */
    public int getPlayerID() {
        return serverInThread.playerID;
    }

    /**
     * Score-Getter
     * @return the total score of the player
     */
    public int getScore() {
        return serverPlayer.getScore();
    }

    /**
     * Name-Getter
     * @return the name of the player
     */
    public String getName() {
        return serverInThread.getName();
    }
}
