package ch.unibas.dmi.dbis.cs108.shaderutils;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

public class Model {
    private final int drawCount;

    private final int vertexID;
    private final int textureID;

    private final int indexObject;

    /**
     * Class used to draw elements with the vertex shader
     *
     * @param vertices   vertices of the object, like edges
     *                   example {0.5, -0.5} representing the offset from the center for one edge
     * @param tex_coords where the texture should stretch over
     * @param indices    which vertices should be connected
     *                   example {0 , 1 , 2}     connect edge 0 to 1 then connect 1 to 2
     */
    public Model(float[] vertices, float[] tex_coords, int[] indices) {
        drawCount = indices.length;

        vertexID = glGenBuffers();
        //binds to the GL_ARRAY_BUFFER vertexObject to buffer data
        glBindBuffer(GL_ARRAY_BUFFER, vertexID);
        glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(vertices), GL_STATIC_DRAW);

        textureID = glGenBuffers();
        //binds to the GL_ARRAY_BUFFER textureCoordObject to buffer data
        glBindBuffer(GL_ARRAY_BUFFER, textureID);
        glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(tex_coords), GL_STATIC_DRAW);

        indexObject = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexObject);

        IntBuffer buffer = BufferUtils.createIntBuffer(indices.length);
        buffer.put(indices);
        buffer.flip();

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);


        //unbind the two type of buffers to clear the memory
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    /**
     * Draw VertexBufferObject
     */
    public void render() {

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vertexID);
        //3 for v3 vector
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, textureID);
        //2 for v2 vector
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

        //GL_ELEMENT_ARRAY_BUFFER to indicate that these are indices for the other GL_ARRAY_BUFFER
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexObject);
        glDrawElements(GL_TRIANGLES, drawCount, GL_UNSIGNED_INT, 0);


        //unbind buffers
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //disable vertex drawing
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);

    }

    /**
     * glDelete content
     */
    public void destroy() {
        glDeleteBuffers(vertexID);
        glDeleteBuffers(textureID);
        glDeleteBuffers(indexObject);
    }

    /**
     * Turns float array into FloatBuffer
     *
     * @param data float[] to be turned into a floatbuffer
     * @return flipped FloatBuffer
     */
    private FloatBuffer createFloatBuffer(float[] data) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        return buffer;
    }

}
