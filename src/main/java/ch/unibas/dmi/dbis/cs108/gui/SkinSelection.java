package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Niu
 * Class for the skin selection screen
 */
public class SkinSelection {

    private final GuiImage background;
    private final NewLabel l;
    private final List<GuiRenderObject> guiRenderObjects = new ArrayList<>();
    private final List<GuiUpdatableObject> guiUpdatableObjects = new ArrayList<>();

    /**
     * Constructor
     * creates the skin selection screen with buttons to choose the skins
     *
     * @param clientGameMachine the clientGameMachine this screen is shown in
     */
    public SkinSelection(ClientGameMachine clientGameMachine) {
        background = new GuiImage(new Texture("bg.png"), new Vector2f(0, -0),
                new Matrix4f().ortho2D(-1, 1, -1, 1), 2, 2);


        Matrix4f projection = new Matrix4f().setOrtho2D(0, clientGameMachine.displayWidth, clientGameMachine.displayHeight, 0);
        int SKIN_COUNT = 5;
        for (int i = 0; i < SKIN_COUNT; i++) {
            Button button = new Button(new Vector2f(((i + 0.5f) * clientGameMachine.displayWidth) / SKIN_COUNT, clientGameMachine.displayHeight * 0.75f - 50),
                    projection, 200, 200);
            Texture[] texArray = new Texture[3];
            texArray[0] = new Texture("skinButton/" + i + "/0.png");
            texArray[1] = new Texture("skinButton/" + i + "/1.png");
            texArray[2] = new Texture("skinButton/" + i + "/2.png");
            button.setTexture3t(texArray);
            int finalI = i;

            button.addActionEvent(() -> {
                clientGameMachine.printer.println(Protocol.RDY + " " + finalI);
                clientGameMachine.printer.flush();
            });

            guiRenderObjects.add(button);
            guiUpdatableObjects.add(button);
        }

        NewLabel label = new NewLabel("CHOOSE YOUR APPEARANCE", new Vector4f(255, 255, 255, 1), new Vector2f((float) clientGameMachine.displayWidth / 3.8f, (float) clientGameMachine.displayHeight - 50),
                projection, clientGameMachine.bigFont);
        l = label;
        guiRenderObjects.add(label);
    }

    /**
     * updates all objects on the screen
     *
     * @param mouseX    mouse position x
     * @param mouseY    mouse position y
     * @param mouseLeft boolean if left mouse is pressed
     */
    public void update(double mouseX, double mouseY, boolean mouseLeft) {
        for (GuiUpdatableObject guiUpdatableObject : guiUpdatableObjects) {
            guiUpdatableObject.update(mouseX, mouseY, mouseLeft);
        }
    }

    /**
     * renders all objects on the screen
     *
     * @param shader the shader used
     */
    public void render(Shader shader) {


        background.render(shader);

        for (GuiRenderObject guiRenderObject : guiRenderObjects) {
            guiRenderObject.render(shader);
        }

        l.render(shader);
    }

}
