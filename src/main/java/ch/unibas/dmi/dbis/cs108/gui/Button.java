package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.Hitbox;
import ch.unibas.dmi.dbis.cs108.shaderutils.Positioning;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Matrix4f;
import org.joml.Vector2f;


public class Button extends AbstractButton implements GuiRenderObject, GuiUpdatableObject {
    private final Positioning positioning;

    Button(Vector2f screePos, Matrix4f projection, float xScale, float yScale) {
        super(new Hitbox(screePos, xScale, yScale));
        this.positioning = new Positioning(1, 1, screePos, projection, xScale, yScale);
    }

    void setTexture3t(Texture[] texture3t) {
        System.arraycopy(texture3t, 0, this.textures, 0, 3);
    }

    @Override
    public void render(Shader shader) {
        super.render(shader, positioning);
    }

    @Override
    public void update(double x, double y, boolean leftMouse) {
        super.update(new Vector2f((float) x, (float) y), leftMouse);
    }
}
