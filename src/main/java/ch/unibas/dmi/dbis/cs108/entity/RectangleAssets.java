package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.shaderutils.Model;


/**
 * @author Daniel Niu
 * Class used to generate a rectangular Model from the Model class
 */
public class RectangleAssets {
    private static Model model;

    public static Model getModel() {
        return model;
    }

    /**
     * initiate new Model with the vbo of a rectangle
     */
    public static void initAsset() {
        float[] vertices = new float[]{
                -0.5f, 0.5f,
                0.5f, 0.5f,
                0.5f, -0.5f,
                -0.5f, -0.5f,
        };
        float[] texture = new float[]{
                0, 0,
                1, 0,
                1,1,
                0, 1,
        };

        int[] indices = new int[]{
                0, 1, 2,
                2, 3, 0
        };

        model = new Model(vertices, texture, indices);
    }

    /**
     * deletes initiated Model
     */
    public static void deleteAsset() {
        if (model != null) {
            model.destroy();
            model = null;
        }

    }
}
