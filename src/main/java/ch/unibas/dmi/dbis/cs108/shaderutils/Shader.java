package ch.unibas.dmi.dbis.cs108.shaderutils;

import ch.unibas.dmi.dbis.cs108.utils.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

/**
 * this class is used to create a Shader
 * needs a FragmentShader and a VertexShader
 */
public class Shader {
    private final Logger logger = LogManager.getLogger(Shader.class);
    private final String fragmentFile;
    private final String vertexFile;
    private int fragmentID, vertexID, progID;

    /**
     * Constructor
     *
     * @param fragmentPath where our Fragment.glsl is located
     * @param vertexPath   where our Vertex.glsl is located
     */
    public Shader(String fragmentPath, String vertexPath) {
        fragmentFile = FileUtils.loadAsString(fragmentPath);
        vertexFile = FileUtils.loadAsString(vertexPath);
    }

    /**
     * creates our shader
     */
    public void create() {

        //creates a Program binded to our progID
        progID = glCreateProgram();

        //creates a VertexShader binded to our vertexID
        vertexID = glCreateShader(GL_VERTEX_SHADER);

        //gets Source for Shader and after, shader gets compiled
        glShaderSource(vertexID, vertexFile);
        glCompileShader(vertexID);

        //if Vertex Shader couldn't been compiled, return error
        if (glGetShaderi(vertexID, GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            logger.error("Vertex Shader");
            throw new IllegalArgumentException();
        }

        //creates a Fragment Shader binded to our fragment ID
        fragmentID = glCreateShader(GL_FRAGMENT_SHADER);

        //gets Source for Shader and after, shader gets compiled

        glShaderSource(fragmentID, fragmentFile);
        glCompileShader(fragmentID);

        //if Fragment Shader couldn't been compiled, return error
        if (glGetShaderi(fragmentID, GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            logger.error("Fragment Shader");
            throw new IllegalArgumentException();
        }

        //connect fragmentID (FragmentShader) with progID and vertexID (VertexShader) with progID
        glAttachShader(progID, vertexID);
        glAttachShader(progID, fragmentID);

        glBindAttribLocation(progID, 0, "vertices");
        glBindAttribLocation(progID, 1, "textures");

        //link our progID actually to a program
        glLinkProgram(progID);

        if(glGetProgrami(progID, GL_LINK_STATUS) != 1){
            System.err.println(glGetProgramInfoLog(progID));
            throw new UnsupportedOperationException();
        }
        glValidateProgram(progID);

        if(glGetProgrami(progID, GL_VALIDATE_STATUS) != 1){
            System.err.println(glGetProgramInfoLog(progID));
            throw new UnsupportedOperationException();
        }



        //delets our Vertex and Fragment Shader
        /*
        glDeleteShader(fragmentID);
        glDeleteShader(vertexID);
        */

    }

    /**
     * binds our Program
     * our progID can be used for a program
     */
    public void bind() {
        glUseProgram(progID);
    }

    /**
     * unbinds our program
     * the existing program is 0
     */
    public void unbind() {
        glUseProgram(0);
    }

    public void setUniform(String uniformName, Matrix4f value){
        int location = glGetUniformLocation(progID, uniformName);
        FloatBuffer mat = BufferUtils.createFloatBuffer(16);

        value.get(mat);
        if (location != -1) {
            glUniformMatrix4fv(location, false, mat);

        } else {
            System.err.println("Could not find Uniform: " + uniformName);
            throw new UnsupportedOperationException();
        }
    }

    public void setUniform(String uniformName, Vector3f value) {
        int location = glGetUniformLocation(progID, uniformName);
        if (location != -1) glUniform3f(location, value.x, value.y, value.z);

    }

    public void setUniform(String uniformName, Vector2f value) {
        int location = glGetUniformLocation(progID, uniformName);
        if (location != -1) glUniform2f(location, value.x, value.y);
    }

    public void setUniform(String uniformName, Vector4f value) {
        int location = glGetUniformLocation(progID, uniformName);
        if (location != -1) glUniform4f(location, value.x, value.y, value.z, value.w);
    }

    public void setUniform(String uniformName, Color value) {
        int location = glGetUniformLocation(progID, uniformName);
        if (location != -1) glUniform4f(location, value.getAlpha(), value.getRed(), value.getGreen(), value.getBlue());
    }


    public void setUniform(String uniformName, int value) {
        int location = glGetUniformLocation(progID, uniformName);
        if (location != -1) {
            glUniform1i(location, value);

        } else {
            System.err.println("Could not find Uniform: " + uniformName);
            throw new UnsupportedOperationException();
        }
    }


    /**
     * destroys our program
     * program doesnt exist afterwards
     */
    public void destroy() {
        glDetachShader(progID, vertexID);
        glDetachShader(progID, fragmentID);
        glDeleteShader(vertexID);
        glDeleteShader(fragmentID);
        glDeleteProgram(progID);
    }

    /**
     * progID-Getter
     * @return our progID
     */
    public int getProgID() {
        return progID;
    }
}
