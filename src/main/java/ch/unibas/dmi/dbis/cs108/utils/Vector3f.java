package ch.unibas.dmi.dbis.cs108.utils;

/**
 * A class representing a 3 dimensional Vector of the data type float.
 */
public class Vector3f {

    public float x, y, z;

    /**
     * Creates a three dimensional zero vector.
     */
    public Vector3f() {
        x = 0.0f;
        y = 0.0f;
        z = 0.0f;
    }

    /**
     * Creates a three dimensional vector with given parameters.
     * @param x float parameter for the x value of the vector.
     * @param y float parameter for the y value of the vector.
     * @param z float parameter for the z value of the vector.
     */
    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * sets the values inside a 3f Vector
     * @param x float parameter for the x value of the vector.
     * @param y float parameter for the y value of the vector.
     * @param z float parameter for the z value of the vector.
     */
    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * X- Getter
     * @return x float parameter for the x value of the vector.
     */
    public float getX() {
        return x;
    }

    /**
     * X- Setter
     * @param x float parameter for the x value of the vector.
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Y-Getter
     * @return y float parameter for the y value of the vector.
     */
    public float getY() {
        return y;
    }

    /**
     * Y- Setter
     * @param y float parameter for the y value of the vector.
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * Z- Getter
     * @return z float parameter for the z value of the vector.
     */
    public float getZ() {
        return z;
    }

    /**
     * Z- Setter
     * @param z float parameter for the z value of the vector.
     */
    public void setZ(float z) {
        this.z = z;
    }
}
