package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.Player;
import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

/**
 * class used to show all scorebars
 */
public class Scorebars extends GuiObject {

    private final Texture scorebar;
    private final Texture scorebarFilling;

    Scorebars() {
        super(-1, new Vector2f(0, 0), 0, 0);
        this.scorebar = new Texture("scorebar/scorebar.png");
        this.scorebarFilling = new Texture("scorebar/scorebarfilling.png");
    }


    @Override
    public void render(Shader shader, ClientGameMachine clientGameMachine, ch.unibas.dmi.dbis.cs108.font.Font font) {

        int count = 0;
        for (Player player : clientGameMachine.players) {
            if (player != null) {


                int scorelength = player.getRoundScore() / 30;

                Vector2f position = new Vector2f(100 + 220 * count, -10);


                shader.bind();
                Matrix4f projection = new Matrix4f().setOrtho2D(0, clientGameMachine.displayWidth, -clientGameMachine.displayHeight, 0);

                shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
                shader.setUniform("projection", projection);
                shader.setUniform("offset", new Vector4f(1, 1, 1, 1));
                shader.setUniform("sampler", 0);
                shader.setUniform("screenPos", new Vector2f(220 * count + 10 + scorelength / 2f, - 10));
                shader.setUniform("pixelScale", new Vector2f(10 + scorelength, 10));

                scorebarFilling.bind(0);
                RectangleAssets.getModel().render();


                shader.setUniform("screenPos", position);
                shader.setUniform("pixelScale", new Vector2f(200, 10));
                scorebar.bind(0);
                RectangleAssets.getModel().render();


                RectangleAssets.getModel().render();
                label(player.getName(), clientGameMachine.verysmallFont, shader, 5 + 220 * count, -32, new Vector4f(0, 0, 0, 1), projection, 1);
                count++;
            }
        }

    }
}
