package ch.unibas.dmi.dbis.cs108.server;

import ch.unibas.dmi.dbis.cs108.game.Lobby;
import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine.ServerGame;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * the ServerClass used to set up the socket and waiting for connections
 */
public class Server implements Runnable {
    //Logger
    private static final Logger logger = LogManager.getLogger(Server.class);
    private static boolean stop = true;
    private final int port;
    public List<ServerInThread> serverInThreads = new ArrayList<>();
    public List<Lobby> lobbies = new ArrayList<>();
    public List<ServerGame> serverGames = new ArrayList<>();
    public List<String> names = new ArrayList<>();
    public List<PrintWriter> printWriters = new ArrayList<>();

    /**
     * Constructor
     *
     * @param port Port-Number
     */
    public Server(int port) {
        this.port = port;
    }

    /**
     * start the game with this ID
     *
     * @param ID lobby id of the lobby to be started
     */
    public void startLobby(int ID) {
        if (ID > lobbies.size() - 1) {
            logger.debug("Lobby doesn't exist");
            return;
        }

        if (lobbies.get(ID).isInGame()) {
            logger.debug("Lobby " + ID + " is already InGame");
            return;
        }


        ServerInThread[] inThreads = new ServerInThread[5];

        //telling every ServerInThread that is in the lobby to start
        int count = 0;
        for (ServerInThread iT : serverInThreads) {
            if (iT.lobbyID == ID) {
                inThreads[count] = serverInThreads.get(count);
                count++;
            }
        }


        //String encoding for playerID and playerName
        StringBuilder sb = new StringBuilder();


        count = 0;
        for (ServerInThread iT : inThreads) {
            if (iT != null) {
                iT.playerID = count;
                sb.append(count).append(";").append(inThreads[count].name).append(";");
                count++;
            }
        }

        //tells Every serverInThread to start the game
        for (ServerInThread iT : inThreads) {
            if (iT != null) {
                iT.startGame(sb.toString());
            }

        }


        //Set lobby to "in game" in the list
        lobbies.get(ID).setInGame();

        logger.debug("Lobby " + ID + " has started a game");
        //start a new game thread
        ServerGame serverGame = new ServerGame(ID, inThreads, this);

        Thread g = new Thread(serverGame, "Game " + ID);
        g.setPriority(10);
        g.start();

        serverGames.add(serverGame);
        serverGames.sort(Comparator.comparing(ServerGame::getLobbyID));

        logger.debug("Lobby " + ID + " started.");

    }

    /**
     * chat to every client in the same lobby
     *
     * @param lobbyID lobby ID
     * @param line    Message to be send
     */
    public void lobbyChat(int lobbyID, String line) {
        for (ServerInThread iT : serverInThreads) {
            if (iT.lobbyID == lobbyID) {
                iT.getPrinter().println(Protocol.CHAT.name() + " " + line);
                iT.getPrinter().flush();
            }
        }
    }

    /**
     * run-method of Server
     * waits for connections and start ServerInThreads
     */
    public void run() {
        int cnt = 0;
        try {
            logger.debug(
                    "Waiting for connections to Port " + port + "...");
            ServerSocket echoed = new ServerSocket(this.port);
            while (stop) {
                Socket socket = echoed.accept();
                ServerInThread eC = new ServerInThread(cnt++, socket, this);
                Thread eCT = new Thread(eC, "ServerInThread " + (cnt - 1));
                eCT.start();
                printWriters.add(new PrintWriter(socket.getOutputStream()));
                serverInThreads.add(eC);
            }

        } catch (IOException e) {
            logger.error(e.toString());
            System.exit(1);
        }
    }

    /**
     * Exports the Array List of names as a String.
     *
     * @return returns the String with all the player names.
     */
    public String getList() {
        StringBuilder sb = new StringBuilder();
        for (ServerInThread serverInThread : serverInThreads) {
            sb.append("  ").append(serverInThread.getName()).append(";");
        }

        return sb.toString();
    }

    /**
     * Method to broadcast a message with its username attached to it
     *
     * @param name name
     * @param line message
     */
    public void broadcast(String name, String line) {
        logger.debug(name + ": " + line);
        for (ServerInThread serverInThread : serverInThreads) {
            serverInThread.getPrinter().println("CHAT " + "BROADCAST (" + name + "): " + line);
            serverInThread.getPrinter().flush();
        }
    }


    /**
     * close Server
     */
    public static void closeServer() {
        stop = false;
    }

}
