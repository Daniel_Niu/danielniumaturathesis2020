package ch.unibas.dmi.dbis.cs108.shaderutils;

import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.font.Letter;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

public class TextRenderer {

    private final Vector4f color;

    public TextRenderer(Vector4f color) {
        this.color = color;
    }

    public void render(String text, Shader shader, Font font, Vector2f position, Matrix4f projection) {
        float xTemp;
        char[] c;
        glBindTexture(GL_TEXTURE_2D, font.ID());
        Map<Character, Letter> chars = font.getCharacters();
        xTemp = position.x;
        c = text.toCharArray();
        shader.setUniform("projection", projection);
        shader.setUniform("matColor", color);
        shader.setUniform("sampler", 0);

        for (char value : c) {


            Letter r = chars.get(value);
            shader.setUniform("screenPos", new Vector2f(xTemp, position.y));
            shader.setUniform("offset", new Vector4f(r.x, r.y, r.w, r.h));
            shader.setUniform("pixelScale", new Vector2f(r.scaleX, r.scaleY));
            FontAssets.initAsset();
            FontAssets.getModel().render();
            xTemp += r.scaleX;
        }
    }
}
