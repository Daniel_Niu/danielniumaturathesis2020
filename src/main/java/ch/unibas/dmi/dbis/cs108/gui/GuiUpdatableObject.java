package ch.unibas.dmi.dbis.cs108.gui;

public interface GuiUpdatableObject {
    void update(double x, double y, boolean leftButton);
}
