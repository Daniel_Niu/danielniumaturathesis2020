package ch.unibas.dmi.dbis.cs108.utils;

import static java.lang.Math.*;

/**
 * A class representing a 4 by 4 matrix of the data type float.sa
 */
public class Matrix4f {


    /**
     * Due to java working with arrays, we need a "1 by 16 matrix" to represent a 4 by 4 matrix.
     */
    private final float[] matrix = new float[16];

    /**
     * Creates an identity matrix, which is a matrix that acts as a neutral operator.
     *
     * @return Returns the identity Matrix with the data type Matrix4f.
     */
    private static Matrix4f identity() {

        Matrix4f identity = new Matrix4f();

        for (int i = 0; i < 4 * 4; i++) {
            identity.matrix[i] = 0.0f;
        }
        for (int i = 0; i <= 15; i = i + 5) {
            identity.matrix[i] = 1.0f;
        }

        return identity;
    }

    /**
     * Multiplies two matrices with each other and returns the result.
     * @param matrix The matrix you want to multiply the first with.
     * @return Returns the result of the data type Matrix4f.
     */
    public Matrix4f multiply(Matrix4f matrix) {

        Matrix4f result = new Matrix4f();

        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                float sum = 0.0f;
                for (int i = 0; i < 4; i ++) {
                    sum += this.matrix[i + y * 4] * matrix.matrix[x + i * 4];
                }
                result.matrix[x + y * 4] = sum;
            }
        }
        return result;
    }

    public float[] getFloat() {
        return matrix;
    }

    /**
     * Inserts a vector into an identity matrix at indices 12 to 14 and returns the matrix.
     * @param vector The vector that is being put into the identity matrix.
     * @return Returns the resulting matrix.
     */
    public static Matrix4f translate (Vector3f vector) {

        Matrix4f result = identity();

        result.matrix [12] = vector.x;
        result.matrix [13] = vector.y;
        result.matrix [14] = vector.z;

        return result;

    }

    /**
     * Creates a matrix used to rotate objects in the game.
     * @param angle The angle of the rotation on the z axis.
     * @return The matrix that is to be used to rotate objects in the game.
     */
    public static Matrix4f rotate (float angle) {


        Matrix4f result = identity();
        float r = (float) toRadians(angle);
        float cos = (float) cos(r);
        float sin = (float) sin(r);

        result.matrix[0] = cos;
        result.matrix[1] = sin;
        result.matrix[4] = -sin;
        result.matrix[5] = cos;

        return result;

    }

    /**
     * Creates a projection matrix for rendering objects orthographically (2 dimensional).
     * @param left left clipping margin of the window
     * @param right right clipping margin of the window
     * @param bottom bottom clipping margin of the window
     * @param top top clipping margin of the window
     * @param near rendering distance
     * @param far rendering distance
     * @return Returns the resulting matrix.
     */
    public static Matrix4f orthographic (float left, float right, float bottom, float top, float near, float far) {

        Matrix4f result = identity();

        result.matrix[0] = 2.0f / (right - left);
        result.matrix[5] = 2.0f / (top - bottom);
        result.matrix[10] = 2.0f / (near - far);

        result.matrix[12] = (left + right) / (left - right);
        result.matrix[13] = (bottom + top) / (bottom - top);
        result.matrix[14] = (near + far) / (near - far);

        return result;

    }

    /**
     * matrix-Getter
     * @return our matrix
     */
    public float[] getMatrix() {
        return matrix;
    }
}
