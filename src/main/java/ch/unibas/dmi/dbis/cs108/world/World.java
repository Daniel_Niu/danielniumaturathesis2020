package ch.unibas.dmi.dbis.cs108.world;

import ch.unibas.dmi.dbis.cs108.entity.Hitbox;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Model;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine.ServerGame;
import org.joml.Vector2f;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Niu
 * World abstract class containing the platforms and walls of the world
 */
public abstract class World {

    public int width;
    public int height;
    List<Vector2f> spawnLocations = new ArrayList<>();
    List<Vector2f> itemSpawnLocations = new ArrayList<>();
    private Hitbox[] platforms;


    /**
     * Constructor
     * initiates map and platforms/walls via png files
     *
     * @param mapName String folder name of the world containing the platforms.png file and entities.png file
     */
    World(String mapName) {
        try {
            InputStream in = Model.class.getClassLoader().getResourceAsStream("textures/maps/" + mapName + "/platforms.png");
            InputStream in2 = Model.class.getClassLoader().getResourceAsStream("textures/maps/" + mapName + "/entities.png");


            assert in != null;
            BufferedImage platforms_png = ImageIO.read(in);
            assert in2 != null;
            BufferedImage entity_sheet = ImageIO.read(in2);

            width = platforms_png.getWidth();
            height = platforms_png.getHeight();


            int[] colorPlatformSheet = platforms_png.getRGB(0, 0, width, height, null, 0, width);
            int[] colorEntitySheet = entity_sheet.getRGB(0, 0, width, height, null, 0, width);


            platforms = new Hitbox[width * height + 4];

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int red = (colorPlatformSheet[x + y * width] >> 16) & 0xFF;
                    int entity_index_blue = colorEntitySheet[x + y * width] & 0xFF;
                    int entity_index_red = (colorEntitySheet[x + y * width] >> 16) & 0xFF;


                    if (red > 100) {
                        platforms[x + y * width] = new Hitbox(new Vector2f(x + 0.5f, -y - 0.5f), 1.0f, 1.0f);
                    }

                    if (entity_index_red > 100) {
                        spawnLocations.add(new Vector2f(x + 0.5f, -y));
                    }
                    if (entity_index_blue > 100) {
                        itemSpawnLocations.add(new Vector2f(x + 0.5f, -y - 0.5f));
                    }

                }

            }



        } catch (IOException e) {
            System.err.println("Could not load world");
            System.exit(-1);
        }
    }

    public abstract void update(Float delta, Input input, ClientGameMachine clientGameMachine);

    public abstract void update(Float delta, ServerGame serverGame);

    /*
    public Matrix4f getWorldMatrix(){
        return world;
    }
     */
    public abstract void render(Shader shader, ClientGameMachine clientGameMachine);

    public Hitbox getPlatformHitBox(int x, int y) {

        try {
            return platforms[x + y * width];
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

}
