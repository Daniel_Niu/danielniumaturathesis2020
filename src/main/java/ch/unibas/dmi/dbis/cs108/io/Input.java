package ch.unibas.dmi.dbis.cs108.io;

import org.lwjgl.glfw.*;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_LAST;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;

/**
 * Input Class: contains all Methods to use keyboard an mouse inside a display
 */
public class Input {
    private final long window;
    private final boolean[] keys;
    private final boolean[] buttons = new boolean[GLFW.GLFW_MOUSE_BUTTON_LAST];
    private double mouseX, mouseY;
    private double scrollX, scrollY;
    private final GLFWCursorPosCallback mouseMove;
    private final GLFWMouseButtonCallback mouseButtons;
    private final GLFWScrollCallback mouseScroll;
    private final GLFWKeyCallback keyboard;
    private final boolean[] keyboardArray = new boolean[GLFW.GLFW_KEY_LAST];
    private final GLFWKeyCallback releasedKeyboard;
    private final boolean[] releaseArray = new boolean[GLFW.GLFW_KEY_LAST];

    /**
     * Constructor of Input
     *
     * @param window a display
     */
    public Input(long window) {
        this.window = window;
        this.keys = new boolean[GLFW_KEY_LAST];

        //fill keyArray with false
        for (int i = 0; i < GLFW_KEY_LAST; i++) {
            keys[i] = false;
            }

        //creates a Callback from CursorPosition
        mouseMove = new GLFWCursorPosCallback() {
            public void invoke(long window, double xpos, double ypos) {
                mouseX = xpos;
                mouseY = ypos;
            }
        };

        //creates a Callback from MouseButton
        mouseButtons = new GLFWMouseButtonCallback() {
            public void invoke(long window, int button, int action, int mods) {
                buttons[button] = (action != GLFW.GLFW_RELEASE);
            }
        };

        //creates a Callback from MouseScroll
        mouseScroll = new GLFWScrollCallback() {
            public void invoke(long window, double xoffset, double yoffset) {
                scrollX = scrollX + xoffset;
                scrollY = scrollY + yoffset;
            }
        };

        //creates a Callback from Keyboard
        keyboard = new GLFWKeyCallback() {
            public void invoke(long window, int key, int scancode, int action, int mods) {
                keyboardArray[key] = (action != GLFW.GLFW_RELEASE);
            }
        };
        releasedKeyboard = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                releaseArray[key] = (action != GLFW_PRESS);
            }
        };
    }

    /**
     * checks if a Key is down
     * @param key specifiy keycode from Keyboard
     * @return true if our display is open and we click a key
     */
    public synchronized boolean isKeyDown(int key) {
        return keyboardArray[key];
    }

    /*
     * checks if a Key was shortly down
     * @param key specific keycode from Keyboard
     * @return true if key is down
     */
    /*public static boolean isKeyPressed(int key) {
        boolean x = false;

        while (Keyboard.next()) {
            if(Keyboard.getEventKeyState()) {
                String s = Keyboard.getKeyName(key);
                if (Keyboard.getEventKey() == Keyboard.getKeyIndex(s)) {
                    x = true;
                }
            }
        }
        return x;
    }*/

    /**
     * checks if a Key was already pressed
     * @param key specific keycode from Keyboard
     * @return true if key isnt down but was pressed (stored in keyArray)
     */
    public synchronized  boolean isKeyReleased(int key) {
        return releaseArray[key];
    }

    /**
     * checks if a MouseButton was shortly down
     * @param button specific keycode from Mousebuttons
     * @return true if buttion is down but nor stored in buttonArray yet
     */
    public synchronized boolean isButtonPressed(int button) {
        return (isButtonDown(button) && !buttons[button]);
    }

    /**
     * checks if a MouseButton was already pressed
     * @param button specific keycode from Mousebuttons
     * @return true if button isnt down but was pressed (stored in buttonArray)
     */
    public synchronized boolean isButtonReleased(int button) {
        return (!isButtonDown(button) && buttons[button]);
    }

    /**
     * checks if a MouseButton is Down
     * @param button specific keycode from Mousebuttons
     * @return boolean-Value from buttonArray (true if button is stored inside)
     */
    public synchronized boolean isButtonDown(int button) {
        return buttons[button];
    }

    /**
     * destroys our MouseCallbacks
     */
    public void destroy() {
        mouseMove.free();
        mouseButtons.free();
        mouseScroll.free();
    }
    /**
     * X-coordinate-Getter
     * @return current x-coordinate where the mouse is
     */
    public double getMouseX() {
        return mouseX;
    }

    /**
     * Y-coordinate-Getter
     * @return current y-coordinate where the mouse is
     */
    public double getMouseY() {
        return mouseY;
    }

    /**
     * X-Scroll-Getter
     * @return current x-coordinate where the mouse has scrolled
     */
    public double getScrollX() {
        return scrollX;
    }

    /**
     * Y-Scroll-Getter
     * @return current y-coordinate where the mouse has scrolled
     */
    public double getScrollY() {
        return scrollY;
    }
    /**
     * MouseMove-Getter
     * @return CursorPositionCallback where our mouse is at the moment
     */
    public GLFWCursorPosCallback getMouseMoveCallback() {
        return mouseMove;
    }

    /**
     * MouseButton-Getter
     * @return MouseButtonCallback from our mouseButtons
     */
    public GLFWMouseButtonCallback getMouseButtonsCallback() {
        return mouseButtons;
    }

    /**
     * MouseScroll-Getter
     * @return ScrollCallback where i Scrolled my Mouse
     */
    public GLFWScrollCallback getMouseScrollCallback() {
        return mouseScroll;
    }
    /**
     * Keyboard-Getter
     * @return KeyCallback from our keyboard
     */
    public GLFWKeyCallback getKeyboardCallback() {
        return keyboard;
    }

    /**
     * updates our keyArray
     * goes threw each key an stores it into our keyArray
     */
    public void update() {
        for (int i = 32; i < GLFW_KEY_LAST; i++)
            keys[i] = isKeyDown(i);
    }

}
