package ch.unibas.dmi.dbis.cs108.io;

import ch.unibas.dmi.dbis.cs108.utils.Vector3f;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

/**
 * class that contains all methods to setup,update and closing a display
 * including Fullscreen, FPS limiter, CallBackCreator
 */
public class Display {
    private final Logger logger = LogManager.getLogger(Display.class);
    private int width, height; //width & height from the display
    private final String title; //title of the display
    private long display; //our display
    private int frames; //our FPS at the moment
    private static long time; //time (speed of computer)
    private Input input; //Input Object
    private final Vector3f background = new Vector3f(1.0f, 1.0f, 1.0f); //background set to white
    private GLFWWindowSizeCallback sizeCallback; //to see if our window has the right size
    private boolean isResized; //true if the size is rightly changed
    private boolean isFullscreen; // true if display is Fullscreen
    private final int[] displayPosX = new int[1];
    private final int[] displayPosY = new int[1]; //position of our display

    /**
     * Display Constructor
     *
     * @param width  of our display
     * @param height of our display
     * @param title  of our display
     */
    public Display(int width, int height, String title) {
        this.width = width;
        this.height = height;
        this.title = title;
    }

    /**
     * creates the GLFW class
     */
    public void create() {

        //glfwInit() intitialize GLFW -->return boolean
        if (!GLFW.glfwInit()) {

            //error handling --> is GLFW initialized?
            logger.error("Error: no GLFW initialized");
            System.exit(-1);
        }

        //creates a Display (checks if Fullscreen or not)
        display = GLFW.glfwCreateWindow(width, height, title, isFullscreen ? GLFW.glfwGetPrimaryMonitor() : 0, 0);

        if (display == 0) {
            //error handling --> has display been created?
            logger.error("Error: no Display created");
            System.exit(-1);
            throw new UnsupportedOperationException();
        }

        //sets Position of the Display in the primary Monitor
        GLFWVidMode videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
        assert videoMode != null;

        //get StartPosition
        displayPosX[0] = (videoMode.width() - width) / 2;
        displayPosY[0] = (videoMode.height() - height) / 2;

        //set DisplayPosition with StartPosition
        GLFW.glfwSetWindowPos(display, displayPosX[0], displayPosY[0]);

        //makes our display visible if was hidden
        GLFW.glfwShowWindow(display);
        //the display which is going to be called by GLFW
        GLFW.glfwMakeContextCurrent(display);



        //openGL automatically stores z-values in depth buffer and discard fragments if failed depth test
        //GL11.glEnable(GL11.GL_DEPTH_TEST);

        //creates an input object
        input = new Input(display);
        createCallbacks();


        //limits frames per second (v sync)
        GLFW.glfwSwapInterval(1);

        //stores currentSystemTime into time
        time = System.currentTimeMillis();

        //make sure LWJGL use OpenGL by itself
        GL.createCapabilities();
    }
    public Input getInput(){
        return this.input;
    }

    /**
     * create Callbacks from input
     */
    private void createCallbacks() {

        //gets the size callback of specified display when display was resized
        sizeCallback = new GLFWWindowSizeCallback() {
            public void invoke(long window, int wi, int he) {
                width = wi;
                height = he;
                isResized = true;
            }
        };

        //set Callbacks for Keyboard and all MouseCallbacks
        GLFW.glfwSetKeyCallback(display, input.getKeyboardCallback());
        GLFW.glfwSetCursorPosCallback(display, input.getMouseMoveCallback());
        GLFW.glfwSetMouseButtonCallback(display, input.getMouseButtonsCallback());
        GLFW.glfwSetScrollCallback(display, input.getMouseScrollCallback());

        //sets the size callback for a resized display
        GLFW.glfwSetWindowSizeCallback(display, sizeCallback);
    }

    /**
     * updates our display
     */
    public void update() {

        //sets resized to false, if window is still original size
        if (isResized) {
            GL11.glViewport(0, 0, width, height);
            isResized = false;
        }

        //sets which RGB values should be cleared in Buffers
        //GL11.glClearColor(background.getX(), background.getY(), background.getZ(), 1.0f);

        //clears RGB values in Color or Depth Buffer
        //GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        //processes all events which are in queue
        GLFW.glfwPollEvents();
        frames++;

        //calculates our frames
        if (System.currentTimeMillis() > time + 1000) {

            //sets Window title including FPS counter
            GLFW.glfwSetWindowTitle(display, title + " FPS: [" + frames + "]");

            time = System.currentTimeMillis();
            frames = 0;
        }
    }

    /**
     * swaps our Buffers (the one is shown and the you draw on)
     */
    public void swapBuffers() {
        GLFW.glfwSwapBuffers(display);
    }

    /**
     *check if Display should close. if not checked, display doesnt know if should run or close after start
     * @return true if display should close
     */
    public boolean shouldClose() {
        return GLFW.glfwWindowShouldClose(display);
    }

    /**
     * destroys our display
     * closes our display
     */
    public void destroy() {
        input.destroy();
        sizeCallback.free();

        //check if display should close or not
        GLFW.glfwWindowShouldClose(display);

        //destroys our display
        GLFW.glfwDestroyWindow(display);

        //terminate program
        GLFW.glfwTerminate();
    }

    /**
     * with this method we can set our Background color
     * @param red float value of red
     * @param green float value of green
     * @param blue float value of blue
     */
    public void setBackgroundColor(float red, float green, float blue) {
        background.set(red, green, blue);
    }

    /**
     * check if Display isFullscreen or not
     * @return true if display is already Fullscreen
     */
    public boolean isFullscreen() {
        return isFullscreen;
    }

    /**
     * sets display to Fullscreen
     * or if Fullscreen sets Display to widowed mode
     * @param isFullscreen our boolean if fullscreen or not
     */
    public void setFullscreen(boolean isFullscreen) {
        this.isFullscreen = isFullscreen;
        isResized = true;

        //if Fullscreen change to widowed Mode
        if (isFullscreen) {
            GLFW.glfwGetWindowPos(display, displayPosX, displayPosY);
            GLFW.glfwSetWindowMonitor(display, GLFW.glfwGetPrimaryMonitor(), 0, 0, width, height, 0);
            GLFW.glfwSwapInterval(1);

        } else {
            //if not Fullscreen change to Fullscreen
            GLFW.glfwSetWindowMonitor(display, 0, displayPosX[0], displayPosY[0], width, height, 0);
            GLFW.glfwSwapInterval(1);
        }
    }

    /**
     * Width-Getter
     * @return our width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Height-Getter
     * @return our height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Title-Getter
     * @return our title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Display-Getter
     * @return our Display
     */
    public long getDisplay() {
        return display;
    }
}
