package ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine;

import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import ch.unibas.dmi.dbis.cs108.utils.ArraysUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Daniel Niu
 * GameState for an on going game on the server
 */
public class ServerGameRun implements GameState {
    private final ServerGame serverGame;
    private final Logger logger = LogManager.getLogger(ServerGameRun.class);

    public ServerGameRun(ServerGame serverGame) {
        this.serverGame = serverGame;
    }

    /**
     * Gameloop containing the tick limiter
     * game logic is handled via the ServerWorld class
     */
    @Override
    public void run() {



        for (int i = 0; i < serverGame.rounds; i++) {
            long lastTime = System.nanoTime();
            final double amountOfTicks = 60D;
            double ns = 1000000000 / amountOfTicks;
            float delta = 0;

            while (serverGame.running) {
                long now = System.nanoTime();
                delta += (now - lastTime) / ns;
                lastTime = now;
                if (delta >= 1) {
                    if (serverGame.shouldClose()) {
                        return;
                    }
                    //update
                    serverGame.serverWorld.update((float) (1 / amountOfTicks), serverGame);

                    delta--;
                    if (serverGame.time > serverGame.roundDuration * 60) {
                        serverGame.time = 0;
                        logger.debug("Game reached game duration");
                        break;
                    }
                    serverGame.time++;
                }
            }

            serverGame.setPoints();
            serverGame.serverWorld.respawnAll();
            serverGame.resetRoundScore();
            logger.debug("Round " + (i + 1) + "has ended");
            try {
                synchronized (this) {
                    wait(5000);
                }
            } catch (InterruptedException e) {
                logger.error("wait() error");
            }
            serverGame.nextRound(i + 2);

            logger.debug("Round " + (i + 2) + " will start");
        }

        serverGame.setGameState(serverGame.getGameEnd());
        serverGame.nextState();
    }

    /**
     * method used to prepare the game
     * a random player is marked at the start
     */
    @Override
    public void start() {
        if (serverGame.shouldClose()) {
            return;
        }
        int randomNum = ThreadLocalRandom.current().nextInt(0, ArraysUtil.getLength(serverGame.serverInThreadPlayers));
        //serverGame.players.get(randomNum).mark();
        int temp = 0;
        for (ServerInThreadPlayer serverInThreadPlayer : serverGame.serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                if (temp == randomNum) {
                    serverInThreadPlayer.serverPlayer.mark();
                    temp = serverInThreadPlayer.serverPlayer.getPlayerID();
                    break;
                }
                temp++;
            }
        }
        //serverGame.serverPlayers[randomNum].mark();

        serverGame.markedID = temp;

        serverGame.printAll(Protocol.MARK.name(), Integer.toString(randomNum));

        for (int i = 0; i < serverGame.serverInThreadPlayers.length; i++) {
            if (serverGame.serverInThreadPlayers[i] != null) {
                serverGame.serverInThreadPlayers[i].serverInThread.getPrinter().println(Protocol.CHAT + " You are Player " + serverGame.serverInThreadPlayers[i].getPlayerID());
                serverGame.serverInThreadPlayers[i].serverInThread.getPrinter().flush();
                serverGame.serverInThreadPlayers[i].serverInThread.getPrinter().println(Protocol.PLAYERID.name() + " " + serverGame.serverInThreadPlayers[i].getPlayerID());
                serverGame.serverInThreadPlayers[i].serverInThread.getPrinter().flush();
            }
        }

        run();

    }

    /*
    private void update(Float delta) {
        for(ServerPlayer p: serverGameMachine.players) {

            p.update(delta);

            for(MapObject mapObject: serverGameMachine.mapObjects){
                p.collideWithPlatform(mapObject);
            }

            if(p.getPlayerID() != serverGameMachine.markedID){
                checkMark(p);
            }

        }

        changeScore();
        printUpdate();

    }

    private void checkMark(ServerPlayer p) {
        if (serverGameMachine.players.get(serverGameMachine.markedID).isCatching(p) && p.getMarkCD() == 0) {
            logger.debug(p.getPlayerID()+" was marked by " + serverGameMachine.markedID);
            serverGameMachine.players.get(serverGameMachine.markedID).unmark();
            p.mark();
            for(PrintWriter prin: serverGameMachine.printers){
                prin.println(Protocol.MARK + " " + p.getPlayerID());
                prin.println(Protocol.UNMARK + " " + serverGameMachine.markedID);
                prin.flush();
            }
            serverGameMachine.markedID = p.getPlayerID();
        }
    }
*/


    /*
    private void changeScore() {

        //add score
        serverGame.players.get(serverGame.markedID).addRoundScore();

        //telling every client the current score
        serverGame.printAll(Protocol.ROUNDSCORE.name(), this.serverGame.markedID + ";" + serverGame.players.get(serverGame.markedID).getRoundScore());

    }

     */
/*
    private void printUpdate(){
        StringBuilder sbPos = new StringBuilder();
        StringBuilder sbVel = new StringBuilder();

        sbVel.append(System.currentTimeMillis());
        sbPos.append(System.currentTimeMillis());

        for(ServerPlayer p: serverGameMachine.players){
            sbPos.append(";");
            sbVel.append(";");
            sbPos.append(p.printPos());
            sbVel.append(p.printVel());

        }

        serverGameMachine.printAll(Protocol.POS.name(), sbPos.toString());

        serverGameMachine.printAll(Protocol.VEL.name(), sbVel.toString());


    }
*/
}
