package ch.unibas.dmi.dbis.cs108;

import java.io.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Daniel Niu
 * Class used to store Highscore in a Highscore.csv file
 */
public class Highscore {

    private static final String filename = "Highscore.csv";
    private static Map<String, Integer> scoreList = new HashMap<>();


    /**
     * load current Highscores
     * @throws IOException BufferedReader exception
     */
    public static void load() throws IOException {
        String line;
        int wins;
        String name;
        InputStream in = new FileInputStream(new File(filename));
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        scoreList.clear();

        while (br.ready()) {
            line = br.readLine();

            int index = line.indexOf(";");

            name = line.substring(0, index);
            wins = Integer.parseInt(line.substring(index + 1));

            scoreList.put(name, wins);
        }
        sortByValue();



    }


    /**
     * save changes to the file
     * @throws IOException BufferedWriter exception
     */
    public static void save() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

        for (String name : scoreList.keySet()) {
            String wins = scoreList.get(name).toString();
            writer.write(name + ";" + wins + System.getProperty("line.separator"));
        }
        writer.flush();
        writer.close();

    }

    /**
     * turn the current Highscores to a String
     * @return Highscore String
     */
    public static String printHighscore(){
        StringBuilder sb = new StringBuilder();
        for (String name: scoreList.keySet()){
            String value = scoreList.get(name).toString();
            sb.append(name).append(": ").append(value).append(";");
        }
        return sb.toString();

    }

    /**
     * taken from https://howtodoinjava.com/sort/java-sort-map-by-values/
     * sorts a map by Value
     */
    private static void sortByValue() {


        LinkedHashMap<String, Integer> reverseSortedMap = new LinkedHashMap<>();
        scoreList.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> reverseSortedMap.put(x.getKey(), x.getValue()));
        scoreList = reverseSortedMap;

    }

    /**
     * Add new win for the player with this name
     * @param name Winner's name
     */
    public static void addWin(String name){

        if (scoreList.containsKey(name)) {
            int score = scoreList.get(name);
            score++;
            scoreList.replace(name, score);
        } else {
            scoreList.put(name, 1);
        }

        try{
            save();
        } catch(IOException e) {
            //TODO
        }

    }


}
