package ch.unibas.dmi.dbis.cs108.utils;

/**
 * Vector2f class
 * used for textureCoordinates
 */
public class Vector2f {
    private float x, y;

    /**
     * Constructor, sets inserted Coordinates to our float values
     * @param x Coordinate from our Texture
     * @param y Coordinate from our Texture
     */
    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * x-Getter
     * @return x-coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * x-Setter
     * @param x our coordinate we want to set
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * y-Getter
     * @return y-coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * y-Setter
     * @param y our coordinate we want to set
     */
    public void setY(float y) {
        this.y = y;
    }
}
