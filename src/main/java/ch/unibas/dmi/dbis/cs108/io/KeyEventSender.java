package ch.unibas.dmi.dbis.cs108.io;

import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * The Class which sends all Key inputs of a Input class to the Server
 */
public class KeyEventSender {
    Logger logger = LogManager.getLogger(KeyEventSender.class);
    private final PrintWriter printer;
    private final String ip;
    public boolean a2 = true;
    public boolean d2 = true;
    private final int port;
    private final boolean running;
    private Input input;
    private boolean a = true;
    private boolean space = true;
    private boolean d = true;
    private final InputStream in;
    private final Socket sock;
    private ClientGameMachine clientGameMachine;

    /**
     * Constructor
     *
     * @param ip   IP-Address
     * @param port Port-Number (8090)
     * @throws IOException Socket.class
     */
    private KeyEventSender(String ip, int port) throws IOException {
        this.ip = ip;
        this.port = port;
        this.running = true;
        this.sock = new Socket(ip, port);
        this.in = sock.getInputStream();
        this.printer = new PrintWriter(sock.getOutputStream());
    }

    public KeyEventSender(ClientGameMachine gm) {
        this.clientGameMachine = gm;
        this.ip = clientGameMachine.ip;
        this.port = clientGameMachine.port;
        this.running = true;
        this.sock = clientGameMachine.sock;
        this.in = clientGameMachine.in;
        this.printer = clientGameMachine.printer;
        this.input = clientGameMachine.input;
    }

    private KeyEventSender(String ip, int port, String name) throws IOException {
        this.ip = ip;
        this.port = port;
        this.running = true;
        this.sock = new Socket(ip, port);
        this.in = sock.getInputStream();
        this.printer = new PrintWriter(sock.getOutputStream());
        this.printer.println(Protocol.CHANGENAME + " " + name);
        this.printer.flush();
    }

    /**
     * Send current Keys to the server
     *
     * @param input GLFW keycallback indicating which keys are currently pressed
     */
    public void sendInputs(Input input){

            // reading input stream
            if (input.isKeyDown(GLFW.GLFW_KEY_SPACE) && space) {
                this.printer.println(Protocol.SPACE.name() + " " + "true");
                this.printer.flush();
                space = false;
            }
            if ((!input.isKeyDown(GLFW.GLFW_KEY_SPACE)) && (!space)) {
                this.printer.println(Protocol.SPACE.name() + " " + "false");
                this.printer.flush();
                space = true;
            }

            if (input.isKeyDown(GLFW.GLFW_KEY_A) && a) {
                if (input.isKeyDown(GLFW.GLFW_KEY_D)) {
                    this.printer.println(Protocol.D.name() + " " + "false");
                    this.printer.flush();
                }
                this.printer.println(Protocol.A.name() + " " + "true");
                this.printer.flush();
                a = false;
            }
            if ((!input.isKeyDown(GLFW.GLFW_KEY_A)) && (!a)) {
                if (input.isKeyDown(GLFW.GLFW_KEY_D)) {
                    this.printer.println(Protocol.D.name() + " " + "true");
                    this.printer.flush();
                }
                this.printer.println(Protocol.A.name() + " " + "false");
                this.printer.flush();
                a = true;
            }
            if (input.isKeyDown(GLFW.GLFW_KEY_D) && d) {
                if (input.isKeyDown(GLFW.GLFW_KEY_A)) {
                    this.printer.println(Protocol.D.name() + " " + "false");
                    this.printer.flush();
                }

                this.printer.println(Protocol.D.name() + " " + "true");
                this.printer.flush();
                d = false;


            }
            if ((!input.isKeyDown(GLFW.GLFW_KEY_D)) && (!d)) {
                if (input.isKeyDown(GLFW.GLFW_KEY_A)) {
                    this.printer.println(Protocol.D.name() + " " + "true");
                    this.printer.flush();
                }
                this.printer.println(Protocol.D.name() + " " + "false");
                this.printer.flush();
                d = true;
            }


    }

}