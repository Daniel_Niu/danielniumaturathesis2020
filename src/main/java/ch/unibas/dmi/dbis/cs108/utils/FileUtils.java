package ch.unibas.dmi.dbis.cs108.utils;

import ch.unibas.dmi.dbis.cs108.shaderutils.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * used to load our Vertex-and Fragment- Shaders as String
 */
public class FileUtils {
    private static final Logger logger = LogManager.getLogger(FileUtils.class);
    /**
     * loads text from a file at path
     * @param path location from our shaders
     * @return String contains text from shaders
     */
    public static String loadAsString(String path) {
        StringBuilder strB = new StringBuilder();
        InputStream in = Model.class.getClassLoader().getResourceAsStream(path);
        assert in != null;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String s;
            while ((s = reader.readLine()) != null) {
                strB.append(s).append("\n");
            }
        } catch (IOException e) {
            logger.error("no file at" + path);
        }
        return strB.toString();
    }
}
