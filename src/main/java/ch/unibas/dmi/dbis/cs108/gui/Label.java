package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

import java.awt.*;

/**
 * a simple text label
 */
public class Label extends GuiObject {

    public String text;
    public Color color;

    /**
     * Constructor
     *
     * @param id       the ID of the label
     * @param text     the text shown on the label
     * @param color    the color of the text
     * @param position the position on the screen
     */
    Label(int id, String text, Color color, Vector2f position) {
        super(id, position, 0, 0);
        this.text = text;
        this.color = color;
    }

    /**
     * changes the text of the label
     *
     * @param s the new text
     */
    void changeText(String s) {
        text = s;
    }

    @Override
    public void render(Shader shader, ClientGameMachine clientGameMachine, Font font) {

        Matrix4f projection = new Matrix4f().setOrtho2D(0, clientGameMachine.displayWidth, clientGameMachine.displayHeight, 0);
        label(text, clientGameMachine.bigFont, shader, hitbox.getCenter().x, hitbox.getCenter().y, new Vector4f(255, 255, 255, 100), projection, -1);


    }
}


