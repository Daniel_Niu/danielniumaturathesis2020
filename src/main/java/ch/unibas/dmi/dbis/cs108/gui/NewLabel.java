package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.font.Letter;
import ch.unibas.dmi.dbis.cs108.shaderutils.FontAssets;
import ch.unibas.dmi.dbis.cs108.shaderutils.Positioning;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

public class NewLabel implements GuiRenderObject {
    private final Positioning positioning;
    private final Vector4f color;
    private final Font font;
    private String text;

    /**
     * Constructor
     *
     * @param text      the text shown on the label
     * @param color     the color of the text
     * @param screenPos the position on the screen
     */
    NewLabel(String text, Vector4f color, Vector2f screenPos, Matrix4f projection, ch.unibas.dmi.dbis.cs108.font.Font font) {

        this.positioning = new Positioning(0, 0, screenPos, projection, 0, 0);
        this.font = font;
        this.text = text;
        this.color = color;

    }

    /**
     * changes the text of the label
     *
     * @param s the new text
     */
    public void changeText(String s) {
        text = s;
    }

    @Override
    public void render(Shader shader) {

        float xTemp;
        char[] c;
        glBindTexture(GL_TEXTURE_2D, font.ID());
        Map<Character, Letter> chars = font.getCharacters();
        xTemp = positioning.screenPos.x;
        c = text.toCharArray();
        shader.setUniform("projection", positioning.projection);
        shader.setUniform("matColor", color);
        shader.setUniform("sampler", 0);
        for (char value : c) {
            Letter r = chars.get(value);
            shader.setUniform("screenPos", new Vector2f(xTemp, positioning.screenPos.y));
            shader.setUniform("offset", new Vector4f(r.x, r.y, r.w, r.h));
            shader.setUniform("pixelScale", new Vector2f(r.scaleX, -r.scaleY));
            FontAssets.initAsset();
            FontAssets.getModel().render();
            xTemp += r.scaleX;
        }
    }


}
