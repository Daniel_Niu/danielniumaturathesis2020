package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Niu
 * Class used to contain all interactive GUI elements of one display
 */
public class WorldGui {
    private final List<GuiObject> objects = new ArrayList<>();


    public WorldGui() {
        objects.add(new Scorebars());
    }

    /**
     * updates all objects on the screen
     * @param input the mouse and keyboard input
     */
    public void update(Input input) {
        for (GuiObject guiObject : objects) {
            guiObject.update(input);
        }
    }

    /**
     * renders all objects on the screen
     * @param shader the shader used
     * @param clientGameMachine the client for which this screen is shown
     * @param font the font used
     */
    public void render(Shader shader, ClientGameMachine clientGameMachine, Font font) {
        for (GuiObject guiObject : objects) {
            guiObject.render(shader, clientGameMachine, font);
        }
    }

}