package ch.unibas.dmi.dbis.cs108.io;

import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.PrintWriter;

/**
 * Class for the Chat GUI using JFrame.
 */
public class ChatWindow extends JFrame {

    private final Logger logger = LogManager.getLogger(ChatWindow.class);

    private final JTextArea chatLog; // where the chat history will be visible
    private final ClientGameMachine clientGameMachine;
    private JTextField input; // where you will type your message in

    /**
     * Constructor for the Chat Window including Buttons, Text Areas and a Text Field for input.
     * Has Action Listeners for the Buttons and pressing Enter in the Text Field.
     *
     * @param gm GameMachine containing all the necessary data
     */
    public ChatWindow(ClientGameMachine gm) {
        this.clientGameMachine = gm;
        PrintWriter printer = clientGameMachine.printer;
        setSize(600, 300); // sets the size of the window

        //join lobby button
        JButton joinLobby = new JButton("Join Lobby");
        joinLobby.addActionListener(actionEvent -> {
            String s = JOptionPane.showInputDialog("Lobby ID?");
            if (s != null) {
                printer.println(Protocol.JOIN.name() + " " + s);
                printer.flush();
            }
        });

        //create lobby button
        JButton createLobby = new JButton("Create Lobby");
        createLobby.addActionListener(actionEvent -> {
            logger.debug("created Lobby.");
            printer.println(Protocol.LOBBYCREATE.name() + " filler");
            printer.flush();

        });

        //change name button
        JButton changeName = new JButton("Change Name");
        changeName.addActionListener(actionEvent -> {
            String s = JOptionPane.showInputDialog("Choose Username");
            printer.println(Protocol.CHANGENAME.name() + " " + s);
            printer.flush();
        });

        //whisper button
        JButton whisper = new JButton("Whisper");
        whisper.addActionListener(actionEvent -> {
            String s1 = JOptionPane.showInputDialog("Who do you want to whisper to?");
            String s3 = JOptionPane.showInputDialog("Message?");
            printer.println(Protocol.WISP.name() + " " + s1 + ";" + s3);
            printer.flush();
        });

        //broadcast button
        JButton broadcast = new JButton("Send Broadcast");
        broadcast.addActionListener(actionEvent -> {
            if (input.getText().length() != 0) {
                printer.println(Protocol.BROADCAST.name() + " " + input.getText());
                printer.flush();
                input.setText("");
            }
        });

        //leave button
        JButton leave = new JButton("Leave");
        leave.addActionListener(actionEvent -> System.exit(0));

        //player list button
        JButton list = new JButton("Show Player List");
        list.addActionListener(actionEvent -> {
            printer.println(Protocol.LIST.name() + " i" );
            printer.flush();
        });

        //player list button
        JButton highscore = new JButton("Highscore");
        highscore.addActionListener(actionEvent -> {
            printer.println(Protocol.REQHS.name()+" filler");
            printer.flush();
        });

        //start button
        JButton start = new JButton("Start Game");
        start.addActionListener(actionEvent -> {
            printer.println(Protocol.STARTGAME + " " + clientGameMachine.lobbyID);
            printer.flush();
        });

        this.chatLog = new JTextArea(10,52); // set size of the chat history area
        this.input = new JTextField(52); // set the width of the message input field

        // detects when user presses enter and sends the message
        input.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (input.getText().length() != 0) {
                        printer.println(Protocol.LOBBYCHAT.name() + " " + input.getText());
                        printer.flush();
                        input.setText("");
                    }
                }
            }
        });

        //make chat history are scrollable
        JScrollPane sp = new JScrollPane(chatLog, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        setResizable(false);
        setLayout(new FlowLayout(FlowLayout.LEFT));

        //using the imported SmartScroller.java class
        new SmartScroller(sp);

        chatLog.setLineWrap(true);
        chatLog.setWrapStyleWord(true);

        //add various elements to the frame
        add(createLobby);
        add(joinLobby);
        add(start);
        add(leave);
        add(sp);
        add(input);
        add(changeName);
        add(list);
        add(highscore);
        add(whisper);
        add(broadcast);
    }

    /**
     * Method to show and extract the player list in a message dialog.
     *
     * @param s the player names condensed in one single string
     */
    void showList(String s) {
        JOptionPane.showMessageDialog(null, formatProtocol(s));
    }

    /**
     * Method to show the highscore list
     *
     * @param s the player names and their scores in a single string encoded according to HIGHSCORE in the protocol
     */
    void showHighscore(String s) {

        JOptionPane.showMessageDialog(null, formatProtocol(s));
    }

    /**
     * Prints the text onto the Text Area used to display the chat log.
     *
     * @param s incoming String
     */
    void print(String s) {
        chatLog.append(s + "\n");
    }

    private String formatProtocol(String s) {
        int semicolon;
        StringBuilder sb = new StringBuilder();
        String firstSubstring;
        String n;
        while (s.contains((";"))) {
            semicolon = s.indexOf(";");
            firstSubstring = s.substring(semicolon + 1);
            n = s.substring(0, semicolon);

            sb.append(n).append("\n");
            s = firstSubstring;
        }
        return sb.toString();
    }

}
