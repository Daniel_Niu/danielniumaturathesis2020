package ch.unibas.dmi.dbis.cs108.shaderutils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;


/**
 * @author Daniel
 * Texture class for opengl textures
 */
public class Texture {
    private final Logger logger = LogManager.getLogger(Texture.class);

    private final String path;
    private int textureID;
    private int width;
    private int height;

    private static final List<Texture> textures = new ArrayList<>();

    /**
     * @param filename filepath of the png file.
     *                 Texture automatically takes from resources/textures/
     *                 example: resources/textures/player/bob.png
     *                 should be: "player/bob.png"
     */
    public Texture(String filename) {
        path = "textures/" + filename;
        BufferedImage bufferedImage;
        try {
            //read file

            InputStream in = Model.class.getClassLoader().getResourceAsStream(path);
            assert in != null;
            BufferedInputStream bufferedInputStream = new BufferedInputStream(in);
            bufferedImage = ImageIO.read(bufferedInputStream);
            logger.debug("loaded " + path);
            width = bufferedImage.getWidth();
            height = bufferedImage.getHeight();

            int[] pixels;
            int[] data = new int[(width * height)];
            pixels = bufferedImage.getRGB(0, 0, width, height, null, 0, width);

            IntBuffer buffer = ByteBuffer.allocateDirect(data.length << 2).order(ByteOrder.nativeOrder()).asIntBuffer();


            //for-loop store rgb values into data
            for (int i = 0; i < width * height; i++) {
                int a = (pixels[i] & 0xff000000) >> 24;
                int r = (pixels[i] & 0xff0000) >> 16;
                int g = (pixels[i] & 0xff00) >> 8;
                int b = (pixels[i] & 0xff);

                data[i] = a << 24 | b << 16 | g << 8 | r;
            }
            buffer.put(data).flip();

            //OpenGL texture initiation
            textureID = glGenTextures();

            glBindTexture(GL_TEXTURE_2D, textureID);

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
            textures.add(this);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void destroyAllTextures() {
        for (Texture tex : textures) {
            tex.destroy();
        }
    }

    /**
     * tells OpenGL to use this texture in the context
     *
     * @param sampler Shader Attribute "sampler" for texture sampling
     */
    public void bind(int sampler) {
        if (sampler >= 0 && sampler <= 31) {
            glActiveTexture(GL_TEXTURE0 + sampler);
            glBindTexture(GL_TEXTURE_2D, textureID);
        }
    }

    /**
     * command this texture from OpenGL
     */
    private void destroy() {
        glDeleteTextures(textureID);
    }
}
