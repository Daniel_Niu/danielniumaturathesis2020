package ch.unibas.dmi.dbis.cs108.utils;

public class ProtocolUtils {

    public static int[] format2i(String s, String separator) {
        int[] arr = new int[2];


        arr[0] = Integer.parseInt(s.split(separator, 2)[0]);
        arr[1] = Integer.parseInt(s.split(separator, 2)[1].split(separator, 2)[0]);

        return arr;
    }

    public static String[] format2s(String s, String separator) {
        String[] arr = new String[2];

        arr[0] = s.split(separator, 2)[0];
        arr[1] = s.split(separator, 2)[1].split(separator, 2)[0];

        return arr;
    }

    public static String[] format3s(String s, String separator) {
        String[] arr = new String[3];
        arr[0] = s.split(separator, 2)[0];
        arr[1] = s.split(separator, 2)[1].split(separator, 2)[0];
        arr[2] = s.split(separator, 3)[2].split(separator, 2)[0];
        return arr;
    }

}
