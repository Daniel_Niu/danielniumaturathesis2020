package ch.unibas.dmi.dbis.cs108.world;

import ch.unibas.dmi.dbis.cs108.config.DeadlineEscapeConf;
import ch.unibas.dmi.dbis.cs108.entity.Item;
import ch.unibas.dmi.dbis.cs108.entity.Portal;
import ch.unibas.dmi.dbis.cs108.entity.ServerPlayer;
import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine.ServerGame;
import ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine.ServerInThreadPlayer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Daniel Niu
 * World class on the server side containing all the logic of the game
 */
public class ServerWorld extends World {
    private final Logger logger = LogManager.getLogger(ServerWorld.class);
    private final ServerGame serverGame;
    //Spawn interval of items
    private int itemCD = 60 * 10;


    /**
     * Constructor
     *
     * @param mapName    String folder name of the world containing the platforms.png file and entities.png file
     * @param serverGame Game thread to which the world is connected to.
     *                   Game thread contains all player variables.
     */
    public ServerWorld(String mapName, ServerGame serverGame) {
        super(mapName);
        this.serverGame = serverGame;
        this.serverGame.worldWidth = this.width;
        this.serverGame.worldHeight = this.height;

        for (int i = 0; i < serverGame.serverInThreadPlayers.length; i++) {

            //adds new player spawning on the spawn location
            if (serverGame.serverInThreadPlayers[i] != null) {
                serverGame.serverInThreadPlayers[i].serverPlayer = new ServerPlayer(serverGame.serverInThreadPlayers[i].serverInThread, new Vector2f(spawnLocations.get(i)), DeadlineEscapeConf.PLAYER_WIDTH, DeadlineEscapeConf.PLAYER_HEIGHT);
            }

        }

        serverGame.portals.add(new Portal(DeadlineEscapeConf.BLUE_PORTAL, new Vector2f(2, -4), new Vector2f(width - 9, -height + 11), 7, 7, false, -1));
        serverGame.portals.add(new Portal(DeadlineEscapeConf.YELLOW_PORTAL, new Vector2f(width - 3, -height + 11), new Vector2f(7, -4), 7, 7, false, 1));

        //tells the server game thread where Items can be spawned, known from the super class
        for (int i = 0; i < super.itemSpawnLocations.size(); i++) {
            serverGame.itemPlaces.add(true);
        }


        //TODO add more items


        serverGame.items[0] = new Item(0, 1, new Vector2f(0, 0), width / 30f, height / 15f);
        serverGame.items[1] = new Item(1, 1, new Vector2f(0, 0), width / 30f, height / 15f);
    }


    /**
     * update method for each game tick
     *
     * @param delta      float value with the current tick duration in seconds
     * @param serverGame game thread containing all necessary player data
     */
    @Override
    public void update(Float delta, ServerGame serverGame) {


        for (ServerInThreadPlayer serverInThreadPlayer : serverGame.serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                ServerPlayer playerObject = serverInThreadPlayer.serverPlayer;
                //update player
                playerObject.update(delta);
                //collide with world Platforms
                playerObject.collideWithWorld(this);
                //checks if the marked player has caught someone
                if (playerObject.getPlayerID() != serverGame.markedID) {
                    checkMark(playerObject);
                }
                for (Portal portal : serverGame.portals) {
                    portal.checkPortal(serverInThreadPlayer.serverPlayer);
                }
                //checks every active item with the current playerObject in the for loop
                if (!playerObject.isMarked()) {
                    for (Item item : serverGame.items) {
                        if (item.active) {
                            if (item.itemCollision(playerObject)) {
                                serverGame.printAll(Protocol.ITEMBUFF.name(), playerObject.getPlayerID() + ";" + item.objectID);
                                serverGame.itemPlaces.set(item.posID, true);
                            }
                        }

                    }
                }

                //if the player has an effect, then it will lower its duration
                playerObject.effectUpdate();
            }

        }
        //adds score for the marked player
        addScore();

        //print all movement updates
        printMovementUpdates();

        //spawns random item on random location, if coolDown is over, other otherwise it will only lower the coolDown
        spawnItem();


    }

    @Override
    public void render(Shader shader, ClientGameMachine clientGameMachine) {

    }

    public void respawnAll() {
        int tempInt = 0;
        for (ServerInThreadPlayer serverInThreadPlayer : serverGame.serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {
                serverInThreadPlayer.serverPlayer.moveTo(new Vector2f(spawnLocations.get(tempInt).x, spawnLocations.get(tempInt).y));
                tempInt++;
            }

        }
    }

    public void addPlayer(int id) {
        //TODO add player method
        //adds new player spawning on the spawn location
        if (serverGame.serverInThreadPlayers[id] != null) {
            serverGame.serverInThreadPlayers[id].serverPlayer = new ServerPlayer(serverGame.serverInThreadPlayers[id].serverInThread, new Vector2f(spawnLocations.get(id)), width / 20f, width / 20f);
        }
    }

    /**
     * the marked player receives scores over timer
     */
    private void addScore() {

        //add score
        if (serverGame.serverInThreadPlayers[serverGame.markedID] != null) {
            serverGame.serverInThreadPlayers[serverGame.markedID].serverPlayer.addRoundScore();

            //telling every client the current score
            serverGame.printAll(Protocol.ROUNDSCORE.name(), this.serverGame.markedID + ";"
                    + serverGame.serverInThreadPlayers[serverGame.markedID].serverPlayer.getRoundScore());
        } else {
            logger.error("marked player was not found");
        }

    }

    /**
     * Print all positions and velocities of all players to all clients
     */
    private void printMovementUpdates() {
        StringBuilder sbPos = new StringBuilder();


        for (ServerInThreadPlayer serverInThreadPlayer : serverGame.serverInThreadPlayers) {
            if (serverInThreadPlayer != null) {

                sbPos.append(serverInThreadPlayer.serverPlayer.printPos());
                sbPos.append(";");
            }
        }


        serverGame.printAll(Protocol.POS.name(), sbPos.toString());
        //serverGameMachine.printAll(Protocol.VEL.name(), sbVel.toString());
    }

    /**
     * spawns an item if the item spawn coolDown has reached 0 and resets it.
     */
    private void spawnItem() {
        if (itemCD == 0) {
            int randomNum1 = ThreadLocalRandom.current().nextInt(0, itemSpawnLocations.size());
            int randomNum2 = ThreadLocalRandom.current().nextInt(0, serverGame.items.length);

            if (serverGame.itemPlaces.get(randomNum1)) {
                Vector2f itemPos = new Vector2f(itemSpawnLocations.get(randomNum1).x, itemSpawnLocations.get(randomNum1).y);

                serverGame.items[randomNum2].activate(itemPos, randomNum1);
                itemCD = 60 * 10;
                logger.debug("Lobby " + serverGame.lobbyID + " spawned an Item " + randomNum2 + " " + itemPos);
                serverGame.printAll(Protocol.ITEMSPAWN.name(), randomNum2 + ";" + itemPos.x + ";" + itemPos.y);
                serverGame.itemPlaces.set(randomNum1, false);
            }

        } else {
            itemCD--;
        }
    }


    private void checkMark(ServerPlayer playerObject) {

        if (serverGame.serverInThreadPlayers[serverGame.markedID].serverPlayer.hitbox.intersects(playerObject.hitbox) && playerObject.getMarkCD() == 0) {


            logger.debug(playerObject.getName() + " was marked by " + serverGame.serverInThreadPlayers[serverGame.markedID].getName());

            //unmark the marked player
            serverGame.serverInThreadPlayers[serverGame.markedID].serverPlayer.unmark();
            playerObject.mark();

            //print new marks
            serverGame.printAll(Protocol.MARK.name(), Integer.toString(playerObject.getPlayerID()));
            serverGame.printAll(Protocol.UNMARK.name(), Integer.toString(serverGame.markedID));
            //mark new player
            serverGame.markedID = playerObject.getPlayerID();
        }
    }


    @Override
    public void update(Float delta, Input input, ClientGameMachine clientGameMachine) {

    }

}
