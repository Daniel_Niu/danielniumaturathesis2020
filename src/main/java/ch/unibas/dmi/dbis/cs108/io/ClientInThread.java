package ch.unibas.dmi.dbis.cs108.io;

import ch.unibas.dmi.dbis.cs108.config.DeadlineEscapeConf;
import ch.unibas.dmi.dbis.cs108.entity.ClientPlayer;
import ch.unibas.dmi.dbis.cs108.entity.Enemy;
import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.utils.ProtocolUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;

import java.io.*;

/**
 * the input Thread for all server transmissions
 */
public class ClientInThread implements Runnable {
    private final Logger logger = LogManager.getLogger(ClientInThread.class);
    private final PrintWriter printer;
    public int playerID;
    private final InputStream in;
    private final ChatWindow chatWindow;
    private final ClientGameMachine clientGameMachine;

    public ClientInThread(ClientGameMachine gm) {
        this.clientGameMachine = gm;
        printer = clientGameMachine.printer;
        this.chatWindow = clientGameMachine.chatWindow;
        this.in = clientGameMachine.in;
        printer.println(Protocol.CHANGENAME + " " + clientGameMachine.playerName);
        printer.flush();
    }

    /**
     * start our Game as Client
     *
     * @param s protocol message consisting of amount of player cnt and playerID of the client
     */
    private void startGame(String s) {

        int id;
        String name;
        int count = 0;


        while (s.contains((";"))) {
            id = Integer.parseInt(s.split(";", 2)[0]);
            s = s.split(";", 2)[1];
            name = s.split(";", 2)[0];
            s = s.split(";", 2)[1];

            count++;
            if (name.equals(clientGameMachine.playerName)) {
                clientGameMachine.playerID = id;
                clientGameMachine.players[id] = new ClientPlayer(name, id, new Vector2f(0, 0), DeadlineEscapeConf.PLAYER_WIDTH, DeadlineEscapeConf.PLAYER_HEIGHT);
            } else {
                clientGameMachine.players[id] = new Enemy(name, id, new Vector2f(0, 0), DeadlineEscapeConf.PLAYER_WIDTH, DeadlineEscapeConf.PLAYER_HEIGHT);
            }
            //clientGameMachine.playerStatuses.add(new PlayerStatus(id, name));

        }
        //clientGameMachine.playerStatuses.sort(Comparator.comparing(PlayerStatus::getPlayerID));


        clientGameMachine.lobbySize = count;
        clientGameMachine.gameStart = true;

    }

    /**
     * sets all players skins to their selected skin
     *
     * @param s the playerID and skinID encoded as in Protocol.SKIN
     */
    private void selectSkins(String s) {

        int id;
        int skinID;

        while (s.contains((";"))) {

            id = Integer.parseInt(s.split(";", 2)[0]);
            s = s.split(";", 2)[1];
            skinID = Integer.parseInt(s.split(";", 2)[0]);
            s = s.split(";", 2)[1];


            if (clientGameMachine.players[id] != null) {
                clientGameMachine.players[id].chooseSkin(skinID);
            }

        }

        clientGameMachine.skinsSelected = true;


    }


    /**
     * @param s String Input
     * @return Protocol Command, EXAMPLE = return "chat"
     */
    private String getProtocol(String s) {
        int space;
        space = s.indexOf(" ");
        return s.substring(0, space);
    }


    /**
     * Requests the High scores from the server
     */
    public void requestHS() {
        printer.println(Protocol.REQHS);
        printer.flush();
    }

    /**
     * ends the current game
     */
    private void endGame() {
        logger.debug("Server ended this game.");
        clientGameMachine.gameStart = false;
    }

    /**
     * places an item in the specified position
     *
     * @param s contains the itemID and its x- and y-position separated by ";" as used in Protocol.ITEMSPAWN
     */
    private void spawnItem(String s) {
        int itemID;
        float x;
        float y;

        itemID = Integer.parseInt(s.split(";", 2)[0]);

        x = Float.parseFloat(s.split(";", 3)[1]);

        y = Float.parseFloat(s.split(";", 3)[2]);

        clientGameMachine.items[itemID].activate(new Vector2f(x, y), -1);
    }

    /**
     * applies an item-effect to the specified player
     *
     * @param s contains the playerID and the itemID separated by ";"
     */
    private void buffPlayer(String s) {
        //plays our buff-Music-File
        //AudioManager.playBuff();

        int playerID;
        int itemID;

        playerID = ProtocolUtils.format2i(s, ";")[0];
        itemID = ProtocolUtils.format2i(s, ";")[1];


        if (clientGameMachine.players[playerID] != null) {
            clientGameMachine.players[playerID].effect(itemID);
            clientGameMachine.items[itemID].deactivate();
        }
        //AudioManager.stop();

    }

    /**
     * updates the positions of the players on the map
     * @param s contains the playerIDs and positions as used in Protocol.POS
     */
    private synchronized void updatePos(String s) {

        int playerID;
        float x;
        float y;


        while(s.contains((";"))) {
            playerID = Integer.parseInt(ProtocolUtils.format3s(s, ";")[0]);
            x = Float.parseFloat(ProtocolUtils.format3s(s, ";")[1]);
            y = Float.parseFloat(ProtocolUtils.format3s(s, ";")[2]);
            s = s.split(";", 4)[3];

            if (clientGameMachine.players[playerID] != null) {
                clientGameMachine.players[playerID].setServerPosition(x, y);
            }


        }

    }

    /**
     * updates velocities of the players
     * @param s contains the playerIDs and velocities as used in Protocol.VEL
     */
    private synchronized void updateVel(String s){
        int playerID;
        float x;
        float y;
        int semicolon;
        String firstSubstring;

        semicolon = s.indexOf(";");
        firstSubstring = s.substring(semicolon+1);

        s = firstSubstring;


        while(s.contains((";"))) {
            playerID = Integer.parseInt(ProtocolUtils.format3s(s, ";")[0]);
            x = Float.parseFloat(ProtocolUtils.format3s(s, ";")[1]);
            y = Float.parseFloat(ProtocolUtils.format3s(s, ";")[2]);
            s = s.split(";", 4)[3];


            if (clientGameMachine.players[playerID] != null) {
                clientGameMachine.players[playerID].setServerVelocity(x, y);
            }


        }

    }

    /**
     * sets the y-velocity of the specified player
     *
     * @param s contains the playerID and y-velocity separated by ";"
     */
    private void setVelY(String s) {
        int playerID;
        float velY;
        playerID = Integer.parseInt(s.split(";", 2)[0]);
        velY = Float.parseFloat(s.split(";", 2)[1].split(";", 2)[0]);

        if (clientGameMachine.players[playerID] != null) {
            clientGameMachine.players[playerID].setServerVelocityY(velY);
        }

    }

    private void setVelX(String s) {
        int playerID;
        float velX;
        playerID = Integer.parseInt(s.split(";", 2)[0]);
        velX = Float.parseFloat(s.split(";", 2)[1].split(";", 2)[0]);

        if (clientGameMachine.players[playerID] != null) {
            clientGameMachine.players[playerID].setServerVelocityX(velX);
        }

    }

    /**
     * sets the x-position of the specified player
     *
     * @param s contains the playerID and x-position separated by ";"
     */
    private void setPosX(String s) {
        int playerID;
        float posX;
        playerID = Integer.parseInt(s.split(";", 2)[0]);
        posX = Float.parseFloat(s.split(";", 2)[1].split(";", 2)[0]);
        if (clientGameMachine.players[playerID] != null) {
            clientGameMachine.players[playerID].setServerPositionX(posX);
        }

    }

    /**
     * sets the y-position of the specified player
     *
     * @param s contains the playerID and y-position separated by ";"
     */
    private void setPosY(String s) {
        int playerID;
        float posY;
        playerID = Integer.parseInt(s.split(";", 2)[0]);
        posY = Float.parseFloat(s.split(";", 2)[1].split(";", 2)[0]);

        if (clientGameMachine.players[playerID] != null) {
            clientGameMachine.players[playerID].setServerPositionY(posY);
        }

    }

    /**
     * marks a player
     *
     * @param s the playerID as String
     */
    private void mark(String s) {
        if (clientGameMachine.players[Integer.parseInt(s)] != null) {
            clientGameMachine.players[Integer.parseInt(s)].mark();
        }
        //play our mark-Music-File
        //AudioManager.playMarked();
    }

    /**
     * removes the mark from a player
     *
     * @param s the playerID as String
     */
    private void unmark(String s) {
        if (clientGameMachine.players[Integer.parseInt(s)] != null) {
            clientGameMachine.players[Integer.parseInt(s)].unmark();
        }
        // AudioManager.stop();
    }

    /**
     * updates the scores for the current round
     *
     * @param s the playerIDs and their scores encoded as in Protocol.ROUNDSCORE
     */
    private void updateRoundScore(String s) {
        int playerID;
        int score;
        playerID = ProtocolUtils.format2i(s, ";")[0];
        score = ProtocolUtils.format2i(s, ";")[1];
        if (clientGameMachine.players[playerID] != null) {
            clientGameMachine.players[playerID].setRoundScore(score);
        }
    }

    /**
     * updates the score for a player
     *
     * @param s the playerID and the score separated by ";"
     */
    private void updateScore(String s) {

        int id;
        int score;



        while (s.contains((";"))) {

            id = Integer.parseInt(s.split(";", 2)[0]);
            s = s.split(";", 2)[1];
            score = Integer.parseInt(s.split(";", 2)[0]);
            s = s.split(";", 2)[1];


            if (clientGameMachine.players[id] != null) {
                clientGameMachine.players[id].setRoundScore(0);
                clientGameMachine.players[id].setScore(score);
            }


        }
        clientGameMachine.roundPause = true;

    }

    /**
     * starts the next round of the game
     *
     * @param s the number of the next round as String
     */
    private void nextRound(String s) {
        clientGameMachine.currentRound = Integer.parseInt(s);
        clientGameMachine.roundPause = false;
    }


    /**
     * run-Methode from ClientInThread, you can change name here, chat, and also start the game
     */
    public void run() {
        Thread.currentThread().setPriority(10);
        String line;

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            while ((line = br.readLine()) != null) {
                int space = line.indexOf(" ");
                Protocol command = Protocol.valueOf(getProtocol(line));
                String firstSubstring = line.substring(space+1);
                switch(command) {
                    case CHAT:
                        chatWindow.print(firstSubstring);
                        break;
                    case NEWNAME:
                        this.clientGameMachine.playerName = firstSubstring;
                        break;
                    case CHANGENAME:
                        chatWindow.print("ERROR: requested name change");
                        break;
                    case LISTCONTENT:
                        chatWindow.showList(firstSubstring);
                        break;
                    case WISP:
                        int firstArg = firstSubstring.indexOf(";");
                        chatWindow.print(firstSubstring.substring(firstArg + 1));
                        break;
                    case JOINLOBBY:
                        clientGameMachine.lobbyID = Integer.parseInt(firstSubstring);
                        break;
                    case START:
                        startGame(firstSubstring);
                        break;
                    case VELOCX:
                        setVelX(firstSubstring);
                        break;
                    case VELOCY:
                        setVelY(firstSubstring);
                        break;
                    case POSX:
                        setPosX(firstSubstring);
                        break;
                    case POS:
                        updatePos(firstSubstring);
                        break;
                    case VEL:
                        updateVel(firstSubstring);
                        break;
                    case POSY:
                        setPosY(firstSubstring);
                        break;
                    case MARK:
                            mark(firstSubstring);
                        break;
                    case UNMARK:
                            unmark(firstSubstring);
                        break;
                    case ROUNDSCORE:
                            updateRoundScore(firstSubstring);
                        break;
                    case ENDROUND:
                        updateScore(firstSubstring);
                        break;

                    case HIGHSCORE:

                        chatWindow.showHighscore(firstSubstring);
                        break;
                    case END:
                        endGame();
                        break;
                    case ITEMSPAWN:
                        spawnItem(firstSubstring);
                        break;
                    case ITEMBUFF:
                        buffPlayer(firstSubstring);
                        break;

                    case SKIN:
                        selectSkins(firstSubstring);
                        break;
                    case ROUND:
                        nextRound(firstSubstring);
                        break;
                    case REMOVE:
                        clientGameMachine.removePlayer(Integer.parseInt(firstSubstring));
                        break;
                    default:
                        break;
                }
            }
        } catch (IOException e) {

            logger.error(e.toString());
            System.exit(1);
        }
    }
}
