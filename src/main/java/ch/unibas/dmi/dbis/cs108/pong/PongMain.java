package ch.unibas.dmi.dbis.cs108.pong;

public class PongMain {
    public static void main(String[] args){
        PongWorld world = new PongWorld();
        world.start();
    }
}
