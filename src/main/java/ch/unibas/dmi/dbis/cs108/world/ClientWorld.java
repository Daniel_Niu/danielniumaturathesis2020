package ch.unibas.dmi.dbis.cs108.world;


import ch.unibas.dmi.dbis.cs108.config.DeadlineEscapeConf;
import ch.unibas.dmi.dbis.cs108.entity.*;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.TextRenderer;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine.ServerGame;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

/**
 * @author Daniel Niu
 * World class for the clientside containing most of the game logic as the serverside
 */
public class ClientWorld extends World {
    private final Font font;
    private final Texture tex;
    private final TextRenderer textRenderer;
    private int timer = DeadlineEscapeConf.ROUND_DURATION * 60;
    //private final WorldGui worldGUI;

    /**
     * Constructor
     *
     * @param mapName           String folder name of the world containing the platforms.png file and entities.png file
     * @param clientGameMachine the current client to which it is connected
     */
    public ClientWorld(String mapName, ClientGameMachine clientGameMachine) {
        super(mapName);
        //worldGUI = new WorldGui();
        this.font = new Font(new java.awt.Font("ARIAL", java.awt.Font.PLAIN, 1));
        this.tex = new Texture("maps/map.png");
        this.textRenderer = new TextRenderer(new Vector4f(0, 0, 0, 100));
        //player initiation

        for (int i = 0; i < clientGameMachine.lobbySize; i++) {

            clientGameMachine.players[i].moveTo(new Vector2f(spawnLocations.get(i)));

            //highlight own player
            if (clientGameMachine.players[i] instanceof ClientPlayer) {
                clientGameMachine.players[i].highlight();
            }
            clientGameMachine.players[i].initGraphics();

        }
        clientGameMachine.portals.add(new Portal(DeadlineEscapeConf.BLUE_PORTAL, new Vector2f(2, -4), new Vector2f(width - 9, -height + 11), 7, 7, true, -1));
        clientGameMachine.portals.add(new Portal(DeadlineEscapeConf.YELLOW_PORTAL, new Vector2f(width - 3, -height + 11), new Vector2f(7, -4), 7, 7, true, 1));

        for (Portal portal : clientGameMachine.portals) {
            portal.initGraphics();
        }

        clientGameMachine.items[0] = new Item(0, 1, new Vector2f(0, 0), width / 30f, height / 15f);
        clientGameMachine.items[1] = new Item(1, 1, new Vector2f(0, 0), width / 30f, height / 15f);
        clientGameMachine.items[0].setAnimation(0, 1, 2, 0);
        clientGameMachine.items[1].setAnimation(0, 1, 2, 1);


    }

    /**
     * update method for each game tick
     *
     * @param delta             float value of the current relative tick length
     * @param input             Input for the keyinputs of the player playing on the client
     * @param clientGameMachine client to which it is connect to. The client
     *                          contains most of the variables used
     */
    public void update(Float delta, Input input, ClientGameMachine clientGameMachine) {


        for (Player player : clientGameMachine.players) {
            if (player != null) {
                player.update(delta, input, this);
                player.collideWithWorld(this);
                player.effectUpdate();
            }
        }
        if (timer > 0) {
            timer--;

        }
        /*for (Entity entity : clientGameMachine.renderObjects) {

            entity.update(delta, input, clientGameMachine.playerStatuses.get(entity.objectID), this);
            entity.collideWithWorld(this);


            entity.effectUpdate();
        }

         */
        //worldGUI.update(input);


    }

    @Override
    public void update(Float delta, ServerGame serverGame) {

    }

    public void resetTime() {
        this.timer = DeadlineEscapeConf.ROUND_DURATION * 60;
    }

    /**
     * Render methode used to render the world
     *
     * @param shader            Shader used for rendering
     * @param clientGameMachine client containing the Lists of entities being rendered
     */
    public void render(Shader shader, ClientGameMachine clientGameMachine) {

        Matrix4f projection = new Matrix4f().ortho2D(-1, 1, -1, 1);

        shader.bind();
        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("sampler", 0);
        shader.setUniform("screenPos", new Vector2f(0, -0));
        shader.setUniform("offset", new Vector4f(0f, 0f, 1f, 1f));
        shader.setUniform("projection", projection);
        shader.setUniform("pixelScale", new Vector2f(2, 2));

        this.tex.bind(0);
        RectangleAssets.getModel().render();

        projection = new Matrix4f().ortho2D(0, DeadlineEscapeConf.DISPLAY_WIDTH, -DeadlineEscapeConf.DISPLAY_HEIGHT, 0);
        textRenderer.render(Integer.toString(timer / 60), shader, clientGameMachine.bigFont, new Vector2f(1600, -100), projection);

        for (Portal portal : clientGameMachine.portals) {
            portal.render(shader, this);
        }
        for (Player player : clientGameMachine.players) {
            if (player != null) {
                player.render(shader, this);
            }
        }



        for (Item item : clientGameMachine.items) {

            if (item.active) {
                item.render(shader, this);
            }
        }

    }

    public void close() {
        //TODO implement close method
    }


}
