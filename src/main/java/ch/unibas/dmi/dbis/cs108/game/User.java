package ch.unibas.dmi.dbis.cs108.game;

import java.io.PrintWriter;

/**
 * User contains a PrintWriter and its corresponding username
 */
public class User {
    private String name;
    private PrintWriter printer;

    /**
     * Constructor
     * @param n the username of the user
     * @param pw the PrintWriter of the user
     */
    public User(String n, PrintWriter pw) {
        this.name = n;
        this.printer = pw;
    }

    /**
     * we can set our own name
     * @param n Input Name
     */
    public void setName(String n) {
        this.name = n;
    }

    /**
     * changes the Printer
     * @param pw our new printer
     */
    public void setPrinter(PrintWriter pw) {
        this.printer = pw;
    }

    /**
     * name-Getter
     * @return name from User
     */
    public String getName() {
        return name;
    }

    /**
     * printer-Getter
     * @return our Printer
     */
    public PrintWriter getPrinter() {
        return printer;
    }

    /**
     * checks if two printers are the same
     * @param other the other printer to compare to
     * @return true if the name and the PrintWriter are identical, false else
     */
    public boolean equals(User other) {
        return name.equals(other.getName()) && printer.equals(other.getPrinter());
    }
}