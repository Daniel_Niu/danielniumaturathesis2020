package ch.unibas.dmi.dbis.cs108.shaderutils;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Mesh class
 * is like a Model but Mesh defines vertices of an object
 * storing vertices typically in triangle form (3 values == 1 triangle)
 */
public class Mesh {
    private final Vertex[] vertices; //VertexArray
    private final int[] indices; //Int Array
    private Material material; //Material where our texture gets loaded
    private int vao, pbo, ibo, cbo, tbo; //VertexArrayObject, PositionBufferObject, IndicesBufferObject, ColorBufferObject, TextureBufferObject

    /**
     * Constructor of Mesh with texture
     *
     * @param vertices of our mesh
     * @param indices  of our mesh
     * @param material holds a texture inside
     */
    public Mesh(Vertex[] vertices, int[] indices, Material material) {
        this.vertices = vertices;
        this.indices = indices;
        this.material = material;
    }
    public Mesh(Vertex[] vertices, int[] indices) {
        this.vertices = vertices;
        this.indices = indices;

    }
    /**
     * creates our Mesh
     */
    public void create() {
        //creates material
        material.create();

        //generate VertexArray and bind it
        vao = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vao);

        //FloatBuffer holds the position Values from our mesh
        FloatBuffer positionBuffer = MemoryUtil.memAllocFloat(vertices.length * 3);
        float[] positionData = new float[vertices.length * 3];

        //stores X,Y,Z into the positionData Array
        for (int i = 0; i < vertices.length; i++) {
            positionData[i * 3] = vertices[i].getPosition().getX();
            positionData[i * 3 + 1] = vertices[i].getPosition().getY();
            positionData[i * 3 + 2] = vertices[i].getPosition().getZ();
        }

        //put x,y,z values from Array into our FloatBuffer
        positionBuffer.put(positionData).flip(); //puts values of positionDataArray into positionBuffer

        //stores the values from FloatBuffer inside our PositionBufferObject
        pbo = storeData(positionBuffer, 0, 3);

        //FloatBuffer holds the color Values from our Mesh
        FloatBuffer colorBuffer = MemoryUtil.memAllocFloat(vertices.length * 3);
        float[] colorData = new float[vertices.length * 3];

        //stores X,Y,Z into the colorData Array
        for (int i = 0; i < vertices.length; i++) {
            colorData[i * 3] = vertices[i].getPosition().getX();
            colorData[i * 3 + 1] = vertices[i].getPosition().getY();
            colorData[i * 3 + 2] = vertices[i].getPosition().getZ();
        }

        //put x,y,z values from Array into our FloatBuffer
        colorBuffer.put(colorData).flip(); //puts values of colorDataArray into colorBuffer

        //stores the values from FloatBuffer inside our ColorBufferObject
        cbo = storeData(colorBuffer, 1, 3);

        //FloatBuffer holds the texture Values from our Material
        FloatBuffer textureBuffer = MemoryUtil.memAllocFloat(vertices.length * 2);
        float[] textureData = new float[vertices.length * 2];

        //stores X,Y into the textureData Array
        for (int i = 0; i < vertices.length; i++) {
            textureData[i * 2] = vertices[i].getTextureCoord().getX();
            textureData[i * 2 + 1] = vertices[i].getTextureCoord().getY();
        }

        //puts x,y values of from Array into FloatBuffer
        textureBuffer.put(textureData).flip();

        //stores the values from FloatBuffer inside our TextureBufferObject
        tbo = storeData(textureBuffer, 2, 2);

        //put indices from mesh into our indicesBuffer
        IntBuffer indicesBuffer = MemoryUtil.memAllocInt(indices.length);
        indicesBuffer.put(indices).flip();

        //generateBuffer from our IndicesBufferObject
        ibo = GL15.glGenBuffers();

        //bind IBO to ArrayBuffer
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, ibo);

        //adds buffer to actionpoints
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);

        //unbind Buffer
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    /**
     * stores Values from FloatBuffers als int
     * @param buffer buffer where we get our values from
     * @param index id
     * @param size  size
     * @return bufferID which we can store in our BufferObjects (int)
     */
    private int storeData(FloatBuffer buffer, int index, int size) {
        //creates new BufferObject
        int bufferID = GL15.glGenBuffers();

        //bind it to our buffer
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, bufferID);

        //use values of buffer for our bufferID
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);

        //adds our buffer to an actionpoint
        GL20.glVertexAttribPointer(index, size, GL11.GL_FLOAT, false, 0, 0);

        //unbind our buffer
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        return bufferID;
    }

    /**
     * destroys our Mesh. destroys each existing Buffer and our VertexArray
     */
    public void destroy() {
        GL15.glDeleteBuffers(pbo);
        GL15.glDeleteBuffers(cbo);
        GL15.glDeleteBuffers(ibo);
        GL15.glDeleteBuffers(tbo);
        GL30.glDeleteVertexArrays(vao);
    }

    /**
     * Vertices- Getter
     * @return vertices[]
     */
    public Vertex[] getVertices() {
        return vertices;
    }

    /**
     * Indices- Getter
     * @return indices[]
     */
    public int[] getIndices() {
        return indices;
    }

    /**
     * VAO- Getter
     * @return int VertexArrayObject
     */
    public int getVAO() {
        return vao;
    }

    /**
     * PBO- Getter
     * @return int PositionBufferObject
     */
    public int getPBO() {
        return pbo;
    }

    /**
     * IBO- Getter
     * @return int IndicesBufferObject
     */
    public int getIBO() {
        return ibo;
    }

    /**
     * CBO-Getter
     * @return int ColorBufferObject
     */
    public int getCBO() { return cbo; }

    /**
     * TBO-Getter
     * @return int TextureBufferObject
     */
    public int getTBO() {
        return tbo;
    }

    /**
     * Material-Getter
     * @return our material
     */
    public Material getMaterial() {
        return material;
    }
}
