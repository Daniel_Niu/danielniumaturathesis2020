package ch.unibas.dmi.dbis.cs108.font;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;


/**
 * Font class creating fonts usable for OpenGL from awt font
 */
public class Font {
    private final Map<Character, Letter> chars = new HashMap<>();
    Logger logger = LogManager.getLogger(Font.class);
    private int fontID;
    private BufferedImage bufferedImage;
    private Vector2f imageSize;
    private final java.awt.Font font;
    private FontMetrics fontMetrics;
    private int i;


    /**
     * Constructor used to load awt font
     *
     * @param font awt Font
     */
    public Font(java.awt.Font font) {
        this.font = font;
        generateFont();
    }

    /**
     * creating Font using Graphics and awt Font
     */
    private void generateFont() {
        GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        Graphics2D graphics = gc.createCompatibleImage(1, 1, Transparency.TRANSLUCENT).createGraphics();
        graphics.setFont(font);

        fontMetrics = graphics.getFontMetrics();
        imageSize = new Vector2f(1024, 1024);
        bufferedImage = graphics.getDeviceConfiguration().createCompatibleImage((int) imageSize.x, (int) imageSize.y, Transparency.TRANSLUCENT);

        fontID = glGenTextures();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, fontID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int) imageSize.x, (int) imageSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, generateImage());

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    /**
     * generate Image buffer
     *
     * @return returns a buffer of the font png as Bytebuffer
     */
    private ByteBuffer generateImage() {
        Graphics2D g = (Graphics2D) bufferedImage.getGraphics();
        g.setFont(font);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        drawCharacters(g);
        return createBuffer();
    }

    /**
     * save metrics into our Glyph array
     *
     * @param g Graphics2D with a defined Font
     */
    private void drawCharacters(Graphics2D g) {
        int tempX = 0;
        int tempY = 0;
        float h = (float) (fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent());

        for (i = 32; i < 256; i++) {
            if (i == 127) continue;
            char c = (char) i;
            float charWidth = fontMetrics.charWidth(c);

            float advance = charWidth + 5;
            if (tempX + advance > imageSize.x) {
                tempX = 0;
                tempY += 1;
            }
            chars.put(c, new Letter(tempX / imageSize.x, (tempY * h) / imageSize.y, charWidth / imageSize.x, h / imageSize.y, charWidth, h));
            g.drawString(String.valueOf(c), tempX, fontMetrics.getMaxAscent() + (h * tempY));

            tempX += advance;
        }
    }

    private ByteBuffer createBuffer() {
        int w = (int) imageSize.x;
        int h = (int) imageSize.y;
        int[] pixels = new int[w * h];
        bufferedImage.getRGB(0, 0, w, h, pixels, 0, w);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(w * h * 4);

        for (i = 0; i < pixels.length; i++) {
            byteBuffer.put((byte) ((pixels[i] >> 16) & 0xFF));   //Red
            byteBuffer.put((byte) ((pixels[i] >> 8) & 0xFF));    //Green
            byteBuffer.put((byte) (pixels[i] & 0xFF));           //Blue
            byteBuffer.put((byte) ((pixels[i] >> 24) & 0xFF));   //Alpha
        }
        byteBuffer.flip();
        return byteBuffer;
    }

    public int ID() {
        return fontID;
    }

    public Map<Character, Letter> getCharacters() {
        return chars;
    }

    public int StringWith(String s) {
        return fontMetrics.stringWidth(s);
    }

}
