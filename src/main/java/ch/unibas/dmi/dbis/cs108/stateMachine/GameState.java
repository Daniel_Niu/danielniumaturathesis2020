package ch.unibas.dmi.dbis.cs108.stateMachine;

/**
 * @author Daniel Niu
 * GameState Interface for a StateMachine
 */
public interface GameState {
    /**
     * run method used for loops maintaining the state
     */
    void run();

    /**
     * initiates one state to be able to run the run() method
     */
    void start();
}
