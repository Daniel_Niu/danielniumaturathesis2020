package ch.unibas.dmi.dbis.cs108.entity;

import org.joml.Vector2f;

/**
 * Status class: containing status of a player seen by the client
 */
public class PlayerStatus extends Status {

    public boolean a;
    public boolean d;
    public boolean space;
    public String name;
    private int id;
    public int skinID;

    private boolean marked = false;
    public int roundScore = 0;
    public int score = 0;

    public PlayerStatus(int id, String name) {
        super(new PositionSize(), new Vector2f(0, 0));
        this.id = id;
        this.name = name;
        this.a = false;
        this.d = false;
        this.space = false;

        this.positioning.pos.x = 0.0f;
        this.positioning.pos.y = -0.0f;
    }

    /**
     * Constructor for the default case
     */
    public PlayerStatus() {
        super(new PositionSize(), new Vector2f(0, 0));
        this.a = false;
        this.d = false;
        this.space = false;

        this.positioning.pos.x = 0.0f;
        this.positioning.pos.y = -0.0f;
    }

    /**
     * Constructor
     *
     * @param x     float value of the x-center
     * @param y     float value of the y-center
     * @param width float value of the width of the Player
     * @param height float value of the height of the Player
     */
    public PlayerStatus(float x, float y, float width, float height) {
        super(new PositionSize(x, y, width, height), new Vector2f(0, 0));
        this.a = false;
        this.d = false;
        this.space = false;

    }

    public void setSkin(int skinID) {
        this.skinID = skinID;
    }

    /**
     * X-velocity getter
     *
     * @return return a float value of the current X-velocity, can be positive and negative
     */
    public float getVelX() {
        return this.movement.x;
    }

    /**
     * X-velocity setter
     * @param f     flaot velocity value in the x-direction
     */
    public void setVelX(float f) {
        this.movement.x = f;
    }

    /**
     * position setter
     * @param x     x coordinate of the new position
     * @param y     y coordinate of the new position
     */
    public void setPos(float x, float y) {
        this.positioning.pos.x = x;


        this.positioning.pos.y = y;
    }

    /**
     * updating method, adding two float values to the position vector
     * @param velX  float value added to the x part of the position vector
     * @param velY  float value added to the y part of the position vector
     */
    public void update(float velX, float velY) {
        this.positioning.pos.x += velX;
        this.positioning.pos.y += velY;
    }

    /**
     * velocity setter: 2 float values
     * @param x     float value for the new x-velocity
     * @param y     float value for the new y-velocity
     */
    public void setVel(float x, float y) {
        this.movement.x = x;
        this.movement.y = y;
    }

    public float getVelY() {
        return this.movement.y;
    }

    /**
     * Y-velocity setter
     * @param f     float velocity value in the y-direction
     */
    public void setVelY(float f) {
        this.movement.y = f;
    }

    public float getX() {
        return this.positioning.pos.x;
    }

    public void setX(float f) {

        this.positioning.pos.x = f;
    }

    public int getPlayerID() {
        return this.id;
    }

    public float getY() {
        return this.positioning.pos.y;
    }

    public void setY(float f) {
        this.positioning.pos.x = f;
    }

    public boolean isMarked() {
        return this.marked;
    }

    public void mark() {
        this.marked = true;
    }

    public void unmark() {
        this.marked = false;
    }

    public int getRoundScore() {
        return this.roundScore;
    }

    public void setRoundScore(int roundScore) {
        this.roundScore = roundScore;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return this.score;
    }

    /**
     * @return true, if A is pressed
     */
    public boolean isA() {
        return a;
    }

    /**
     *
     * @return true if D is pressed
     */
    public boolean isD() {
        return d;
    }

    /**
     *
     * @return true if Space is pressed
     */
    public boolean isSpace() {
        return space;
    }
}
