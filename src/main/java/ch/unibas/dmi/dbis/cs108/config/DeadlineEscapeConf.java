package ch.unibas.dmi.dbis.cs108.config;

public class DeadlineEscapeConf {

    public static final int DISPLAY_WIDTH = 1728;
    public static final int DISPLAY_HEIGHT = 972;

    public static final float PLAYER_WIDTH = 2.5f;

    public static final float PLAYER_HEIGHT = 3.2f;

    public static final int ROUND_NUMBER = 2;
    public static final int ROUND_DURATION = 60;

    //Object IDs
    public static final int YELLOW_PORTAL = 5;
    public static final int BLUE_PORTAL = 4;


    //Entities
    public static final float GRAVITY = 1f;

    public static final float X_VELOCITY = 10f;

    public static final float JUMP_AIR_COOL_DOWN = 20;

    public static final float MAX_FALL_SPEED = -60;

    //Item effects
    public static final float SPEED_BUFF_MULTIPLIER = 2;

    public static final int EFFECT_DURATION = 60 * 10;

}
