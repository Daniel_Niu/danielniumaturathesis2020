package ch.unibas.dmi.dbis.cs108.entity;

import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * @author Daniel Niu
 * Position class used to describe the size and center of an entity
 */
public class PositionSize {
    public Vector3f pos;
    public Vector3f size;

    /**
     * Default Constructor
     * position is set to 0|0
     * size is set to width: 1 height: 1
     */
    public PositionSize() {
        pos = new Vector3f();
        size = new Vector3f(1f, 1f, 0);

    }

    /**
     * Constructor
     *
     * @param x      float value of the x position
     * @param y      float value of the y postion
     * @param width  float value of the width
     * @param height float value of the height
     */
    public PositionSize(float x, float y, float width, float height) {
        pos = new Vector3f(x, y, 0);
        size = new Vector3f(width, height, 0);
    }

    public PositionSize(float x, float y, float z, float width, float height) {
        pos = new Vector3f(x, y, z);
        size = new Vector3f(width, height, 0);
    }

    /**
     * Translates the Matrix4f mat with the position and scales it with the size
     *
     * @param mat Matrix4f projection matrix
     * @return Matrix4f with tranlsation of position and scaling
     */
    public Matrix4f getTranslation(Matrix4f mat) {
        mat.translate(pos);

        mat.scale(size);
        return mat;
    }
}
