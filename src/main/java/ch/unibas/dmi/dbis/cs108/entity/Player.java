package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.config.DeadlineEscapeConf;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.LoopAnimation;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.TextRenderer;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import ch.unibas.dmi.dbis.cs108.world.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;


public class Player extends Entity {

    private static final int ANIMATIONS = 3;
    private static final int ANIMATION_IDLE = 0;
    private static final int ANIMATION_WALK = 1;
    private static final int ANIMATION_MARKED = 2;
    private final Logger logger = LogManager.getLogger(Player.class);
    private final String name;
    private final int playerID;
    public boolean shouldMoveLeft = false;
    public boolean shouldMoveRight = false;
    public boolean shouldJump = false;
    Vector2f serverPosition = new Vector2f(0, 0);
    Vector2f serverVelocity = new Vector2f(0, 0);
    private boolean marked = false;
    private int skinID = -1;
    private int roundScore = 0;
    private int markCD = 0;
    private int score = 0;


    private Font font;
    private TextRenderer textRenderer;
    private Texture mark;
    private Texture scorebar;
    private Texture scorebarFilling;

    /**
     * Constructor
     *
     * @param id       int Object id
     * @param position Start position of this entity
     */
    public Player(int id, String name, Vector2f position, float width, float height) {
        super(0, position, width, height);
        this.playerID = id;
        this.name = name;
    }


    public void chooseSkin(int skinID) {
        this.skinID = skinID;
    }

    public void initGraphics() {
        this.font = new Font(new java.awt.Font("ARIAL", java.awt.Font.PLAIN, 15));
        this.textRenderer = new TextRenderer(new Vector4f(0, 0, 0, 100));
        this.scorebar = new Texture("scorebar/scorebar.png");
        this.scorebarFilling = new Texture("scorebar/scorebarfilling.png");
        this.mark = new Texture("project.png");
        super.createAnimationArray(ANIMATIONS);
        super.setAnimation(ANIMATION_IDLE, new LoopAnimation(1, 10, "playerAnimations/" + skinID + "/IDLE"));
        super.setAnimation(ANIMATION_WALK, new LoopAnimation(4, 10, "playerAnimations/" + skinID + "/WALK"));
        super.setAnimation(ANIMATION_MARKED, new LoopAnimation(4, 4, "playerAnimations/clock"));
    }

    @Override
    public void update(float delta, Input input, ClientWorld clientWorld) {
        logger.error("Update method in superclass Player should never be evoked");
    }


    public void render(Shader shader, World world) {
        super.render(shader, world);
        int scorelength = getRoundScore() / 30;

        Vector2f position = new Vector2f(100 + 220 * playerID, -10);


        shader.bind();
        Matrix4f projection = new Matrix4f().setOrtho2D(0, DeadlineEscapeConf.DISPLAY_WIDTH, -DeadlineEscapeConf.DISPLAY_HEIGHT, 0);



        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("projection", projection);
        shader.setUniform("offset", new Vector4f(1, 1, 1, 1));
        shader.setUniform("sampler", 0);
        shader.setUniform("screenPos", new Vector2f(220 * playerID + 10 + scorelength / 2f, -10));
        shader.setUniform("pixelScale", new Vector2f(10 + scorelength, 10));

        scorebarFilling.bind(0);
        RectangleAssets.getModel().render();


        shader.setUniform("screenPos", position);
        shader.setUniform("pixelScale", new Vector2f(200, 10));
        scorebar.bind(0);
        RectangleAssets.getModel().render();


        RectangleAssets.getModel().render();
        textRenderer.render(getName(), shader, this.font, new Vector2f(5 + 220 * this.playerID, -32), projection);


        renderProject(shader, world);
    }


    private void renderProject(Shader shader, World world) {
        Matrix4f projection = new Matrix4f().setOrtho2D(0, world.width, -world.height, 0);

        if (marked) {
            shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
            shader.setUniform("projection", projection);
            shader.setUniform("offset", new Vector4f(1, 1, 1, 1));
            shader.setUniform("sampler", 0);
            shader.setUniform("pixelScale", new Vector2f(hitbox.getWidth() * flipX * 0.4f, hitbox.getHeight() * 0.4f));
            shader.setUniform("screenPos", new Vector2f(hitbox.getX(), hitbox.getY() + 4));
            this.mark.bind(0);

            RectangleAssets.getModel().render();
        }
    }

    public int getRoundScore() {
        return this.roundScore;
    }

    public int getPlayerID() {
        return this.playerID;
    }

    /**
     * Prepares the position information for use in the protocol
     *
     * @return A string in the form: "playerID;xPos;yPos"
     */
    public String printPos() {
        Vector2f vector = hitbox.getCenter();
        return this.playerID + ";" + vector.x + ";" + vector.y;
    }

    /**
     * increases roundScore by 1
     */
    public void addRoundScore() {
        this.roundScore++;
    }

    /**
     * adds the specified number to the score
     *
     * @param add the score increase
     */
    public void addScore(int add) {
        score += add;
    }

    public int getScore() {
        return this.score;
    }

    public void resetRoundScore() {
        this.roundScore = 0;
    }

    /**
     * sets marked to false
     * and starts a 60 tick protection from getting marked
     */
    public void unmark() {
        this.velocityX = DeadlineEscapeConf.X_VELOCITY;
        this.marked = false;
        this.markCD = 60;
    }

    public void setServerPosition(float x, float y) {
        this.serverPosition.x = x;
        this.serverPosition.y = y;
    }

    public void setServerPositionX(float x) {
        this.serverPosition.x = x;
    }

    public void setServerPositionY(float y) {
        this.serverPosition.y = y;
    }

    public void setServerVelocity(float x, float y) {
        this.serverVelocity.x = x;
        this.serverVelocity.y = y;
    }

    public void setServerVelocityY(float y) {
        this.serverVelocity.y = y;
    }

    public void setServerVelocityX(float x) {
        this.serverVelocity.x = x;
    }

    public void setRoundScore(int score) {
        this.roundScore = score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public int getSkinID() {
        return skinID;
    }

    public void mark() {
        this.marked = true;
        this.velocityX = DeadlineEscapeConf.X_VELOCITY * DeadlineEscapeConf.SPEED_BUFF_MULTIPLIER;
    }


    public boolean isMarked() {
        return this.marked;
    }

    /**
     * getter for current mark cooldown
     *
     * @return current mark cooldown
     */
    public int getMarkCD() {
        return this.markCD;
    }

    public String getName() {
        return this.name;
    }

    void discountMarkCD() {
        this.markCD--;
    }

    @Override
    public void update(float delta, PlayerStatus playerStatus, ClientWorld clientWorld) {

    }

    @Override
    public void update(float delta) {

    }
}
