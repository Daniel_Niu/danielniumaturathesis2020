package ch.unibas.dmi.dbis.cs108.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * our Lobby class
 * methods to set Player-Status, removePlayers, addPlayers and count Players in Lobby
 */
public class Lobby {
    private final Logger logger = LogManager.getLogger(Lobby.class);
    private int playerCnt;
    private String status;

    /**
     * Constructor
     * set Count to 1 because first player in lobby
     * set Status to in Lobby
     */
    public Lobby() {
        this.playerCnt = 1;
        this.status = "In Lobby";
    }

    /**
     * set Status to ingame
     */
    public void setInGame(){
        this.status = "In game";
    }

    /**
     * set Status to finished
     */
    public void setFinished() {
        this.status = "Finished";
    }

    /**
     * stat Status to in lobby
     */
    public void setInLobby() {
        this.status = "In Lobby";
    }

    /**
     * get number of players in lobby
     *
     * @return number of players
     */
    public int getPlayerCnt() {
        return this.playerCnt;
    }

    /**
     * remove a player from lobby
     * if no one is in the lobby, print error
     */
    public void removePlayer(){
        if(this.playerCnt==0){
            logger.error("lobby Player count is below 0");
        }
        this.playerCnt--;
    }

    /**
     * adds a Player as long there arent more than 5 players in the lobby
     * @return if player was added to the lobby
     */
    public boolean addPlayer(){
        if(playerCnt+1>5){
            return false;
        }
        if(status.equalsIgnoreCase("finished")){
            return false;
        }
        this.playerCnt++;
        return true;
    }


    public boolean isInGame(){
        return status.equals("In game");
    }
    /**
     * Status-Getter
     * @return Status from a player
     */
    public String getStatus(){
        return this.status;
    }
}
