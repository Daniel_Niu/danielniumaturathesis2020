package ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine;

import ch.unibas.dmi.dbis.cs108.audio.AudioManager;
import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.gui.RoundEndGUI;
import ch.unibas.dmi.dbis.cs108.io.Display;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.io.KeyEventSender;
import ch.unibas.dmi.dbis.cs108.shaderutils.FontAssets;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.stateMachine.GameState;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.opengl.GL11.*;

/**
 * @author Daniel Niu
 * GameState Class used during a game on the client
 */
public class InGame implements GameState {

    Logger logger = LogManager.getLogger(InGame.class);
    private final ClientGameMachine clientGameMachine;
    private boolean state = true;
    private Input input;
    private Display display;
    private Shader shader;

    /**
     * Constructor
     *
     * @param gm client to which it corresponds
     */
    InGame(ClientGameMachine gm) {
        clientGameMachine = gm;
    }

    /**
     * GameLoop of the Game being played
     */
    @Override
    public void run() {

        //start GameScreen with the amount of players
        ClientWorld clientWorld = new ClientWorld("test", clientGameMachine);
        RoundEndGUI roundEndGUI = new RoundEndGUI(clientGameMachine);
        clientGameMachine.world = clientWorld;
        //start our background Music
        AudioManager.playBackground();

        RectangleAssets.initAsset();
        FontAssets.initAsset();


        long lastTime = System.nanoTime();
        final double amountOfTicks = 60D;
        double ns = 1000000000 / amountOfTicks;
        float delta = 0;

        //GameScreen loop
        while ((!display.shouldClose() && !input.isKeyDown(GLFW.GLFW_KEY_ESCAPE)) && clientGameMachine.gameStart/*&& (System.currentTimeMillis() - startTime) < 100000*/) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1) {
                //update
                if (!clientGameMachine.roundPause) {
                    clientWorld.update((float) (1 / amountOfTicks), display.getInput(), clientGameMachine);
                }
                delta--;


            }
            display.update();



            glClearColor(0, 0, 0, 1);

            clientGameMachine.keyEventSender.sendInputs(input);
            glClear(GL_COLOR_BUFFER_BIT);
            if (!clientGameMachine.roundPause) {
                clientWorld.render(shader, clientGameMachine);
            } else {
                clientWorld.resetTime();
                roundEndGUI.render(shader, clientGameMachine.bigFont);
            }
            display.swapBuffers();

        }
        //stops our BackgroundMusic and our Effectsound (buff/marked)
        AudioManager.stop();
        AudioManager.stop();
        AudioManager.stop();
        state = false;
        //close Display
        clientGameMachine.setGameState(clientGameMachine.getEndGame());
        clientGameMachine.nextState();

    }

    /**
     * initiation of the key inputs and the KeyEventSender
     */
    @Override
    public void start() {
        clientGameMachine.input = clientGameMachine.disp.getInput();
        clientGameMachine.keyEventSender = new KeyEventSender(clientGameMachine);
        this.display = clientGameMachine.disp;

        this.shader = clientGameMachine.shader;

        this.input = clientGameMachine.input;

        run();

    }
}
