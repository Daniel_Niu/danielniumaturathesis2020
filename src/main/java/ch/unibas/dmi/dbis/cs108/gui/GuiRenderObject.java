package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;

public interface GuiRenderObject {
    void render(Shader shader);
}
