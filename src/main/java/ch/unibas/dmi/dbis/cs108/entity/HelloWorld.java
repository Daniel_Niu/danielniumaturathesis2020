package ch.unibas.dmi.dbis.cs108.entity;


/**
 * Class used for printing "Hello World"
 * @author Daniel Niu
 */
public class HelloWorld{
        int number;
        int secondNumber;

    /**
     * method used to pring HelloWorld out of the console
     */
    private static void printHelloWorld(){
        System.out.println("Hello World");
        }

    /**
     * main method
     * @param args  arguments from the initiation
     */
    public static void main(String[] args){
            printHelloWorld();
        }
}