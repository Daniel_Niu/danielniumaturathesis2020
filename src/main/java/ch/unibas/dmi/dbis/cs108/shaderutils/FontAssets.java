package ch.unibas.dmi.dbis.cs108.shaderutils;

public class FontAssets {
    private static Model model;

    public static Model getModel() {
        return model;
    }

    /**
     * initiate new Model with the vbo of a rectangle
     */
    public static void initAsset() {
        if (model == null) {
            float[] vertices = new float[]{
                    0f, 1f,
                    1f, 1f,
                    1f, 0f,
                    0f, 0f,
            };
            float[] texture = new float[]{
                    0, 0,
                    1, 0,
                    1, 1,
                    0, 1,
            };

            int[] indices = new int[]{
                    0, 1, 2,
                    2, 3, 0
            };

            model = new Model(vertices, texture, indices);
        }
    }

    /**
     * deletes initiated Model
     */
    public void deleteAsset() {
        model.destroy();
        model = null;
    }
}


