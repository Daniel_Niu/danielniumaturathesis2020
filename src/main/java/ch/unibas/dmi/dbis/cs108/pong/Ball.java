package ch.unibas.dmi.dbis.cs108.pong;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Vector2f;

public class Ball {

    Vector2f position;
    Vector2f velocity;
    Vector2f size = new Vector2f(100, 100);
    float velX = 10;
    Texture tex;


    public Ball(){
        position = new Vector2f(0,-500);
        velocity = new Vector2f(10,0);
    }

    public void initGraphics(){
        this.tex = new Texture("project.png");
    }

    public void checkCollision(SuperPong object) {

        Vector2f distance = object.position.sub(position, new Vector2f());
        distance.x = Math.abs(distance.x);
        distance.y = Math.abs(distance.y);

        distance.sub(size.add(object.size, new Vector2f()));

        if(distance.x < 0 && distance.y < 0){
            if(object instanceof Ping){
                velX = 1.01f*velX;
                this.velocity.x = velX;
                this.velocity.y = velocity.y + object.velocity.y * 0.5f;
            }
            if(object instanceof Pong){
                velX = 1.01f*velX;
                this.velocity.x = -velX;
                this.velocity.y = velocity.y + object.velocity.y * 0.5f;
            }
        }


    }


    public void update(Double delta, Input input){
        this.position.add(velocity);
        collideWithBorder();


    }

    public void collideWithBorder(){
        if(position.y>0 || position.y<-972){
            velocity.y = -velocity.y;
        }
        if(position.x<0){
            velocity.x = 10;
        }
        if(position.x>1728){
            velocity.x = -10;
        }

    }
}
