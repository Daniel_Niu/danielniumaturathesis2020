package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.config.DeadlineEscapeConf;
import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.LoopAnimation;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import ch.unibas.dmi.dbis.cs108.world.World;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

/**
 * @author Daniel
 * <p>
 * Abstract class for an Entity in a game
 */
public abstract class Entity {

    final int DURATION_TICKS = DeadlineEscapeConf.EFFECT_DURATION;
    final float JUMP_AIR_COOLDOWN = DeadlineEscapeConf.JUMP_AIR_COOL_DOWN;
    final float MAX_FALL_SPEED = DeadlineEscapeConf.MAX_FALL_SPEED;
    final float GRAVITY = DeadlineEscapeConf.GRAVITY;
    private final float SET_X_VELOCITY = DeadlineEscapeConf.X_VELOCITY;
    private final float SET_JUMP_VELOCITY = 23f;
    private boolean highlighted = false;
    float velocityJump = SET_JUMP_VELOCITY;
    float velocityX = SET_X_VELOCITY;
    float jump_air = 0;
    public boolean active = false;
    float velY = 0;
    float velX = 0;
    public int posID = -1;
    int activeTimer = 0;
    int effectDuration = 0;
    int flipX = 1;
    boolean marking = false;
    private LoopAnimation[] animation;
    private int currentEffect = -1;
    private Texture highlight;
    public Hitbox hitbox;
    public int objectID;
    private int currentAnimation;

    /**
     * Constructor
     *
     * @param id       int Object id
     * @param position x and y of the position
     * @param width    width of entity
     * @param height   height of entity
     */
    Entity(int id, Vector2f position, float width, float height) {
        this.objectID = id;
        this.hitbox = new Hitbox(position, width, height);
        this.currentAnimation = 0;
    }




    /**
     * Method for moving the entity in a specific direction
     *
     * @param direction Vector2f: x and y directions
     */
    void move(Vector2f direction) {
        hitbox.setCenter(hitbox.getCenter().add(new Vector2f(direction)));
    }

    void createAnimationArray(int length) {
        this.animation = new LoopAnimation[length];
    }

    void useAnimation(int id, int flipX) {
        this.currentAnimation = id;
        this.flipX = flipX;

    }


    /**
     * Method for moving the entity to a specific point
     *
     * @param target Vector2f: point P(x|y)
     */
    public void moveTo(Vector2f target) {
        hitbox.setCenter(new Vector2f(target));
    }

    public void effect(int effectID) {
        float SPEED_BUFF_MULTIPLIER = DeadlineEscapeConf.SPEED_BUFF_MULTIPLIER;
        int SET_EFFECT_DURATION = 60 * 5;
        switch (effectID) {
            case 0:
                velocityX = velocityX * SPEED_BUFF_MULTIPLIER;
                effectDuration = SET_EFFECT_DURATION;
                currentEffect = 0;
                break;
            case 1:
                velocityX = velocityX * -1;
                effectDuration = SET_EFFECT_DURATION;
                currentEffect = 1;
            default:
                break;
        }
    }

    /**
     * Sets animations
     *
     * @param index         int index of the Animation array
     * @param loopAnimation The Animation to be added
     */
    void setAnimation(int index, LoopAnimation loopAnimation) {
        animation[index] = loopAnimation;
    }

    /**
     * Methode for rendering this entity
     *
     * @param shader Shader used to render
     * @param world  World which gives the Orthomatrix for the position
     */
    public void render(Shader shader, World world) {

        Matrix4f projection = new Matrix4f().setOrtho2D(0, world.width, -world.height, 0);
        shader.bind();
        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", projection);
        shader.setUniform("screenPos", new Vector2f(hitbox.getCenter()));
        shader.setUniform("offset", new Vector4f(0, 0, 1, 1));
        shader.setUniform("pixelScale", new Vector2f(hitbox.getWidth() * flipX, hitbox.getHeight()));

        animation[currentAnimation].bind(0);
        RectangleAssets.getModel().render();

        if (highlighted) {
            highlight.bind(0);
            shader.setUniform("pixelScale", new Vector2f(hitbox.getWidth() * flipX * 0.25f, hitbox.getHeight() * 0.25f));
            shader.setUniform("screenPos", new Vector2f(hitbox.getX(), hitbox.getY() + 2.75f));
            RectangleAssets.getModel().render();
        }


    }

    /**
     * updates effects if the entity currently had an effect
     */
    public void effectUpdate() {
        if (this.effectDuration > 0) {
            effectDuration--;
        } else {
            if (currentEffect != -1) {

                switch (currentEffect) {
                    case 0:
                    case 1:
                        velocityX = SET_X_VELOCITY;
                        break;
                    default:
                        break;

                }
                currentEffect = -1;
            }
        }
    }


    /**
     * Collision of this entity with the World platforms/walls
     *
     * @param world World in which the platforms/walls are
     */
    public void collideWithWorld(World world) {

        hitbox.canJump = false;
        //5x5 field around the player detecting platform Hitboxes
        Hitbox[] boxes = new Hitbox[49];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                boxes[i + j * 7] = world.getPlatformHitBox((int) (hitbox.getX() + 0.25 - (7 / 2)) + i, (int) (-hitbox.getY() + 0.5 - (7 / 2)) + j);
            }
        }


        for (Hitbox value : boxes) {
            if (value != null) {
                hitbox.correctPosition(value);
            }
        }
        if (hitbox.canJump) {
            velY = -0.1f;
        }
        if (hitbox.hitHead) {
            jump_air = 0;
            velY -= 1.5f;
        }

        Hitbox box = null;

        for (Hitbox value : boxes) {
            if (value != null) {
                if (box == null) box = value;

                Vector2f length1 = box.getCenter().sub(hitbox.getCenter(), new Vector2f());
                Vector2f length2 = value.getCenter().sub(hitbox.getCenter(), new Vector2f());

                //finding the smallest distance of all boxes that are in the 5x5 box
                if (length1.lengthSquared() > length2.lengthSquared()) {
                    box = value;
                }
            }
        }
        if (box != null) {


            //the player is pushed out of the object
            hitbox.correctPosition(box);



            //if the correction pushed the player into another object
            for (Hitbox value : boxes) {
                if (value != null) {

                    Vector2f length1 = box.getCenter().sub(hitbox.getCenter(), new Vector2f());
                    Vector2f length2 = value.getCenter().sub(hitbox.getCenter(), new Vector2f());

                    if (length1.lengthSquared() > length2.lengthSquared()) {
                        box = value;
                    }
                }
            }


            hitbox.correctPosition(box);


        }
        if (hitbox.canJump) {
            velY = -0.1f;
        }
        if (hitbox.hitHead) {
            jump_air = 0;
            velY -= 1.5f;
        }



    }

    public void highlight() {
        this.highlighted = true;
        highlight = new Texture("highlight.png");
    }

    public abstract void update(float delta, Input input, ClientWorld clientWorld);

    public abstract void update(float delta, PlayerStatus playerStatus, ClientWorld clientWorld);

    public abstract void update(float delta);
}
