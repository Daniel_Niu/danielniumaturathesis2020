package ch.unibas.dmi.dbis.cs108.game;
public enum Protocol {
    /* All arguments are separated by spaces (' '). The first space separates the command from the first argument
     * and if there is a second argument the second space separates the first two arguments.
     * If there are more spaces than arguments taken by the command, the excess spaces are treated
     * as part of the last argument. */

    //COMMAND  //NAME //FUNCTION
    CHANGENAME,   /* CHANGENAME (String new name): Changes the name of the player.
                   Example: "CHANGENAME My New Name" changes the username to "My New Name"*/
    NEWNAME,    /* NEWNAME (String new name): sends changed name to client. Example: "NEWNAME Bob"*/
    CHAT,   /* CHAT (String message): sends a chat message.
             Example: "CHAT Hello World", where "Hello World" is the message*/
    BROADCAST,  /* BROADCAST (String message): Broadcasts a message to all players in all lobbies.
                 Example: "BROADCAST Hello World", where "Hello World" is the message*/
    LOBBYCHAT,  /* LOBBYCHAT (String message): Sends a message to all players in the same lobby. The lobbyID
                 is not given as an argument and is instead determined in the ServerThread.
                 Example: "LOBBYCHAT Hello World", where "Hello World" is the message*/
    LIST,   /* LIST: Request User List. It uses no arguments. Example: "LIST"*/
    LISTCONTENT,    /* LISTCONTENT (String message): Sends User list from the server to the client.
                     Example: "LISTCONTENT   Lobby 0 : 0/5 players;;  (you) me;  someone",
                     where the message contains all information to be shown. ';' encodes the '\n' symbol.*/
    A,      /* A (boolean on/off):   The User pressed a -> on the releases a -> off.
             Example: "A true" indicates that the A-button is pressed.
             on/off is a boolean indicated by "true" or "false".*/
    D,      /* D (boolean on/off):   The User pressed d -> on the releases d -> off.
             Example: "D true" indicates that the D-button is pressed.
             on/off is a boolean indicated by "true" or "false".*/
    SPACE,  /* SPACE (boolean on/off):   The User pressed space -> on the releases space -> off.
             Example: "SPACE true" indicates that the spacebar is pressed.
             on/off is a boolean indicated by "true" or "false".*/
    JOIN,   /* JOIN (int Lobbynumber): player joins the lobby of the given ID.
             Lobbynummer needs to be an Integer number.
             Example: "JOIN 14" joins to the lobby with the ID 14.*/
    JOINLOBBY,/*JOINLOBBY (int Lobbynumber): player has joined the lobby of the given ID.
               Lobbynumber needs to be an Integer number. If Lobbynubmer is -1 it means that the player could
               not join the lobby.
               Example: "JOINLOBBY 1" tells the client that it has joined the lobby 1
              */
    LOBBYCREATE, /* LOBBYCREATE: creates a new lobby and adds the user to that lobby. Example: "LOBBYCREATE"*/
    START,  /* START (PlayerCount;PlayerID): Starts the game. PlayerCount is the number of players in the game
             and PlayerID is the ID of the Client.
             Example: "START 3;0" where PlayerCount == 3 and PlayerID == 0 and the values are separated by ";".
             Both arguments are Integer values.*/
    WISP,  /* WISP (target username; message): sends a whispered message to another player.
            Example: "WISP Hallo Welt;Ich sage hallo!"
            sends the message "Ich sage hallo!" to the player "Hallo Welt"*/
    VEL,     /* VEL (int id;float xVelo;float yVelo): Sends a list of player velocities to the clients.
              Example: "VEL 0;0.0;2.1" sends the velocities of player 0.*/
    VELOCX,  /* VELOCX (int id;float xVelo): used to transmit x-velocity changes of the players.
              Example: "VELOCX 0;1.0" sets the x-velocity of the player with id == 0 to 1.0
              id is an Integer value and xVelo is a float value.*/
    VELOCY,  /* VELOCY (int id;float yVelo): used to transmit y-velocity changes of the players.
              Example: "VELOCY 0;1.0" sets the y-velocity of the player with id == 0 to 1.0
              id is an Integer value and yVelo is a float value.*/
    POS,     /* POS (int playerID;float xPos;float yPos): Sends the positions of the given players to all clients.
              Example: "POS 0;1.0;2.2;1;0.8;4.1" sends the positions of the players 0 and 1.*/
    POSX,    /* POSX (int id;float xPos): Used to transmit the x-position of the players
              Example: "POSX 0;0.0" sets the x-position of the player with id == 0 to the point 0.0
              id is a Integer value and xPos is a float value.*/
    POSY,    /* POSY (int id;float yPos): Used to transmit the y-position of the players
              Example: "POSY 0;0.0" sets the y-position of the player with id == 0 to the point 0.0
              id is a Integer value and yPos is a float value.*/

    PLAYERID,/* PLAYERID (int id): used to transmit id.*/
    RDY,    /* RDY (int skinID): Is sent by the client and signals the server that the player is ready to start.
             It also sends the ID of the skin the client uses.
             Example: "RDY 2"*/
    MARK,   /* MARK (int id): The client marks the player with the given ID.
             Example: "MARK 0" marks the player with id == 0. id is an Integer value.*/
    UNMARK, /* UNMARK (int id): The client unmarks the player with the ID of the argument.
             Example: "UNMARK 1" unmarks the player with id == 1. id is an Integer value.*/
    ROUNDSCORE,  /* ROUND (int userID;int score): Sends the roundscore of the player in the current round to the client.
             Example: "SCORE 0;1000" sends the score "1000" of the player with ID == "0".
             userID and score are both Integer values separated by ";".*/
    SCORE,  /* SCORE (int playerID;int score): sends the score to client. Uses arbitrarily many pairs
             of playerID;score. Example: "SCORE 0;20;1;0"*/
    END,    /* END: Server tells the client to end the game. Example: "END"*/
    REQHS,  /* REQHS: "Request Highscore" Client requests highscore from server Example: "REQHS"*/
    HIGHSCORE,  /* HIGHSCORE (highscore content): Server sends highscore information to client.
                 Example: "HIGHSCORE NAME1;1000;NAME2;1999;" where the argument is the highscore list
                 with NAME;SCORE; and ";" is used to separate the datapoints of the list.*/
    STARTGAME, /* STARTGAME (Lobbynumber): Client tells the lobby to start the game of this number.
                Example: "STARTGAME 1"
                */
    ENDROUND,   /* ENDROUND (int id;int score): Server tells the clients to end the round and gives a list of playerscores
                 separated by ";".
                 Example: "ENDROUND 1;0;2;20;3;40" where player 1 has 0 points, player 2 has 20 and player 3 has 40.*/
    ITEMSPAWN,  /* ITEMSPAWN (int itemID;float xPos;float yPos): Used to spawn an item on the given position.
                 Example: "ITEMSPAWN 1;0.0;0.0" spawns the item with id 1 in position 0.0/0.0*/
    ITEMBUFF,   /* ITEMBUFF (int playerID;int itemID): gives the specified player the effect of the chosen item.
                 Example: "ITEMBUFF 0;1" gives the player with ID 0 the effect of item 1.*/
    SKIN,   /* SKIN (int playerID;int skinID): Sends a list of players and their respective skins
             separated by ";" to the client.
             Example: "SKIN 1;0;2;4;3;2"*/
    ROUND,  /* ROUND (int round number): Tells the client to start the next round. Example: "ROUND 2"*/
    REMOVE, /* REMOVE (int id): removes the player with the given ID from the game. Example: "REMOVE 1"*/
}
