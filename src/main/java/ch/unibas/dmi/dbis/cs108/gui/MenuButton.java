package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.font.Font;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import ch.unibas.dmi.dbis.cs108.stateMachine.clientStateMachine.ClientGameMachine;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

/**
 * a pressable button for the menu
 */
public class MenuButton extends GuiObject {


    private final Texture[] textures = new Texture[3];
    public int skinID;

    /**
     * Constructor
     * @param id the ID of the button
     * @param position the position on screen
     * @param width the width of the buttons hitbox
     * @param height the height of the buttons hitbox
     */
    public MenuButton(int id, Vector2f position, float width, float height) {
        super(id, position, width, height);


        Texture tex1 = new Texture("menubutton/0.png");
        Texture tex2 = new Texture("menubutton/1.png");
        Texture tex3 = new Texture("menubutton/2.png");

        setTexture(0, tex1);
        setTexture(1, tex2);
        setTexture(2, tex3);

    }

    /**
     * sets the textures used by the button
     *
     * @param index 0 is used for the default texture, 1 for when the button is selected, 2 when the button is pressed
     * @param tex   the new texture
     */
    private void setTexture(int index, Texture tex) {
        textures[index] = tex;
    }

    /**
     * renders the button
     * @param shader the shader used
     * @param clientGameMachine the ClientGameMachine this button is used for
     * @param font the font for the button text
     */
    @Override
    public void render(Shader shader, ClientGameMachine clientGameMachine, Font font) {
        Vector2f position = hitbox.getCenter();

        Vector2f scale = hitbox.getHalf_extent();
        Matrix4f projection = new Matrix4f().setOrtho2D(0, clientGameMachine.displayWidth, clientGameMachine.displayHeight, 0);
        shader.bind();
        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", projection);
        shader.setUniform("screenPos", new Vector2f(position.x, position.y));
        shader.setUniform("offset", new Vector4f(0, 0, 1, 1));
        shader.setUniform("pixelScale", new Vector2f(scale.x * 2, -scale.y * 2));


        //asses which texture
        switch (state) {
            case SELECTED:
                textures[1].bind(0);

                break;
            case CLICKED:
                textures[2].bind(0);
                clientGameMachine.endScreen = false;
                break;
            default:
                textures[0].bind(0);
                break;
        }
        RectangleAssets.getModel().render();
    }


}


