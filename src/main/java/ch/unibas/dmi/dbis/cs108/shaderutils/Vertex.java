package ch.unibas.dmi.dbis.cs108.shaderutils;

import ch.unibas.dmi.dbis.cs108.utils.Vector2f;
import ch.unibas.dmi.dbis.cs108.utils.Vector3f;

/**
 * Vertex Class, holds Attributes of an object
 * used for our
 */
public class Vertex {
    private final Vector3f position;
    private final Vector3f color;
    private final Vector2f textureCoord;

    /**
     * Constructor
     *
     * @param position     Vector3f to get our position from our object
     * @param color        Vector3f to get our color form our object
     * @param textureCoord Vector2f to get the x,y coordinates from our texture
     */
    public Vertex(Vector3f position, Vector3f color, Vector2f textureCoord) {
        this.position = position;
        this.color = color;
        this.textureCoord = textureCoord;
    }

    /**
     * position-Getter
     * @return our position (Vector3f)
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * color-Getter
     * @return our color (Vector3f)
     */
    public Vector3f getColor() {
        return color;
    }

    /**
     * textureCoord-Getter
     * @return our coordinates from our Texture
     */
    public Vector2f getTextureCoord() {
        return textureCoord;
    }
}
