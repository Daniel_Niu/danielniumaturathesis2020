package ch.unibas.dmi.dbis.cs108.server;
import ch.unibas.dmi.dbis.cs108.Highscore;
import ch.unibas.dmi.dbis.cs108.entity.PlayerStatus;
import ch.unibas.dmi.dbis.cs108.game.Lobby;
import ch.unibas.dmi.dbis.cs108.game.Protocol;
import ch.unibas.dmi.dbis.cs108.game.User;
import ch.unibas.dmi.dbis.cs108.stateMachine.serverStateMachine.ServerGame;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


/**
 * the thread to which the clients connect
 */
public class ServerInThread implements Runnable {
    private static final Logger logger = LogManager.getLogger(ServerInThread.class);
    private static final List<User> users = new ArrayList<>();
    private final User userInfo;
    private final int id;
    private final Socket socket;
    private boolean rdy = false;
    private final InputStream in;
    private final PrintWriter printer;
    public String name;
    private final Server server;
    private final PlayerStatus playerStatus = new PlayerStatus();
    public int playerID;
    public int skinID;
    public int lobbyID = -1;



    /**
     * Constructor
     *
     * @param id     id of the client
     * @param socket socket to which the client connects
     * @param server the server to which the thread is connected to
     * @throws IOException includes Socket.getInputStream()
     */
    ServerInThread(int id, Socket socket, Server server) throws IOException {
        this.server = server;
        this.name = Integer.toString(id);   //id of the Client will be its initial name
        this.id = id;                       //id is defined
        this.socket = socket;               //the own socket is added
        this.in = socket.getInputStream();       //inputStream is saved
        this.printer = new PrintWriter(socket.getOutputStream());     //outputStream is saved
        this.userInfo = new User(name, printer);
    }

    /**
     * sends a message to one specific user
     *
     * @param name     the name of the sender
     * @param argument the username of the target player and the message separated by ";"
     */
    private static void whisper(String name, String argument) {
        int semicolon = argument.indexOf(";");
        String target = argument.substring(0, semicolon);
        String line = argument.substring(semicolon + 1);
        PrintWriter me = getPrinter(name);
        if (me == null) {
            return;
        }
        PrintWriter temp = getPrinter(target);
        if (temp == null) {
            me.println("WISP " + name + ";ERROR: This user does not exist");
            me.flush();
            return;
        }
        logger.debug(name + " whispered: " + line);
        temp.println("WISP " + target + ";" + name + " whispered: " + line);  //whispered message to target
        temp.flush();
        me.println("WISP " + name + ";you whispered to " + target + ": " + line); //confirmation message to sender
        me.flush();
    }

    /**
     * A method, which checks if the name is already in the list of names
     *
     * @param newName name
     * @return Shows if the name has not been used yet, true means not used
     */
    private boolean checkName(String newName) {
        int namesOccurrence = server.names.indexOf(newName);
        return namesOccurrence == -1 || server.names.isEmpty();
    }

    public synchronized boolean isRdy() {
        return this.rdy;
    }

    /**
     * shows how many Players are in Lobby and the Status of the lobbies (inGame, in Lobby)
     *
     * @return String with the infos
     */
    private String lobbyToString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Open Lobbies:;");
        for (int i = 0; i < server.lobbies.size(); i++) {
            sb.append("  Lobby ").append(i).append(": ").append(server.lobbies.get(i).getPlayerCnt()).append("/5 Players  Status: ").append(server.lobbies.get(i).getStatus()).append(";");
        }
        if (this.lobbyID != -1) {
            sb.append(" ;");
            sb.append("Players in your lobby: ");
            sb.append("Lobby ").append(this.lobbyID).append(";");
            for (ServerInThread t : server.serverInThreads) {
                if (t.lobbyID == this.lobbyID) {
                    sb.append("  ");
                    if (t.name.equals(this.name)) {
                        sb.append("(You) ");
                    }
                    sb.append(t.name).append(";");

                }
            }
        }
        return sb.toString();
    }

    /**
     * Removes a name from the list of names
     *
     * @param name name
     */
    public void removeName(String name) {
        server.names.remove(name);
    }

    /**
     * Adds a name to the static list of names
     *
     * @param name name
     */
    private void addName(String name) {
        server.names.add(name);
    }

    /**
     * startGame for a Player
     *
     * @param s a single string encoding the player count and
     *          the playerID of the client according to START in the protocol
     *          Format: playerID +";"+ playerName+";"
     *          Example 2 players: 0 + ";" + "Bob0" + ";" + 1 + ";" + "Bob1" + ";"
     */
    public void startGame(String s) {
        printer.println(Protocol.START.name() + " " + s);
        printer.flush();
    }

    /**
     * Changes the name of a player
     * Checks whether the name is taken or not, if not taken it adds the new name and deletes the old name
     *
     * @param name name
     */
    private void changeName(String name) {
        if (name.contains(";")) {
            printer.println("CHAT ERROR: Your username may not contain ';'");
            printer.flush();
            return;
        }
        if (checkName(name)) {
            if (!server.names.isEmpty()) {
                server.names.remove(this.name);
            }
            server.names.add(name);
            server.broadcast("Server", "The user " + this.name + "'s name was changed to " + name + ".");
            printer.println(Protocol.NEWNAME + " " + name);
            updateUsers(name, printer);
            this.name = name;
        } else {
            printer.println("CHAT " + name + " has already been taken.");
            for (int i = 1; i < 100; i++) {
                if (checkName(name + "_" + i)) {
                    changeName(name + "_" + i);
                    break;
                }
            }
        }
    }

    /**
     * update our User Infos
     *
     * @param newName    new Name for the User
     * @param newPrinter new Printer for the User
     */
    private void updateUsers(String newName, PrintWriter newPrinter) {
        if (!users.isEmpty()) {
            users.remove(this.userInfo);
        }
        this.userInfo.setName(newName);
        this.userInfo.setPrinter(newPrinter);
        users.add(userInfo);
    }

    /**
     * printer-Getter
     * @return our Printer
     */
    public PrintWriter getPrinter(){
        return this.printer;
    }

    /**
     * A-Getter
     * @return true if A is pressed
     */
    public boolean getA() {
        return this.playerStatus.a;
    }


    /**
     * sends player list as String
     */
    private void sendList() {
        printer.print(Protocol.LISTCONTENT.name() + " " + lobbyToString());
        printer.print(";All Players:");
        printer.print(";" + server.getList() + "\n");
        printer.flush();
    }

    /**
     * D-Getter
     *
     * @return true D is pressed
     */
    public boolean getD() {
        return this.playerStatus.d;
    }

    /**
     * checks if String is same as anotherString
     *
     * @param s String to compare
     */
    private void inputA(String s) {
        if (s.equalsIgnoreCase("true")) {
            this.playerStatus.a = true;

        }
        if (s.equalsIgnoreCase("false")) {
            this.playerStatus.a = false;
        }
    }

    /**
     * Space-Getter
     *
     * @return true if Space is Pressed
     */
    public boolean getSpace() {
        return this.playerStatus.space;
    }

    /**
     * checks if String is same as another String
     *
     * @param s String to compare
     */
    private void inputD(String s) {
        if (s.equalsIgnoreCase("true")) {
            this.playerStatus.d = true;
        }
        if (s.equalsIgnoreCase("false")) {
            this.playerStatus.d = false;
        }
    }

    /**
     * check if String is same as another String
     *
     * @param s String to compare
     */
    private void inputSpace(String s) {

        if (s.equalsIgnoreCase("true")) {
            this.playerStatus.space = true;
        }
        if (s.equalsIgnoreCase("false")) {
            this.playerStatus.space = false;
        }
    }

    /**
     * takes a username and finds the associated PrintWriter
     * @param name the username
     * @return the PrintWriter that belongs to the username
     */
    private static PrintWriter getPrinter(String name) {
        for(User u: users){
            if(name.equals(u.getName())){
                return u.getPrinter();
            }
        }
        return null;
    }

    /**
     * takes a string and isolates its first word
     *
     * @param s the input string
     * @return the first word of the input
     */
    private String getProtocol(String s) {
        int space;
        space = s.indexOf(" ");
        if (space == -1) {
            return s;
        }
        return s.substring(0, space);
    }

    /**
     * joining Lobby
     *
     * @param s String (number of lobby which i wanna join)
     */
    private void joinLobby(String s) {
        int l = Integer.parseInt(s);
        if (l >= server.lobbies.size() || l < 0) {
            printer.println(Protocol.CHAT.name() + " Lobby does not exist.");
            printer.flush();
            return;
        }
        if (server.lobbies.get(l).addPlayer()) {
            printer.println(Protocol.JOINLOBBY + " " + l);
            printer.flush();
            if (this.lobbyID != -1) {
                server.lobbies.get(this.lobbyID).removePlayer();
            }
            this.lobbyID = l;

        } else {
            printer.println(Protocol.CHAT.name() + " The Lobby is full.");
            printer.flush();
        }

    }


    /**
     * sends the Highscore after a client requests it
     */
    private void sendHighscore() throws IOException {

        loadHighscore();
        printer.println(Protocol.HIGHSCORE + " " + Highscore.printHighscore());
        printer.flush();
    }

    private void loadHighscore() throws IOException {
        try {
            Highscore.load();
        } catch (IOException e) {
            logger.error("Could not load Highscore");
            File highscore = new File("Highscore.csv");
            if (highscore.createNewFile()) {
                logger.debug("Created new Highscore.csv file");
            }
        }
    }

    /**
     * creates a Lobby
     */
    private void createLobby() {
        int newLobbyID = server.lobbies.size();
        //the player was not previously in a lobby
        if (lobbyID == -1) {

            server.lobbies.add(newLobbyID, new Lobby());
            this.lobbyID = newLobbyID;

        } else {
            server.lobbies.add(newLobbyID, new Lobby());
            server.lobbies.get(this.lobbyID).removePlayer();
            this.lobbyID = newLobbyID;
        }
        printer.println(Protocol.JOINLOBBY + " " + newLobbyID);
        printer.flush();

    }


    /**
     * run-Methode from ServerInThread
     * you can create a Lobby, change name, chat, whisper etc.
     */
    public void run() {

        try {
            server.broadcast("Server", "User " + this.name + " has joined.");
            printer.println(Protocol.CHAT.name() + " Please select a lobby");

            printer.flush();


            //InputStream processed by BufferedReader
            addName(Integer.toString(this.id));
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;

            loadHighscore();

            //waits for messages from clients
            while ((line = br.readLine()) != null) {
                Protocol command = Protocol.valueOf(getProtocol(line));
                int space = line.indexOf(" ");
                String firstSubstring = line.substring(space+1);
                switch(command){
                    case CHANGENAME:
                        changeName(firstSubstring);
                        break;

                    case BROADCAST:
                        server.broadcast(name, line.substring((space+1)));
                        break;

                    case LOBBYCHAT:
                        server.lobbyChat(this.lobbyID, this.name + ": " + line.substring((space+1)));
                        break;

                    case LIST:
                        sendList();
                        break;
                    case A:
                        inputA(firstSubstring);
                        break;
                    case D:
                        inputD(firstSubstring);
                        break;
                    case SPACE:
                        inputSpace(firstSubstring);
                        break;
                    case JOIN:
                        joinLobby(firstSubstring);
                        break;
                    case LOBBYCREATE:
                        createLobby();
                        break;

                    case REQHS:
                        sendHighscore();
                        break;

                    case WISP:
                        whisper(name, firstSubstring);
                        break;

                    case RDY:
                        synchronized (this) {
                            this.rdy = true;
                            skinID = Integer.parseInt(firstSubstring);
                        }

                        break;

                    case STARTGAME:
                        if (Integer.parseInt(firstSubstring) == -1) {
                            printer.println(Protocol.CHAT + " You are currently not in a lobby");
                            printer.flush();
                        } else {
                            server.startLobby(Integer.parseInt(firstSubstring));

                        }
                        break;

                    default:
                        logger.debug("Received message without command: " + line);

                }


            }

            server.printWriters.remove(this.printer);
            server.broadcast("Server", "Player " + name + " left.");
            in.close();
            printer.close();
            socket.close();
            server.names.remove(this.name);
            users.remove(this.userInfo);

        } catch (IOException e) {
            logger.error(e.toString());
            server.printWriters.remove(this.printer);
            server.broadcast("Server", this.name + " has crashed.");
            server.names.remove(this.name);
            server.serverInThreads.remove(this);
            users.remove(this.userInfo);
            if (this.lobbyID != -1) {
                server.lobbies.get(this.lobbyID).removePlayer();
                if (server.lobbies.get(lobbyID).isInGame()) {
                    ServerGame temp = server.serverGames.get(lobbyID);
                    temp.removePlayer(playerID);
                }
            }

            this.lobbyID = -1;
        }

    }

    /**
     * Exports the Array List of names as a String.
     *
     * @return returns the String with all the player names.
     */
    public String getList() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < server.names.size(); i++) {
            sb.append("  ").append(server.names.get(i)).append(";");
        }

        return sb.toString();
    }

    /**
     * name getter
     *
     * @return player name
     */
    public String getName(){
        return this.name;
    }
}

