package ch.unibas.dmi.dbis.cs108.pong;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Vector2f;
import org.lwjgl.glfw.GLFW;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;


public class Ping extends SuperPong{
    Texture tex;

    private boolean up = true;
    private boolean down = true;

    public Ping(){
        super(new Vector2f(100,-300), new Vector2f(0,0));
    }
    public void initGraphics(){
        this.tex = new Texture("project.png");
    }
    @Override
    public void update(double delta, Input input) {

        velocity.y = 0;

        if (!input.isKeyDown(GLFW.GLFW_KEY_W)) {
            down = true;
        }
        if (!input.isKeyDown(GLFW.GLFW_KEY_S)) {
            up = true;
        }

        //MOVE LEFT
        if (input.isKeyDown(GLFW.GLFW_KEY_W) && up && position.y < 0) {

            velocity.y = 10;
            down = false;
        }

        //MOVE RIGHT
        if (input.isKeyDown(GLFW.GLFW_KEY_S) && down && position.y > -972) {

            velocity.y = -10;
            up = false;
        }

        position.add(velocity);


    }
}
