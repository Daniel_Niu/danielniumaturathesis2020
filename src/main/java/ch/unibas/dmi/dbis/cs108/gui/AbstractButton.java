package ch.unibas.dmi.dbis.cs108.gui;

import ch.unibas.dmi.dbis.cs108.entity.Hitbox;
import ch.unibas.dmi.dbis.cs108.entity.RectangleAssets;
import ch.unibas.dmi.dbis.cs108.shaderutils.Positioning;
import ch.unibas.dmi.dbis.cs108.shaderutils.Shader;
import ch.unibas.dmi.dbis.cs108.shaderutils.Texture;
import org.joml.Vector2f;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;


abstract class AbstractButton {
    private final Hitbox hitbox;
    private final int SELECTED = 1;
    private final int CLICKED = 2;
    private final int IDLE = 0;
    List<Object> actionList = new ArrayList<>();
    Texture[] textures = new Texture[3];
    private int state;

    AbstractButton(Hitbox hitbox) {
        this.hitbox = hitbox;
    }

    public void addActionEvent(ClickedFunction cf) {
        actionList.add(cf);
    }

    public void update(Vector2f mouse, boolean leftClick) {
        if (hitbox.intersects(mouse)) {
            state = SELECTED;
            if (leftClick) {
                state = CLICKED;
                for (Object function : actionList) {
                    ClickedFunction f = (ClickedFunction) function;
                    f.exec();
                }
            }
        } else {
            state = IDLE;
        }
    }


    public void render(Shader shader, Positioning positioning) {
        shader.bind();
        textures[state].bind(0);
        shader.setUniform("matColor", new Vector4f(1, 1, 1, 1));
        shader.setUniform("projection", positioning.projection);
        shader.setUniform("sampler", 0);
        shader.setUniform("screenPos", positioning.screenPos);
        shader.setUniform("offset", new Vector4f(0, 0, 1, 1));
        shader.setUniform("pixelScale", new Vector2f(positioning.xScale, -positioning.yScale));
        RectangleAssets.initAsset();
        RectangleAssets.getModel().render();
    }

}
