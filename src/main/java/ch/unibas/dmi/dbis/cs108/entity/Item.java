package ch.unibas.dmi.dbis.cs108.entity;

import ch.unibas.dmi.dbis.cs108.io.Input;
import ch.unibas.dmi.dbis.cs108.shaderutils.LoopAnimation;
import ch.unibas.dmi.dbis.cs108.world.ClientWorld;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;

/**
 * items that can spawn during the game
 */
public class Item extends Entity {


    List<Object> effectList = new ArrayList<>();
    
    /**
     * Constructor
     *
     * @param id               int Object id
     * @param animation_length int animation duration
     * @param position         Start position of this entity
     */
    public Item(int id, int animation_length, Vector2f position, float width, float height) {
        super(id, position, width, height);
        super.createAnimationArray(animation_length);
    }

    /**
     * sets the animation of the item
     * @param index int index of the Animation array
     * @param length the length of the animation
     * @param frames the number of frames of the animation
     * @param id the id of the animation
     */
    public void setAnimation(int index, int length, int frames, int id) {
        super.setAnimation(index, new LoopAnimation(length, frames, "item/" + id));
    }

    /**
     * checks whether the item has a collision with an entity
     * @param e the other entity
     * @return true if the hitboxes overlap, false else
     */
    public boolean itemCollision(Entity e) {
        Collision collision = hitbox.getCollision(e.hitbox);
        if (collision.overlap && e.effectDuration == 0) {
            active = false;
            e.effect(objectID);
            return true;
        }
        return false;
    }

    public int getObjectID() {
        return objectID;
    }

    @Override
    public void update(float delta, Input input, ClientWorld clientWorld) {

    }

    @Override
    public void update(float delta, PlayerStatus playerStatus, ClientWorld clientWorld) {

    }

    /**
     * deactivates the item
     */
    public void deactivate() {
        activeTimer = 0;
        active = false;
    }


    public void activate(Vector2f target, int posID) {
        moveTo(target);
        this.posID = posID;
        activeTimer = DURATION_TICKS;
        active = true;
    }

    /**
     * causes the item to deactivate after some time
     *
     * @param delta the duration of one tick, most cases 1 / 60 as this game normally is 60 ticks per second
     */
    @Override
    public void update(float delta) {
        if (activeTimer == 0) {
            active = false;
        } else {
            activeTimer--;
        }
    }
}
