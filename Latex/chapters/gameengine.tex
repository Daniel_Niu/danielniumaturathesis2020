\setchapterpreamble[u]{\margintoc}
\chapter{Game Engine}
\labch{gameengine}

\textbf{Abstract} In this chapter we will discuss the game engine which is the basic environment that we have to create before even thinking about implementing game specific features. We will thereafter discuss what a game engine is and what basic concepts needs to be implemented. We will cover the game loop, game entities and the state management.

\section{What is a game engine?}

The game engine is the core of each video game. It includes the basic functionality that nearly every game is based on. Graphic engine, physic engine, sound system, state management, data storage, threading and network connections. 
\begin{description}
	\item[Graphic engine] How can the game elements be shown: drawing game object onto the screen.
	\item[Physic engine]Managing physics of a world so that it makes sense: Movement of objects, Collision detection, gravity.
	\item[Sound system] Accessing the audio player on the specific device and play the right sound at the right time.
	\item[State management] How and where the states are saved and how the game is handled according to the game rules.
	\item[Data storage] Saving and loading player data and current progress.
	\item[Threading] Coordinating the tasks that have to be executed simultaneously. Multitasking between different task.
	\item[Network connections] handling interaction between client and server and other users.
\end{description}
Professional game developers usually only build upon finished game engines that are specifically made for game development. The most popular game engines are: “CryEngine”, “Frostbite”, “Unity Engine” and “Unreal Engine”.
These game engines often include tools and editors that allow the developer to work on and express creative ideas without much programming.\sidecite{WikipediaGameEngine}
\section{Game Loop}
The game loop is the central part of most games. It receives the inputs by the player and calculates the outcome according to the game’s rules and the game state to then \glsadd{render}render/“draw” the game elements onto the screen as seen in \textbf{Figure 3.1}. In the case of real time games, the game loop continues even if the player gives no input. For example, by standing around if no button is pressed.

\begin{figure}[h]

\includegraphics[width=12cm]{gameloop.png}
\labfig{gameloop}
\caption{Game Loop}
\end{figure}

The game loop is characterized by 2 main methods: the update method and the render method. The task of the update method is to update the parameters and states for each game object from one time-unit to the other. It should apply game physics and test for interactions between other game objects. Hence, it should be executed in a consistent frequency, as movements are only little jumps that happen with every update. This can be achieved by comparing the time before the update and after to calculate if the duration of one time-unit was reached, if not then it will simply not update until it is reached. The game loop of this game project was capped at 60 TPS (ticks/updates per second).

\begin{lstlisting}[language=Java,caption=Tick Limiter]
long lastTime = System.nanoTime();
final double amountOfTicks = 60D;
double ns = 1000000000 / amountOfTicks;
float delta = 0;

//Game Loop
while (running){
    long now = System.nanoTime();
    delta += (now - lastTime) / ns;
    lastTime = now;
    if (delta >= 1) {
        //update
        update()
        delta--;
    }
    render();

}

\end{lstlisting}
\begin{marginfigure}[-2.5cm]
	\includegraphics{gameloopuml}
	\caption[Game Loop UML]{Game Loop UML} 
	\labfig{gameloopuml}
\end{marginfigure}
The render method is responsible for “drawing” game objects onto the screen. The frequency in which this occurs does not have to be capped since it represents the FPS (frames per second) of the game which should be as high as possible depending on the computer performance. However, since the game of this project was not expected to be a high performance game with special visual effects like motion-blur we had decided to only render the values connected to the update method, which practically caps the FPS at 60.
The update and the render methods should both be present in each game object. Each game object should be responsible to draw itself correctly, this has the advantage over a centralized render system by preventing the complexity of the loop to explode with increasing types of game objects. The system of a game loop which was used in this project is depicted in \textbf{Figure 3.2}. 


The Class GameLoop holds an object of the Class World which contains a List of the class Entitiy that include all the Game Objects in the game world. If the method update() is \glsadd{invoke}invoked in GameLoop the method update() in class World will then be executed which will then execute update for all entities in the List, allowing each object to update and render itself. The simplest form could look something like \textbf{Figure 3.3}.
\begin{marginfigure}[-7.5cm]
	\includegraphics{gameloop3}
	\caption[Game Loop Code]{Game Loop Code} 
	\labfig{gameloopcode}
\end{marginfigure}


\section{Game Entities}
Entity component system (ECS) is the architectural pattern that we’ve used for this project. It follows the “composition over inheritance principle”. It means that every object in a game scene is an entity (e.g player, enemy, items, etc.) and that this is achieved by interfaces and abstract class inheritance. The advantage of this system is that the behaviour of an entity can be changed during runtime by systems that add, remove or mutate components.\sidecite{WikipediaEntity} To achieve this, every class of an entity has methods that are preset by their implemented interfaces or extended abstract superclasses. The next example will show this concept practically.
\begin{lstlisting}[language=Java,caption=Entity class, label=lst:entity]
Abstract class Entity{
	Vector2f position ;

	public abstract void update();
}
\end{lstlisting}
\textbf{Listing \ref{lst:entity}} shows an abstract superclass named Entity. That means that every entity deriving this class has to implement a \textbf{new update method}. As different entities of a game behave differently this is a logical choice. For example, an update method of a player should consist of moving the player according to the input, as in contrast to an Item entity that should stay on its place and count down its duration. 
This structure allows us \textbf{to access every entity with the same method but resulting into different outcomes} allowing us to put every entity into the same List without considering its type.

\begin{lstlisting}[language=Java,caption=World class, label=lst:world]
Class World{
	
	List<Entity> entities = new ArraysList<>;
	
	Public World(){
		
		entities.add(new Player());
		entities.add(new Item());
		
	}
	public void update(){
		for(Entity e: entities){
			e.update();
		}
	
}

\end{lstlisting}

\textbf{Listing \ref{lst:world}} is the world in which every entity exists. The world saves all entities in a List. The versatility of an Entity List is shown on line 7 and 8 where Player and Items can both be added to the Entity List, as explained before: every game object is an entity and therefore deriving from the Entity class.
The advantage now is that if every object needs to be updated, then we can simply go through the whole List and invoke all the update methods. We assume that every object themselves know what to do and therefore avoid long logic chains where we check the type of entity. The part \textbf{for(Entity e: entities)} means that we go trough all objects in the List.

Another feature of the Entity system is the implementation of interfaces, in our game we chose to use abstract classes since Java provides it and it combines the advantages of a superclass and interfaces. However, use of interfaces in the context of an entity system can be seen in our GUI system. An example here:

\begin{lstlisting}[language=Java,caption=RenderObject interface, label=lst:renderobject]
Interface GuiRenderObject{
	void render(Shader shader);
}
\end{lstlisting}

and

\begin{lstlisting}[language=Java,caption=UpdatableObject interface, label=lst:updatableobject]
Interface GuiUpdatableObject{
	void update(double x, double y, boolean leftButton);
}
\end{lstlisting}

We have two interfaces that will force the objects that implement them to write a method that is defined in the interface. A "renderobject" needs a render method to draw itself, whereas an "updatableobject" needs a update method to update itself. The update method of a GUI object needs the location of the mouse pointer and the boolean of the leftButton to check whether the mouse is hovering it and/or clicking it.  (more complex GUI system will include the right mouse button allowing right click operations)

\begin{lstlisting}[language=Java,caption=Button Class, label=lst:button]
Class Button implements GuiRenderObject, GuiUpdatableObject{
	@Override
	public void render(Shader shader){
	}

	@Override
	public void update(double x, double y, boolean mouseLeft){
	}
}

\end{lstlisting}
The Button class shown in \textbf{Listing \ref{lst:button}} is a GUI object that can be seen on the screen and can be clicked therefore both interfaces have to be implemented. This forces the Button class to include both methods, indicated with @Override.

\begin{lstlisting}[language=Java,caption=Image Class, label=lst:image]
Class Image implements GuiRenderObject{
	@Override
	public void render(Shader shader){
	}
}
\end{lstlisting}

The Image class shown in \textbf{Listing \ref{lst:image}} is a GUI object that can be seen but does not interact with the mouse or other user inputs, hence only the RenderObject interface has to be implemented.
\begin{lstlisting}[language=Java,caption=GUIScene Class, label=lst:guiscene]
Class GUIScene{
	List<GuiRenderObject> renderObjects = new ArrayList<>;
	List<GuiUpdatableObject> updatableObjects = new ArraysList<>;

	public GUIScene(){
		Button button = new Button();
		Image image = new Image();
			

		renderObjects.add(button);
		renderObjects.add(image);
		updatableObjects.add(button;
	}
	public update(double x, double y, boolean mouseLeft){
		for(GuiUpdatableObject o : updatableObjects){
			o.update(x, y, mouseLeft);
		}
	}
	public render(Shader shader){
		for(GuiRenderObject o: renderObjects){
			o.render(shader);
		}
	}
}
\end{lstlisting}
Now putting everything together in the GUIScene \textbf{Listing \ref{lst:guiscene}}. You can see the similarity  between the GUIScene and the World class \textbf{Listing \ref{lst:world}}. The difference now is that there are two List meaning two different \textbf{Entity systems}. Both system function like earlier in the World class where different objects have the same method, that do different things in each object type. The render method in a button should consider its state, whether it is clicked or not and whether the mouse is hovering it. Whereas an image does not change and only considers its size and position.
\section{Summary}
In this section we have discussed what a game engine is and how different games can have the same game engine but with different game rules and texture. We have discussed that a game is made out of a game loop that updates multiple times per second so that it seems like a continues process. The structure and concept of Entity Component System was explained using example classes. We now know how to structure a game engine. The update method and the render method are still unknown but we know in which context they should be invoked.