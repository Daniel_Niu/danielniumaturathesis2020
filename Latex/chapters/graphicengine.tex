\setchapterpreamble[u]{\margintoc}
\chapter{Graphic Engine}
\labch{graphicengine}
The innerworkings of a game at its core only consists of numbers and Strings. For the sake of accessibility and enjoyment of the game it must be shown on the screen. The job of the graphics engine is to be the bridge between controlling each LED-pixel on the screen and the numbers representing the game state. In our project, OpenGL was used for this task. 

\textbf{Abstract} This section will discuss what OpenGL is and how programming with OpenGL will look like. Note that all the example code are using LWJGL(low weight java game library), which is a java package that include all the OpenGL methods. OpenGL is usually used in C++, but since our project is Java based, we had to use a Java library. However, the implementation on C++ and Java are nearly identical.

Furthermore, we will show an example of how we rendered a 2D image and point at the source code of our project, so that the reader can view and understand the code that we used.

\section{What is OpenGL?}
Open graphics library(OpenGL) on the one hand is an \acrfull{apiLabel} for various programming languages, meaning that it is an external library that you can download, allowing client applications like a game to access the graphics card on the computer system. The game logic normally runs on your processor (CPU: central processing unit) and the graphic calculations on your graphics card. Hence, \textbf{graphics card(GPU: graphic processing unit)}

On the other hand, it is also a driver on the graphics card that includes the implementation of the OpenGL rendering system. Each graphics card vendor has to ensure that OpenGL is supported on the hardware. The OpenGL rendering system allows the translation from OpenGL API commands into GPU(graphics processing unit) commands. Simply speaking: OpenGL is a library that give you commands with which you can calculate things on your graphics card which makes things a lot faster.\sidecite{khronos}

\section{OpenGL as a state machine}
OpenGL unlike a usual object-oriented-library works like a large state machine. Instead of creating objects of itself, it only uses its static class as a collection of variables shadowing in the background. The use of OpenGL involves changing variables and defining the current state by using static methods: the current state of OpenGL is referred to as the “OpenGL context”.
So, if you want to draw triangles onto the screen for example, we would have to change the current OpenGL context to triangles hinting that our next values that we will feed OpenGL will be 3 sets of 3D numbers representing the vertices of a new triangle.
\section{Object in OpenGL are IDs}
An object is a set of options and settings that is a subset of the OpenGL context. It functions similarly to a functional interface in java, as it is a function that can be saved and reused. It allows the user to save specific configurations of a task onto an id allowing a more detailed context. With call of its id ,the OpenGL context then knows that these configurations have to be applied and the task at hand should be performed in this manner.
Example:

\begin{lstlisting}[language=Java,caption=OpenGL Object Example, label=lst:openglobject]
int bufferObjectID = glGenBuffers();
glBindBuffer(GL_ARRAY_BUFFER, bufferObjectID);
glBufferData(GL_ARRAY_BUFFER, data, GL_STATIC_DRAW);
//unbiding all GL_ARRAY_BUFFERs
glBindBuffer(GL_ARRAY_BUFFER,0);
\end{lstlisting}


\textbf{Listing \ref{lst:openglobject}} shows how a sequence of OpenGL commands could create such an object. First the object must be generated, this creates a reference in form of an integer id. This object can then be put into the current OpenGL context by binding it. This allows us to define the options of this specific object. The expressions in capital letters represent the type of object which is being handled and how this object will be used. The third command allows us to give this object parameters. In this case the data given to this object could be an array of numbers representing vertices of an image object. Like for example the relation between every point of a triangle. At the end of this sequence the object is being unbound and therefore put out of the OpenGL context and saved in the background by the OpenGL state machine.
\section{From Vertices to Images}
OpenGL acts in the 3D space, all coordinates of vertices of shapes are defined as 3 dimensional vectors. But since our computer monitor is a 2D plane all the 3D shapes have to be compressed and transformed into 2D pixels that our monitor can show. The process of projecting 3D coordinates onto the 2D windows is handled by the graphics pipeline of OpenGL. Note that OpenGL is usually used for 3D objects, therefore the inherent 3 dimensional design.

\begin{figure}[h]
\includegraphics[width=8cm]{openglpipeline.png}
\labfig{pipeline}
\caption{OpenGL Pipeline,\\
learnopengl, accessed 20.09.2020\\
\url{https://learnopengl.com/Getting-started/Hello-Triangle} }
\end{figure}

\textbf{Figure 4.1} shows an abstract representation of the graphics pipeline. The first step is the vertex shader, this step takes in vertex data which are 3D coordinates representing vertices of shapes. This then undergoes shape assembly and geometry shader which defines the connections between the vertices forming the edges. Rasterization then, assigns and defines the pixels that correspond to the shape, for then the fragment shader to assign color to each pixel. In the last step “test and blending” the different objects and shapes are tested whether they are behind each other and how high the opacity is to create and assemble the scene correctly.
The steps colored blue are the steps where the user can write their own shaders in. Shaders are program that are loaded onto the graphics card handling the data, which the graphics card receives from the \glsadd{cpu}CPU. Shaders act similar to OpenGL object by creating a context in which the data will be used. In this game project, the shaders were rather simple since this game is only 2D and does not consider lighting or other effects. The shapes formed by the vertex shader are always rectangular as we’ve only rendered .png images onto the screen.\sidecite{learnopengl}
\section{Shaders for 2D scenes}
As mentioned in the previous section: shaders are small programs that run on the graphics card in this case during the OpenGL pipeline. There are 2 shaders that every program using OpenGL should include: a vertex shader and a fragment shader. The shaders usually include one page of code structured like \textbf{Listing 4.2} with the version at the top, followed by the in and out parameters and then the main program. The programming language is \acrfull{glslLabel} which is very similar to the C programming language.
The task of shaders is straightforward: take in data, process data and then output data for the next step in the pipeline. The data given to a shader can be from another shader or also from the running program on the CPU, as shaders run on the GPU(Graphics card) and not on the processor. The data that is specific for shaders to take in and put out is defined by the variable names. 
\clearpage
\subsection{Vertex Shader}
\begin{lstlisting}[language=GLSL,caption=Vertex Shader, label=lst:vertexshader] 
#version 140

attribute vec2 vertices;
attribute vec2 textures;

out vec4 color;
out vec2 uvCoords;

uniform vec4 matColor;
uniform mat4 projection;
uniform vec4 offset;
uniform vec2 pixelScale;
uniform vec2 screenPos;

void main()
{
   color = matColor;

   gl_Position = projection * vec4((vertices * pixelScale) + screenPos, 0, 1);
   uvCoords = (textures * offset.zw) + offset.xy;
}
\end{lstlisting}
\textbf{Listing \ref{lst:vertexshader}} shows the vertex shader that was used for this project. It is a versatile shader capable of rendering 2D rectangular graphics with any 2D image. Each parameters has its meaning and will be discussed in this section: 
\textbf{Attribute \color{OliveGreen}{vec2} \color{Black}vertices} and \textbf{attribute \color{OliveGreen}{vec2} \color{Black}vertices} are defined by the Model class in this project, their task is to define the space on which the textures should be stretched over. In this project:

\begin{lstlisting}[language=Java,caption= Vertex Buffer Object, label=lst:vbo]
float[] vertices = new float[]{
        -0.5f, 0.5f,
        0.5f, 0.5f,
        0.5f, -0.5f,
        -0.5f, -0.5f,
};
float[] texture = new float[]{
        0, 0,
        1, 0,
        1,1,
        0, 1,
};

int[] indices = new int[]{
        0, 1, 2,
        2, 3, 0
};

\end{lstlisting}
\begin{figure}[h]
\includegraphics[width=10cm]{vbo.png}
\labfig{vbo}
\caption{Vertex Buffer Illustration,\\
Wikipedia, accessed 20.09.2020\\
\url{http://wiki.lwjgl.org/wiki/File:Coordinates.png.html}}
\end{figure}
The vertices represent the vertices of the object, in this case a square with the width and height of 1.0.  The texture coordinates represent the corners of the st coordinates that the picture is stretched over, also known as UV coordinates, OpenGL takes in png files and normalizes them so that the left upper corner is (0, 0) and right bottom corner is (1, 1) as seen in \textbf{Figure 4.2}. The indices tell OpenGL how to connect the vertices and the uv-coordinates, this happens in sets of 3 since 3D graphic always consists of triangles. It is common in 3D graphics to define the complexity of an object in the amount of triangles it consists of. In our case it would be two triangles creating one rectangle. The \textbf{Listing \ref{lst:vbo}} shows the creation of a Vertex Buffer Object(VBO), which happens in the class “Model” under \textbf{src\textbackslash main\textbackslash java\textbackslash ch\textbackslash unibas\textbackslash dmi\textbackslash bis\textbackslash cs108\textbackslash shaderutils\textbackslash Model.java}.\\ Note that model can also be referred to as mesh in this case. This creation saves our model onto the graphics card, to allow fast rendering of this specific model/mesh. This step could seem a bit unnecessary since our model only consists of two triangles, however OpenGL is usually used for 3D graphics which involves much more complex objects with up to thousands of triangles. The attributes seen in \textbf{Listing \ref{lst:vertexshader}} represent only 1 point, therefore it could look something like vec2 vertices = -0.5, 0.5 and vec2 textures = 0,0, which makes a bit more sense if we look at \textbf{Figure 4.2} being the upper left corner.
\begin{figure}[h]
\includegraphics[width=10cm]{uvmap.png}
\labfig{uvmap}
\caption{Texture to UVMap and onto Model,\\
Wikipedia, accessed 20.09.2020\\
\url{https://de.wikipedia.org/wiki/UV-Koordinaten}}
\end{figure}
\\
The \textbf{out \color{OliveGreen}{vec4} \color{Black}color} and the \textbf{out \color{OliveGreen}{vec2} \color{Black}uvCoords} parameters of \textbf{Listing \ref{lst:vbo}} are for the Fragment shader. \textbf{uvCoords} defining the position on the UV Map from where it should take the color from. The UV Map is the normalized png image file in this case. The \textbf{vec4 color} defines the intensity of each color channel, amplify or lessen the chromatic colors red, green or blue (RGB) and the fourth channel alpha, which is the opacity of the color. This happens by vector multiplication with the color value sampled from the UV Map. The color vector should be (1,1,1,1) in the normal case where the exact texture color should be shown.

\textbf{Uniforms} are variable that are changed by the CPU during runtime.
The meaning of each uniform are the following:
\begin{description}
	\item \textbf{uniform \color{OliveGreen}{vec4} \color{Black}matColor} defines color intensity for the fragment shader.
	\item \textbf{uniform \color{OliveGreen}{mat4} \color{Black}projection} is a 4 dimensional matrix used for camera projections. In this case of a 2D game it uses an orthographic-2D-Matrix which allows us to map the current vision frame of the world coordinates onto the normalized devices coordinates. Meaning that we only have to consider the top, bottom x-coordinates and left, right y-coordinates of our current camera field of view and the x and y coordinate of the center of object that are visible.
	

\end{description}
\[
v_{normalized} =
	\begin{bmatrix}
		\frac{2}{right-left} & 0 & 0 & -\frac{right+left}{right-left}\\[6pt]
		0 & \frac{2}{top-bottom} & 0 & -\frac{top+bottom}{top-bottom}\\[6pt]
		0 & 0 & \frac{-2}{far-near} & -\frac{far+near}{far-near}\\[6pt]
		0 & 0 & 0 & 1
	\end{bmatrix}
\begin{bmatrix}
		x\\[6pt]
		y\\[6pt]
		z\\[6pt]
		0
\end{bmatrix}
\]
\begin{figure}[h]
\includegraphics[width=12cm]{ndc.png}
\labfig{ndc}
\caption{Normalized Device Coordinates,\\
stackoverflow, accessed 20.09.2020\\
\url{https://stackoverflow.com/questions/49705057/not-understanding-the-gluortho2d-function}}
\end{figure}

\begin{description}
	\item \textbf{uniform \color{OliveGreen}{vec4} \color{Black}offset} is used if the user wants to only show a portion of the uv map. If the user wants to show the whole UV Map, then the vector would look like this: (0, 0, 1, 1). (0, 0, 0.5, 0.5) would only be the upper left quarter. This should become more clear if we look at \textbf{Figure 4.2}
	\item \textbf{uniform \color{OliveGreen}{vec2} \color{Black}pixelScale} defines the width and height of the object that should be rendered. The normal width and height of an object is 1 as mentioned  and seen in \textbf{Figure 4.2}, this is the case for pixelScale = (1,1). If pixelScale would be (2, 2) then the object would fill the whole screen if it were in the center of our camera, since the nomralized device space has width and height 2 as seen in \textbf{Figure 4.4}.
	\item \textbf{uniform \color{OliveGreen}{vec2} \color{Black}screenPos} are the x and y coordinates of the object center in the context of the game world.
\end{description}	



\subsection{Fragment Shader}
\begin{lstlisting}[language=GLSL,caption=Fragment Shader, label=lst:fragmentshader]
#version 140

uniform sampler2D sampler;

in vec4 color;
in vec2 uvCoords;

void main()
{
   gl_FragColor = color * texture2D(sampler, uvCoords);
}

\end{lstlisting}

The fragment shader defines the color of each pixel in the model. It receives the parameter uvCoords and color from the vertex shader. The uvCoords are the coordinate on the uv Map or the png file. It “samples” the color value and multiplies it with the color vector which was defined in the vertex shader. Gl\_ FragColor is the specific key variable of the fragment shader defining the color value of the specific pixel of the model and its opacity.
\section{Rendering 2D image code example}
The necessary classes that you should create for rendering your first 2D image onto a screen are:
\begin{description}
	\item[Display Class], can also be called Window class: using a framework to create a Window for OpenGL
	\item[Shader Class] consisting of a vertex and a fragment shader, the initiation should give the graphics card the shaders as parameters.
	\item[Texture Class] Converting png files into ByteBuffers, so that it can be saved onto the graphics card.
	\item[Model Class] Defining the shape of the graphic object and how you want to stretch the texture in our case png image over it.
\end{description}

All classes can be found in the gitlab repository in the package under: \textbf{src\textbackslash main\textbackslash java\textbackslash ch\textbackslash unibas\textbackslash dmi\textbackslash bis\textbackslash cs108\textbackslash shaderutils}.
\section{Summary}
We have discussed what a graphic engine is and how graphic programming in OpenGL would look like. There are programs that run on the processor and there programs that run on the graphics card: The main game should run on the processor and the graphics calculation should be done on the graphics card using graphics library drivers and shaders which are programs that the game developer creates.

OpenGL acts like a state machine, the OpenGL context must be defined before doing anything. The OpenGL pipeline includes many steps that form the shapes and rasterize the pixels. Shaders are the only steps which we can  customize. Shaders are written in the OpenGL Shading Language, they include in and out variable that interact with the CPU to render graphics.
