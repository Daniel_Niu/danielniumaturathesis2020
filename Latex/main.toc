\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax
\babel@toc {english}{}
\defcounter {refsection}{0}\relax
\contentsline {chapter}{Foreword and Acknowledgements}{v}{chapter*.1}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\nonumberline Contents}{vi}{chapter*.2}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {2}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {1.1}Aim of the thesis}{1}{section.1.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {1.2}About the author}{1}{section.1.2}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {2}Programming in Java}{2}{chapter.2}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {3}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.1}Why Java?}{2}{section.2.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.2}Before Object Oriented Programming}{2}{section.2.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.3}Object oriented programming}{3}{section.2.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.4}Summary}{4}{section.2.4}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {3}Game Engine}{5}{chapter.3}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {4}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.1}What is a game engine?}{5}{section.3.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.2}Game Loop}{5}{section.3.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.3}Game Entities}{7}{section.3.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.4}Summary}{9}{section.3.4}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {4}Graphic Engine}{10}{chapter.4}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {5}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.1}What is OpenGL?}{10}{section.4.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.2}OpenGL as a state machine}{10}{section.4.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.3}Object in OpenGL are IDs}{11}{section.4.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.4}From Vertices to Images}{11}{section.4.4}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.5}Shaders for 2D scenes}{12}{section.4.5}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\nonumberline Vertex Shader}{13}{section*.11}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\nonumberline Fragment Shader}{16}{section*.15}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.6}Rendering 2D image code example}{16}{section.4.6}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.7}Summary}{16}{section.4.7}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {5}Programming as a group}{18}{chapter.5}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {6}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {5.1}Tools}{18}{section.5.1}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\nonumberline Git}{18}{section*.16}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\nonumberline Integrated development environment(IDE)}{18}{section*.17}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {5.2}Communication and Coordination}{21}{section.5.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {5.3}Splitting the work}{22}{section.5.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {5.4}Coding guidelines}{22}{section.5.4}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {5.5}Quality control}{23}{section.5.5}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {5.6}Summary}{26}{section.5.6}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {6}Product}{27}{chapter.6}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {7}
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {7}Procedure}{29}{chapter.7}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {8}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {7.1}Structure of lecture}{29}{section.7.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {7.2}OpenGL implementations}{29}{section.7.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {7.3}How to learn new things in programming}{30}{section.7.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {7.4}Coding and Debugging}{31}{section.7.4}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {7.5}Summary}{31}{section.7.5}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {8}Conclusion}{32}{chapter.8}%
\defcounter {refsection}{0}\relax
\etoc@startlocaltoc {9}
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {8.1}Summary}{32}{section.8.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {8.2}End Product}{32}{section.8.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {8.3}Matura Thesis}{32}{section.8.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {8.4}Food for thought}{33}{section.8.4}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{Statement of Honesty}{34}{chapter*.26}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\nonumberline References}{36}{chapter*.28}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{List of Terms}{37}{section*.29}%
